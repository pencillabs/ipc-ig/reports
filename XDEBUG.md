O xdebug gera arquivos de profiling no formato Valgrind, eles já estão
instalados no container do PHP basta adicionar as flags abaixo para habilitar
por padrão e definir o caminho onde os arquivos Valgrind serão salvos:

Adicionar o seguinte em `/usr/local/etc/php/conf.d/xdebug.ini`:

    xdebug.profiler_enable=1
    xdebug.profiler_output_dir=/var/www/public_html/xdebug

Subir a aplicação local, fazer algumas requisições para gerar os arquivos e
analisá-los com a ferramenta para visualização e análise de arquivos profiler
Valgrind `kcachegrind`.
