# Relatório socialprotection.org

O trabalho em desenvolvimento pela Pencillabs juntamente com o IPC-IG na melhoria
dos processos de trabalho e de desempenho da plataforma [Social Protection](https://socialprotection.org)
tem como objetivo alcançar os seguintes resultados:

 1. Possibilitar o desenvolvimento e disponibilização em produção de novas funcionalidades e correções pelo próprio IPC.
 2. Facilitar o processo de identificação e mapeamento de erros na plataforma.
 3. Disponibilizar testes automatizados e capacitar colaboradores para evoluirem a suite de testes.
 4. Desenvolver melhorias de desempenho na plataforma.

Para evitar que este trabalho se torne estático e perca seu objetivo com o tempo,
foi criado este repositório para conter documentações, análises e fluxos de trabalho
sempre atualizados da plataforma.

## 1. Possibilitar o desenvolvimento e disponibilização em produção de novas funcionalidades e correções pelo próprio IPC

Dentro do escopo do objetivo 1 foram desenvolvidos os seguintes produtos:

 - **Reestruturação de ambiente de desenvolvimento para possibilitar alterações de código:**
O ambiente de desenvolvimento não estava funcional. Foram feitas as correções e configurações
necessárias para que o ambiente voltasse a ser utilizado para o desenvolvimento de novas features.

 - **Reestruturação do fluxo de desenvolvimento para possibilitar a internalização de correções:**
O fluxo de trabalho proposto pela empresa não estava adequado devido às divergências entre
o código de produção e de homologação. Como todas as features são testadas antes em homologação,
foi necessário adequar o fluxo para se criar uma `branch` de código a partir de homologação e 
não produção. O novo fluxo, já repassado para o colaborador é apresentado abaixo.

![Fluxo Desenvolvimento](assets/fluxo-desenvolvimento.png)

 - **Desenvolvimento de pequenos ajustes de tema para familiarização com o código juntamente com o colaborador:**
Foi feita a substituição do logo do ipc para exemplificar o novo fluxo de trabalho, além disso,
foi feita uma pequena correção de javascript no header da aplicação, que ainda não está no
repositório de código.

 - **Desenvolvimento de tela de jobs por meio de código para repasse juntamente com o colaborador:**
A tela de jobs que foi inicialmente contruída por meio da interface do Drupal foi
migrada para código em ambiente local controlado, este trabalho será refeito juntamente
com o colaborador para repassar os passos e os arquivos que necessitam de modificação
para o desenvolvimento de novas views no código, utilizando o fluxo ajustado para
disponibilização do trabalho em produção.


## 2. Facilitar o processo de identificação e mapeamento de erros na plataforma

Já no objetivo 2, foram desenvolvidos os seguintes produtos:

 - **Definição de fluxo de mapeamento e registro de erros:** 
O mapeamento e a formalização de erros atualmente são feitos por meio de planilha. Os
registros abertos pelas equipes são centralizados em uma pessoa, responsável por verificar
a real necessidade do registro do erro e o grau de prioridade do problema relatado.
O melhor fluxo é padronizar a abertura de erros de acordo com um template, segundo
disponibilizado [aqui](https://gitlab.com/pencillabs/ipc-ig/reports/-/raw/188ecd8ae8093929757f8fe1c57c9ef007ecbdf8/issue-template.md)
e aberto diretamente no GIRA. Caso o registro não esteja no padrão, o cartão deve 
ser descartado. Essa prática facilita o entendimento dos possíveis bugs, quando eles
acontecem e qual o grau de prioridade estipulado. Uma pessoa deverá ser designada para
avaliar os cartões abertos diariamente.


## 3. Disponibilizar testes automatizados e capacitar colaboradores para evoluirem a suite de testes

Para o objetivo 3 foram desenvolvidos os seguintes produtos:

 - **Desenvolvimento de suite inicial de testes:** 
A [suite de testes automatizados desenvolvida][testes] tem como objetivo verificar se as páginas
foram, pelo menos, renderizadas corretamente, verificar algum erro de timeout da plataforma
e conferir alguns detalhes de conteúdo ou de buscas específicas. A suite ainda possui muito
a ser desenvolvido, mas o processo de capacitação com relação ao desenvolvimento de novos
testes já foi iniciado e o ambiente local de testes do colaborador já está sendo preparado
para execução dos testes e exportação de relatórios.

 - **Desenvolvimento de material de capacitação de colaboradores:** 
A proposta é centralizar no André e Yannick a execução e desenvolvimento de novos
testes sempre que necessário. Para isso, já foi preparado material documentando os
procedimentos para execução e evolução da suite de testes.

## 4. Desenvolver melhorias de desempenho na plataforma

O escopo inicial delimitado para este objetivo seria a execução de correções de
desempenho a nível de tema, incluindo a minificação de arquivos estáticos, resolução
de problemas de renderização do navegador, e correções de html para dispositivos móveis.
Entretanto, com o acontecimento de graves falhas de lentidão na plataforma e a percepção
da equipe de que o tema não seria capaz de gerar tanta lentidão, foi executada uma análise
mais a fundo com relação a stack completa da plataforma. Nesse sentido, o primeiro
entregável desse objetivo é o [resultado da análise de código da plataforma](/NEW_RELIC_REPORT.md)

A segunda entrega deste objetivo é o [teste de performance](/PERFORMANCE.md) que
comprova a suspeita levantada pelos testes do New Relic em relação a lentidão
causada pelo memcache, além disso os testes demonstram também uma melhoria
substancial com cache HTTP do Varnish, reduzindo o tempo de requisições de 5
segundos (média sem memcache) para 5 milisegundos (média com varnish).

 - **Report de análise de código da plataforma:** 
O portal socialprotection.org foi avaliado com apoio do serviço New Relic, uma ferramenta
integrada a um serviço de dashboard com informações sobre performance de aplicações em
todo o seu stack, desde a camada de banco de dados até a interface de usuário no frontend.

Além dos esforços relacionados à avaliação de código e configurações do Drupal, foi feita
uma análise relacionada às configurações internas de execução de processos do PHP e do
gerenciamento de processos PHP do FastCGI. O estudo e os resultados alcançados estão relatados
no [Relatório de avaliação do FPM](/FPM_CHANGES.md).

## 5. Avaliação do ToR para desenvolvimentos futuros

Foi feita uma avaliação do ToR da quarta etapa de desenvolvimento
da plataforma para avaliação da possibilidade de desenvolvimento de uma das features listadas,
atividade que se encontra fora do escopo desse trabalho, mas que tem impacto direto na 
utilização dos fluxos aqui propostos.

[testes]: https://gitlab.com/pencillabs/ipc-ig/social-protection-tests
