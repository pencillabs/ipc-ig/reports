# Relatório de avaliação Social Protection 

O portal socialprotection.org foi avaliado com apoio do serviço New Relic,
uma ferramenta integrada a um serviço de dashboard com informações sobre
performance de aplicações em todo o seu stack, desde a camada de banco de
dados até a interface de usuário no frontend.

O portal tem apresentado lentidão na navegaçao, é possível perceber que algumas
áreas do site levam vários segundos para responder, no diagnśtico realizado
com a ferramenta New Relic é possível perceber que o grande vilão no consumo
de tempo e recursos é a renderização de conteúdo HTML por parte do código PHP,
ou seja, é a própria aplicação PHP escrita em Drupal que está consumindo tempo
anormal a cada requisição ao site.

![New Relic Overview](assets/new_relic_overview_php.png)

A imagem acima apresenta uma visão geral do dashboard do New Relic com os dados
coletados do portal socialprotection.org de uma instância local sendo executada
em ambiente de desenvolvimento, a linha em destaque no gráfico mostra uma requisição
pontual neste ambiente com o tempo total de resposta de 94,2 segundos, mesmo para
um ambiente local, uma requisição com tempo de resposta superior a 1 minuto mostra
claramente problemas em algum ponto da aplicação.

No gráfico do New Relic é possível perceber o tempo de cada camada da aplicação
que formam ao final o tempo total da requisição, tempo de banco MySQL, PHP,
cache, dependências externas, entre outros, de todos os tempos somando 94,2
segundos o tempo tomado pelo PHP ocupa a grande parte, 71,3 segundos, isto
deixa um alerta sobre o PHP e como ele está sendo executado, ou sobre possíveis
problemas de implementação causando tal lentidão, a imagem abaixo também
do New Relic mostra um pouco mais de detalhes sobre a requisição sendo
analisada aqui.

![New Relic Transactions](assets/new_relic_transactions.png)

O New Relic mostra também uma lentidão relativa anormal do módulo
`undp_community` e das views `user_info_header` e `favorites` em relação
ao sistema como um todo, como é possível visualizar nas imagens abaixo.

![time comsuming modules](assets/newrelic/time-consuming-modules.png)

![time comsuming views](assets/newrelic/time-consuming-views.png)

O módulo `undp_community` consome 3 vezes mais tempo que o segundo módulo mais
lento do sistema (diversos cenários além do cenário retratado nos gráficos
acima confirmam este consumo). Algo semelhante com as views `user_info_header`
e `favorites` consumindo em valores relativos muito mais tempo que o restante
das views do sistema, tomando, no melhor caso, 10 vezes mais tempo que qualquer
outra view do portal, num cenário específico (distinto do cenário retratado nos
gráficos acima) o módulo `undp_community` consumiu 10 segundos enquanto o
segundo módulo em consumo de tempo foi o `dblog` e o `context`, consumindo 3
segundos cada um, a view `user_info_header` consumindo 400ms e `favorites`
200ms, enquanto a view `search_pages` consome 20ms, estes valores se alteram em
cada cenário de teste mas a grandeza relativa se mantém, com o módulo
`undp_community` 3 vezes mais lento que os outros módulos do portal e as views
`user_info_header` e `favorites` 10 vezes mais lentas.

O New Relic mostra que o tempo total de banco de dados é maior com o memcache
habilitado (visível na primeira metade do gráfico abaixo) impactando no tempo
total das requisições, como é visto no gráfico **Web Transactions time**
relativo aos mesmos dados do gráfico sobre o tempo de banco de dados.

![time consuming database](assets/newrelic/time-consuming-database.png)

![web transactions time](assets/newrelic/web-transactions-time-memcache.png)

Diante deste cenário fica claro perceber que o problema na lentidão do portal
está de fato no PHP e não em outras camadas da arquitetura do portal como banco
de dados, ~~sistema de cache memcached~~ ou mesmo a interface de usuário, assim
algumas possibilidades para resolver ou minimamente mitigar tal problema podem
ser tomadas, apresentadas à seguir.

## Solução 1: Configurar cache HTTP usando Varnish ou o próprio Nginx

Adicionar cache de páginas HTML usando solução de cache HTTP como Varnish, de
forma a deixar em cache páginas inteiras evitando acessos ao Drupal, esta
solução pode ser também implementada utilizando o próprio Nginx uma vez que
este já faz parte do stack e possui bom suporte a realizar cache.

Esta solução não resolve o problema no lado PHP, apenas evita que os usuários
em sua maioria enfrentem tal problema uma vez que suas requisições serão
respondidas pelo Varnish ou Nginx sem chegar ao Drupal, importante frisar que
esta solução não abrange sessões de usuários logados no sistema e atingem
apenas acesso anônimo.

## Solução 2: Revisar configuração atual do memcached e quais views estão incluídas

De acordo com o [Report do Site Audit](site_audit.html.md), existem na plataforma
78 views ativas, das quais 65 não estão recebendo nenhum tipo de cache. Além das views, 
existem 65 queries dessas páginas que também poderias ser cacheadas pelo `memcached`. 
Isso dá um total de 83% das páginas sem nenhum cacheamento via `memcached`.

Além das views não cacheadas, o `memcached` não recebeu nenhuma configuração específica
para tratamento das páginas do drupal, o que pode estar levando a um desempenho ruim da
ferramenta. A possibilidade aqui é rever o arquivo de configuração atual do `memcached`
para afinar os recursos às necessidades da plataforma, como limite de memória disponível,
período de expire dos caches, etc.

Uma outra questão a ser avaliada nessa solução é a eficiência do memcached e se
os resultados sem o uso de memcache e apenas do AuthCache do Drupal seria uma
alternativa para melhoria no desempenho da plataforma. [Testes de performance
com o JMeter](/PERFORMANCE.md) mostram que o memcache aumenta em 40% o tempo médio
de resposta das requisições, enquanto o tempo médio com memcache é de 7
segundos, sem o memcache o tempo cai para 5 segundos, estes testes foram
realizados no ambiente de trabalho local de @joenio.

## Solução 3: Revisar e otimizar flags e atributos de configuração de toda a stack

Ajustar com as melhores boas práticas, seguindo a realidade do portal todos os
serviços e ferramentas que o compõe, MySQL, Drupal, Memcached, Nginx, e outros.
Avaliar por exemplo, se a atual configuração do PHP de permitir scripts
alocados em memória ocupando 1GB está dentro do que se considera boa prática e
como isto pode ser contornado. Avaliar também se o MySQL, o memcached e o
próprio Drupal estão bem configurados e quais ajustes podem ser realizados para
reduzir a lentidão atual percebida no sistema.

Esta solução é independente das soluções anteriores e recomenda-se que seja feita
em algum momento, mesmo que tardiamente, é importante rever o setup completamente pois
nesta pequena fase de consultoria realizada foi possível perceber algumas definições
fora do ideal.

É importante deixar claro que as soluções propostas não são mutualmente
conflitantes e podem ser realizadas todas elas ou apenas uma, em qualquer
ordem, sendo o cenário ideal realizar todas elas para resolver de maneira mais
sustentável o problema de lentidão do portal.
