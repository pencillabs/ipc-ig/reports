# Benchmark relacionado a configurações de servidor

Nessa análise foram feitas avaliações do desempenho da plataforma executando 
melhorias nas configurações do *FastCGI Process Manager* (FPM). A avaliação leva
em consideração também características do carregamento das páginas que puderam
ser revistas a partir da melhoria das configurações de servidor.

## Considerações sobre as configurações do servidor

Com base nas análises de servidor executadas, os logs do PHP indicaram configurações
que exploravam pouco os recursos da máquina e como consequência geraram maior lentidão
nas respostas da aplicação. Originalmente, a configuração do FPM era a seguinte:

```php
pm.max_children = 5
pm.start_servers = 2
pm.min_spare_servers = 2
pm.max_spare_servers = 3
```

Essas configurações definem para a aplicação que no máximo 5 processos poderão ser
iniciados (`pm.max_children = 5`), que 2 processos serão criados quando a aplicação
for iniciada (`pm.start_servers = 2`), que o mínimo de processos ativos serão 2
(`pm.min_spare_servers = 2`) e o máximo 3 (`pm.max_spare_servers = 3`). 
Ou seja, posso ter 5 processos, mas apenas 3 em espera, sendo que 2 deles serão criados
mesmo sem conexão.

O volume de requisições que a plataforma recebe diariamente é muito superioir aos
valores configurados originalmente. De acordo com os registros do Cloud Flare, 
a média está variando entre 300 a 500 usuários por hora. Em um dia de pico obtivemos
os acessos apresentados na figura abaixo.

![Dados de Acesso - Cloud Flare][acessos_cloudflare]

Se temos apenas 5 processos respondendo em paralelo, teremos no máximo 5 requisições simultâneas.
Para o volume de 250 a 600 acessos por hora, precisaremos de 4 a 10 processos simultâneos para
responder todas as requisições sem enfileirar nenhuma delas. Pelas análises, a plataforma
mantém pelo menos 4 processos com consumo semelhante paralelo (ainda não foram identificados
quais recursos se mantém persistentes nesses processos), nesse sentido, além dos
10 processos, são necessários mais 4 para evitar gargalos adicionais. Nas configurações atuais,
apenas um processo está disponível continuamente para requisições, o que gera ainda
mais demora.
 Testes de desempenho realizados em janeiro de 2020, apresentaram um comportamento que 
após essa análise fizeram mais sentido. O tempo de resposta do servidor variava entre
0.3 e 4 segundos, indicando que, em momentos de acessos simultâneos, a aplicação 
demorava mais tempo para começar a processar as requisições, confirmando nossa 
hipótese de falhas no paralelismo do php.

## Alterações de Configuração FastCGI Process Manager

Levando em consideração as limitações de recursos do servidor de produção, foram
alteradas as configurações do arquivo `/usr/local/etc/php-fpm.d/www.conf` do
container php para os seguintes valores:

```php

pm.max_children = 20
pm.start_servers = 4
pm.min_spare_servers = 4
pm.max_spare_servers = 8

```

Valores superiores a esses começariam a degradar o desempenho da plataforma,
consumindo mais processamento do que o disponível pela instância atual. Entretanto,
com as configurações, foi possível perceber que o volume de memória da VM é 
bastante superior ao necessário, considerando as limitações de CPU.

Para otimizar ainda mais os recursos, o ideal seria aumentar o número de núcleos
lógicos e diminuir o volume de memória RAM (caso isso reduza custos). O ideal seria
a utilização de 8 núcloes lógicos, com 8GB de RAM. Nessas configurações os valores
seriam:

```php

pm.max_children = 40
pm.start_servers = 8
pm.min_spare_servers = 8
pm.max_spare_servers = 16

```

## Comparativo de desempenho

A comparação entre o tempo de carregamento atual e o anterior à configuração, é
apresentada nesse video, com algumas ponderações apontadas a seguir.

![Load Time Video][load_time_video]

À esquerda é apresentado o carregamento da plataforma no dia 06/04 às 16 horas, 
antes de aumentar a VM de produção, com as correções realizadas. À direita é o
carregamento da plataforma às 9 horas, em 28 de janeiro.

A primeira ponderação a respeito do vídeo é o tempo de resposta ser melhor para 
a nova configuração mesmo em um horário que normalente possui mais acessos (de 
acordo com o Cloud Flare).
A segunda é o tempo de espera após os 3.2 segundos, no carregamento atual.
De acordo com o rastro do tráfego de rede, a aplicação gastou 2.2s requisitando
um arquivo Javascript do servidor `kanecohen.github.io`, e apenas após esse 
carregamento, a modal é apresentada. Desconsiderando o carregamento desse recurso,
a página está interativa apartir de 2.2 segundos e completa aos 4 segundos.

![Rastro de requisições][rastro_de_requisicoes]

Se comparado com o carregamento do servidor no começo do ano, a página obteve
melhora de aproximadamente 1.6 segundos para ficar interativa e de 2.0 segundos
para completar o carregamento. Variando de 3.8 para 2.2 segundos para interatividade
(linha verde na vertical) e de 6.4 para 4.4 segundos para carregamento e execução
de javascript.

![Rastro de requisições Antigo][rastro_de_requisicoes_antigo]

A comparação de desempenho executada em produção após o aumento da máquina para 16Gb
não obteve resultados melhores para momentos de pouco acesso. Entretanto, em momentos
de pico, o tempo de resposta do primeiro byte (início do processamento) foi bastante
estável se comparado com a VM de 8GB, que variava entre 0.2 a 0.6 segundos
dependendo da carga do servidor.

O tempo de resposta, em momentos de pouco acesso estão bastante semelhantes, como
apresentado na figura abaixo. Apesar do conteúdo ser carregado mais rápido, a 
percepção do usuário de diferença de velocidade é praticamente nenhuma (cerca de 
46ms - de 5026ms para 4980ms).

![Comparação servidor aumentado][comparacao_servidor]

## Conclusão e próximos passos

Com os esforços empreendidos até o momento, foi possível gerar melhora significativa
de desempenho da plataforma, com alterações sutis em arquivos de configuração específicos.
Além dessas alterações, serão feitas também, correções a nível de cache da plataforma,
diminuindo o número de acessos anônimos simultâneos ao servidor de produção e corrigindo
configurações falhas de cacheamento interno do Drupal.


[rastro_de_requisicoes]: /assets/rastro_de_requisicoes.png
[rastro_de_requisicoes_antigo]: /assets/rastro_de_requisicoes_antigo.png
[acessos_cloudflare]: /assets/Captura_de_Tela_2020-04-08_às_18.45.19.png
[load_time_video]: /assets/load_time_video.mp4
[comparacao_servidor]: /assets/comparacao_aumento_servidor.png
