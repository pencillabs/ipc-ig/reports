# Resultados de testes feitos com Web Page Test

A ferramenta utilizada possibilita a comparação de vários cenários diferentes,
alguns dos quais foram testados com o objetivo de diminuir o tempo de carregamento
da plataforma. Os cenários são apresentados abaixo e a lista de todos os reports
gerados. Essa lista de testes foram feitos com o objetivo de avaliar principalmente 
melhorias de cacheamento e de recursos frontend pouco explorados.

## Ambiente de Homologação

### ENV 1: Ambiente de homologação sem alterações

Um teste gerado em homologação antes de todas as configurações foi utilizado como
parâmetro de comparação para as demais correções.

[06/04/2020 17:20:14](https://www.webpagetest.org/result/200406_29_2c1e0bf5551a2f8b3a808ad9417ef621/)

[14/04/2020 16:41:23](https://www.webpagetest.org/result/200414_JX_416ed44b419bd9bb407085c9586bdfc7/)

[14/04/2020 17:15:22](https://www.webpagetest.org/result/200414_W3_1e0a209a0f7b6f5bf6d395bde56340bd/)

**Média das métricas de velocidade**

| Load Time | First Byte | First Paint | Speed Index | Last Paint |
| --------- | ---------- | ----------- | ----------- | ---------- |
|   **7.526s** |    **1.471s** |    **3.892s** |    **4.207s** |    **5.033s** |
<!--|   7.717s |    1.192s |    2.246s |    3.595s |    4.800s |-->
<!--|   6.871s |    1.231s |    2.221s |    3.874s |    5.100s |-->
<!--|   7.992s |    1.989s |    3.000s |    4.207s |    5.200s |-->


### ENV 2: Ambiente de homologação com cache Varnish para páginas

A primeira alteração foi a utilização do Varnish para cacheamento de páginas

[14/04/2020 17:03:51](https://www.webpagetest.org/result/200414_52_874e02f6838fe673a5342fba51d38812/)

[14/04/2020 17:20:20](https://www.webpagetest.org/result/200414_9F_04019dc87cf10f94d41bce0dbbc3ddfd/)


**Média das métricas de velocidade**

| Load Time | First Byte | First Paint | Speed Index | Last Paint |
| --------- | ---------- | ----------- | ----------- | ---------- |
|   **5.254s** |    **0.114s** |    **1.220s** |    **2.365s** |    **3.350s** |
<!-- 5.600s	0.107s	1.100s	1.122s	2.295s	3.300s -->
<!-- 4.907s	0.121s	1.200s	1.203s	2.434s	3.400s -->


### ENV 3: Ambiente de homologação com cache Varnish para páginas e assets

Adicionando também regras do Varnish para melhorar o cacheamento de estáticos, tivemos outras melhorias

[14/04/2020 17:27:03](https://www.webpagetest.org/result/200414_3P_e60887dc86f32ee06420b38a62ff7b82/)

[15/04/2020 09:37:03](https://www.webpagetest.org/result/200415_Y5_65a663a79ad3d6bc34a0ebff3ad6f80f/)

**Média das métricas de velocidade**

| Load Time | First Byte | First Paint | Speed Index | Last Paint |
| --------- | ---------- | ----------- | ----------- | ---------- |
|   **4.741s** |    **0.120s** |    **1.080s** |    **2.424s** |    **3.350s** |
<!-- 5.060s	0.119s	1.100s	1.088s	2.414s	3.300s -->
<!-- 4.907s	0.121s	1.200s	1.203s	2.434s	3.400s -->

### Resultados

As métricas importantes de serem observadas entre o ENV 1 e o ENV 2 foram o *first byte*
e o *last paint* que reduziram bastante com o cacheamento do html. Isso porque com
o uso do Varnish, as requisições são respondidas sem a necessidade de processamento 
pelo php, reduzindo a carga do servidor e melhorando o desempenho para outras requisições
mais prioritárias. Com isso, o tempo total e o tempo da última renderização reduziram
consideravelmente, cerca de 2.3 segundos no carregamento.

Já a comparação entre o ENV 2 e o ENV 3 tiveram mudanças mais sutis, mas com 
resultados significativos. O tempo médio de carregamento da página reduziu cerca
de 0.6 segundos, e uma redução leve no *first paint* dado que algumas requisições
o navegador não precisou executar, devido ao cache local.


## Ambiente de Produção

### Ambiente de produção sem alterações

[29/01/2020 14:13:06](https://www.webpagetest.org/result/200129_N8_6dbf1369b1700f8fe91f6d78dbf152d5/)

[29/01/2020 14:15:04](https://www.webpagetest.org/result/200129_NC_f1e5aee7ea24b465bebccd853a4ee22a/)

[05/02/2020 17:35:33](https://www.webpagetest.org/result/200205_2N_12d093126dd902344fce8153d30025e3/)

[05/02/2020 17:43:30](https://www.webpagetest.org/result/200205_3Z_441f0b2ca0d0d4d83470f45fb86a1a70/)

**Média das métricas de velocidade**

| Load Time | First Byte | First Paint | Speed Index | Last Paint |
| --------- | ---------- | ----------- | ----------- | ---------- |
|   **7.502s** |    **0.847s** |    **3.657s** |    **4.641s** |    **6.477s** |
<!--|   7.875s	0.740s	4.022s	4.902s	6.000s |-->
<!--|   7.885s	0.128s	3.800s	4.024s	6.300s |-->
<!--|   7.179s	1.267s	3.636s	5.089s	7.406s |-->
<!--|   7.072s	1.255s	3.170s	4.549s	6.200s |-->

### Ambiente produção com correção de FPM

[06/04/2020 09:38:34](https://www.webpagetest.org/result/200406_DZ_dc07835da8d60845d2cacb7a18e66fbb/)

[06/04/2020 09:53:44](https://www.webpagetest.org/result/200406_44_0b21315506e337e74af9545d6913b090/)

[06/04/2020 10:02:12](https://www.webpagetest.org/result/200406_J8_23908a209a80811d2945bf38217ed016/)

[06/04/2020 10:56:41](https://www.webpagetest.org/result/200406_M0_55df7266702a53007173ccfbf75b9c6d/)

[07/04/2020 09:24:39](https://www.webpagetest.org/result/200407_7S_e7410f05198040dbf8e869fca22ee7a6/)

[08/04/2020 10:37:13](https://www.webpagetest.org/result/200408_70_908badf5a2e7079efdf21d5182cf1dd6/)


**Média das métricas de velocidade**
*Tempo médio de resposta: 4.810 segundos*

| Load Time | First Byte | First Paint | Speed Index | Last Paint |
| --------- | ---------- | ----------- | ----------- | ---------- |
|   **4.758s** |    **0.916s** |    **2.339s** |    **3.414s** |    **5.133s** |
<!--|   4.659s	0.856s	2.328s	3.282s	5.000s |-->
<!--|   4.678s	0.916s	2.207s	3.241s	5.000s |-->
<!--|   4.701s	0.953s	2.800s	3.685s	5.200s |-->
<!--|   4.762s	0.870s	2.000s	3.204s	5.000s |-->
<!--|   4.772s	0.992s	2.600s	3.623s	5.300s |-->
<!--|   4.976s	0.913s	2.100s	3.414s	5.300s |-->

### Ambiente de produção com correção de cache

[17/04/2020 17:38:08](https://www.webpagetest.org/result/200417_KR_9c416081fca9a9e1f31c093b5e22bb7d/)

[17/04/2020 17:56:28](https://www.webpagetest.org/result/200417_ZS_c6d429ce98f314b606775e17919e9e27/)

[17/04/2020 22:43:24](https://www.webpagetest.org/result/200418_FR_d3048079d502d8fc6ff7e0b160bbd36e/)

[17/04/2020 21:43:34](https://www.webpagetest.org/result/200418_28_7ab399b2e4dd08b0c659b7d605ad2c07/)


A meta de melhora no ambiente de produção é voltada para a redução direta de duas
métricas: *First Byte* e *First Paint*, para as quais foram alcançados resultados
semelhantes a homologação, com valores próximos a **0.120s** e **1.080s**. Nesse sentido,
alcançamos resultados próximos a 3.5s de carregamento e 

| Load Time | First Byte | First Paint | Speed Index | Last Paint |
| --------- | ---------- | ----------- | ----------- | ---------- |
|   **3.574s** |    **0.206s** |    **1.239s** |    **2.414s** |    **2.701s** |

