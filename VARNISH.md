# Cache HTTP com Varnish

O Varnish foi configurado no servidor de homologação
http://homolog.socialprotection.org para medir e validar os ganhos reais no tempo
de resposta com cache HTTP.

```mermaid
graph LR
    A[User] -->|request| B(Nginx)
    B -->|fastcgi| C(Drupal)
    C -->|query| D[(Mysql)]
    E[User] -->|request| F{{Varnish}}
    F -. proxy .-> G(Nginx)
    G -. fastcgi .-> H(Drupal)
    H -. query .-> I[(Mysql)]
    style G stroke-dasharray: 5
    style H stroke-dasharray: 5
    style I stroke-dasharray: 5
```

O diagrama acima apresenta as conexões e consultas evitadas pelo Varnish, em
tracejado, contribuindo para evitar consultas ao banco e renderização de
códigos PHP do Drupal quando se encontra em cache a página requisitada pelo
usuário.

O ambiente de homologação foi analisado com auxílio do serviço
[WePageTest](https://www.webpagetest.org) e os resultados mostram
um ganho relevante no tempo de resposta.

Os testes do JMeter foram também executados, a vazão foi de 2,0/segundo e
o tempo médio de 1,5 segundos por requisição.

![sumario jmeter homolog](assets/jmeter/sumario-homolog.png)

Os testes do JMeter gerou alguns erros de timeout no Varnish (exemplo abaixo),
talvez seja necessário aumentar o timeout padrão do Varnish.

![timeout varnish](assets/jmeter/homolog-guru-meditation.png)

A maior parte dos erros são da página `/help`, abaixo segue o arquivo de
configuração do Varnish.

```varnish
vcl 4.0;

sub vcl_backend_response {
  # cache if no Cookie!
  # Drupal is adding "Cache-Control: no-cache, must-revalidate" HTTP header
  # to every request, this makes Varnish not cache at all, cause this
  # we are removing Cache-Control header to instruct Varnish to do cache
  # but we do not want to cache logged-in users with Cookie
  if (!bereq.http.Cookie) {
    set beresp.ttl = 300s; # 5min
    unset beresp.http.Cache-Control;
  }
  if (bereq.url ~ "/files" ||
      bereq.url ~ "(?i)\.(pdf|asc|dat|txt|doc|xls|ppt|tgz|csv|png|gif|jpeg|jpg|ico|swf|css|js)(\?.*)?$") {
    unset beresp.http.set-cookie;
    set beresp.http.cache-control = "public, max-age=691200";
    set beresp.ttl = 8d;
    #return (deliver);
  }
}

backend default {
  .host = "nginx";
  .port = "8080";
}
```

Referências:
* [Varnish 4.x Configuration](https://www.drupal.org/node/2626330)
* [Cache tags + Varnish](https://www.drupal.org/docs/8/api/cache-api/cache-tags-varnish)
* [Some sample VCL for Drupal 7 and 8](https://www.varnish-software.com/wiki/content/tutorials/drupal/drupal_vcl.html)
