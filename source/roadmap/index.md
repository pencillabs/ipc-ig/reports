---
title: Roadmap
date: 2020-06-23 11:11:26
toc: true
---

The development cycle of the Social Protection platform follows the [Scrum metodology](https://www.scrum.org/). To be able to execute fast and reliable delivery of functional software, the team is developing a set of enhancements to provide the required toolset: An **efficient automated testing structure**, a **Continuous Integration & Deploy pipeline** and a **Standadized workflow for development**. This page contains the roadmap for achieving these goals and expectations about new features and bug fixes. To read more about the process, follow to the [Overall Information](#Overall-information) section.

## Non-technical work

 - **Meetings:** Sprint Planning, Review and Retrospective meetings.
 - **Planner tracking:** Answer, prioritize and review Planner tasks to update backlog and keep the team up to date.
 - **Support calls:** Emergengy calls to resolve issues or answer questions.
 - **Roadmap Updates:** Weekly roadmap updates to keep it up to date.

### **[Preparation: Roadmap and workflow](https://gitlab.com/pencillabs/ipc-ig/social-protection/-/milestones/1)**
<small class="sprint-date">From 02/01/2021 to 02/19/2021</small>

  [✅ Create issues related to backlog bugs and tasks on gitlab](https://gitlab.com/pencillabs/ipc-ig/social-protection/-/issues/36)
  [✅ Evaluation of test results of the new production environment](https://gitlab.com/pencillabs/ipc-ig/social-protection/-/issues/14)
  [✅ Fixing bugs that don't require coding](https://gitlab.com/pencillabs/ipc-ig/social-protection/-/issues/13)
  [✅ Updating planner and tasks](https://gitlab.com/pencillabs/ipc-ig/social-protection/-/issues/12)
  [✅ Higher Logic Meeting for presenting OCs platform](https://gitlab.com/pencillabs/ipc-ig/social-protection/-/issues/9)
  [✅ Technical meeting with Higher Logic team](https://gitlab.com/pencillabs/ipc-ig/social-protection/-/issues/11)
  [✅ Effort level mesure for backlog tasks](https://gitlab.com/pencillabs/ipc-ig/social-protection/-/issues/10)
  [✅ Definition of the Sprint 1 backlog](https://gitlab.com/pencillabs/ipc-ig/social-protection/-/issues/45)
  [✅ Update Roadmap strucutre to include sprint task list](https://gitlab.com/pencillabs/ipc-ig/social-protection/-/issues/44)
  [✅ Test Jira integration for Gitlab](https://gitlab.com/pencillabs/ipc-ig/social-protection/-/issues/43)
  [✅ Test HL sandbox environment](https://gitlab.com/pencillabs/ipc-ig/social-protection/-/issues/43)

  Hours (estimated | consumed): [32h | 46h](https://gitlab.com/pencillabs/ipc-ig/social-protection/-/milestones/1)

## Sprints

The execution of the work is divided in sprints, each one containing a set os sprints, epics and goals that are defined bellow.

### **[Sprint 1: Bugfixes ](https://socialprotection.atlassian.net/secure/RapidBoard.jspa?rapidView=1&projectKey=SP&view=reporting&chart=sprintRetrospective&sprint=1)**
  <small class="sprint-date">From 02/22/2021 to 03/05/2021</small>

  <details>
    <summary>
      🔲🟧✅✅✅✅✅✅✅✅✅✅✅✅✅✅✅  Planned: 70h | Consumed: 82h30m
    </summary>
    <div style="background:#f2f2f2;padding:20px;">
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-1"> [ Non-technical ] Prepare USs to next sprint</a><span style="float:right"> 6h | 6h 30m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-8"> [ Non-technical ] Sprint control and monitoring </a><span style="float:right"> 6h | 10h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-61"> Integrate Jobs content type to platform features</a><span style="float:right"> 22h | 23h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-16"> Search pages - wrong number of items per page </a><span style="float:right"> 14h | 10h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-19"> Search pages - Sort by not working </a><span style="float:right"> 10h | 10h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-81"> Fix for jobs application deadline field </a><span style="float:right"> 2h | 2h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-25"> Dificulty to access OCs members emails</a><span style="float:right"> 3h | 4h 30m </span><br/>
      <hr style="background: lightgrey;"/>
      <h6> Other issues </h6>
      ✅ <a href="https://gitlab.com/pencillabs/ipc-ig/social-protection/-/issues/37">[ IC ] Fixes for data centralization of the cluster</a><span style="float:right"></span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-12">[ CP ] Deploy Country Profiles to ci/homolog</a><span style="float:right"></span><br/>
      <hr style="background: lightgrey;"/>
      <h6> Included to the sprint </h6>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-25"> Glossary page with pagination </a><span style="float:right"> -- | 0h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-92"> Add job icon to share page </a><span style="float:right"> -- | 0h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-81"> Blog post pattern 'learn' instead of 'discover' </a><span style="float:right"> -- | 5h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-82"> Error page when trying to access OC anonymously </a><span style="float:right"> -- | 4h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-83"> OC login link redirects to home page </a><span style="float:right"> -- | 2h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-88"> Fixes after last machine integration to cluster </a><span style="float:right"> -- | 6h </span><br/>
      <hr style="background: lightgrey;"/>
      <h6> Removed from Sprint </h6>
      🟧 <a href="https://socialprotection.atlassian.net/browse/SP-17"> Blog tags linking to internal pages </a><span style="float:right"> 10h | -- </span><br/>
      🔲 <a href="https://socialprotection.atlassian.net/browse/SP-18"> Stakeholder edit button when not published </a><span style="float:right"> 6h | -- </span><br/>
    </div>
  </details>

### **[Sprint 2: Initial GTM and Podcasts ](https://socialprotection.atlassian.net/secure/RapidBoard.jspa?rapidView=1&projectKey=SP&view=reporting&chart=sprintRetrospective&sprint=3)**
  <small class="sprint-date">From 03/08/2021 to 03/19/2021</small>

  <details>
    <summary>
      ✅✅✅✅✅✅✅✅✅✅✅✅✅✅✅✅✅✅✅✅✅✅✅🔲🔲🟧🔲🔲  Planned: 151h | Consumed: 114h
    </summary>
    <div style="background:#f2f2f2;padding:20px;">
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-123"> [ Non-technical ] Prepare Databases and Multimedia US </a><span style="float:right"> 1h | 1h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-126"> [ Non-technical ] Sprint control and monitoring </a><span style="float:right"> 6h | 5h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-17"> Blog tags linking to internal pages </a><span style="float:right"> 10h | 7h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-74"> The platform provides initial integration with Google Tag Manager </a><span style="float:right"> 8h | 7h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-101"> The platform sends users' anonymized, nonpersonal information through the GTM dataLayer object </a><span style="float:right"> 8h | 2h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-113"> Contact URL should be user friendly </a><span style="float:right"> 5h | 3h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-104"> The platform sends content metadata through the GTM dataLayer object </a><span style="float:right"> 8h | 5h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-106"> Drupal sends the current user's course_category, course_start_date, course_organiser and course_language values GTM's dataLayer object </a><span style="float:right"> 6h | 5h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-108"> The platform allows crawling from certain bots </a><span style="float:right"> 4h | 2h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-122"> Generate reports that are currently unavailable to download </a><span style="float:right"> 4h | 4h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-60"> Podcasts content type creation </a><span style="float:right"> 40h | 40h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-190"> Deploy features to production </a><span style="float:right"> 1h30m | 3h </span><br/>
      <hr style="background: lightgrey;"/>
      <h6> Other issues </h6>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-12">[ CP ] Validate and deploy Country Profiles to Production</a><span style="float:right"></span><br/>
      <hr style="background: lightgrey;"/>
      <h6> Included to the sprint </h6>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-183"> Coordination with Extendo </a><span style="float:right"> 1h | 1h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-168"> Missing stakeholder on search page </a><span style="float:right"> 1h | 1h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-167"> New home cover image edition </a><span style="float:right"> 1h30m | 3h30m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-160"> Fix email template broken images </a><span style="float:right"> 2h | 2h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-159"> Fix error on publishing stakeholders </a><span style="float:right"> 6h | 7h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-97"> Fix error on publishing stakeholders again </a><span style="float:right"> 30m | 30m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-155"> Fix dev environment of colaborators </a><span style="float:right"> 1h | 1h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-151"> Fix missing fields in newsletter subscription form </a><span style="float:right"> 2h | 2h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-127"> Users unable to edit profile information </a><span style="float:right"> 2h | 2h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-75"> Reports now downloaded correctly </a><span style="float:right"> 8h | 9h </span><br/>
      <hr style="background: lightgrey;"/>
      <h6> Removed from Sprint </h6>
      🔲 <a href="https://socialprotection.atlassian.net/browse/SP-105"> The platform sends search metada through the GTM dataLayer object </a><span style="float:right"> 6h | 0h </span><br/>
      🔲 <a href="https://socialprotection.atlassian.net/browse/SP-124"> [ Non-technical ] Prepare requirements for SP-112 </a><span style="float:right"> 2h | 0h </span><br/>
      🟧 <a href="https://socialprotection.atlassian.net/browse/SP-107"> Stakeholder section's URLs comply with the platform's standard </a><span style="float:right"> 23h | 24h </span><br/>
      🔲 <a href="https://socialprotection.atlassian.net/browse/SP-109"> Telephone numbers use the "tel:+" ahref tag </a><span style="float:right"> 6h | 0h </span><br/>
      🔲 <a href="https://socialprotection.atlassian.net/browse/SP-110"> Email addresses use the "mailto:" ahref tag </a><span style="float:right"> 6h | 0h </span><br/>
    </div>
  </details>


### **[Sprint 3: Provide GTM Integration ](https://socialprotection.atlassian.net/secure/RapidBoard.jspa?rapidView=1&projectKey=SP&view=reporting&chart=sprintRetrospective&sprint=4)**
  <small class="sprint-date">From 03/23/2021 to 04/02/2021</small>

  <details>
    <summary>
      🔲🔲🔲🟦✅✅✅✅✅✅✅✅✅✅✅✅✅✅  Planned: 97h | Consumed: 116h30m
    </summary>
    <div style="background:#f2f2f2;padding:20px;">
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-208"> [ Non-technical ] Sprint control and monitoring </a><span style="float:right"> 6h | 6h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-199"> Send user register event and data to GTM using DataLayers </a><span style="float:right"> 8h | 7h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-200"> Send user login event and data to GTM using DataLayers </a><span style="float:right"> 8h | 6h30m  </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-107"> Stakeholder section's URLs comply with the platform's standard </a><span style="float:right"> 23h | 29h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-105"> The platform sends search metada through the GTM dataLayer object </a><span style="float:right"> 6h | 7h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-124"> [ Non-technical ] Prepare requirements for SP-112 </a><span style="float:right"> 2h | 1h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-112"> Share section forms should be integrated to GTM </a><span style="float:right"> 24h | 21h45m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-106"> Online courses should be integrated to GTM </a><span style="float:right"> 6h | 10h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-209"> Deploy features to homologation environment </a><span style="float:right"> 6h | 6h </span><br/>
      <hr style="background: lightgrey;"/>
      <h6> Other issues </h6>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-192"> Fix broken permissions after Country Profiles Data import script </a><span style="float:right"> 3h | 3h </span><br/>
      <hr style="background: lightgrey;"/>
      <h6> Included to the sprint </h6>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-144"> User should receive "recover password" email when requested </a><span style="float:right"> 16h | 11h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-145"> User should receive "welcome" email when registered </a><span style="float:right"> 1h | 30m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-189"> Fix cron failures on production </a><span style="float:right"> 7h | 6h15m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-224"> Published blog post unavailable to users </a><span style="float:right"> 2h | 1h30m </span><br/>
      <hr style="background: lightgrey;"/>
      <h6> Removed from Sprint </h6>
      🟦 <a href="https://socialprotection.atlassian.net/browse/SP-191"> Legal & Policy Frameworks (LPF) should migrate all publication fields </a><span style="float:right"> 8h | 9h30m </span><br/>
      🔲 <a href="https://socialprotection.atlassian.net/browse/SP-196">[ IC ] Implementation of OC new layout and fixes.</a><span style="float:right"></span><br/>
      🔲 <a href="https://socialprotection.atlassian.net/browse/SP-109"> Telephone numbers use the "tel:+" ahref tag </a><span style="float:right"> 6h | 0h </span><br/>
      🔲 <a href="https://socialprotection.atlassian.net/browse/SP-110"> Email addresses use the "mailto:" ahref tag </a><span style="float:right"> 6h | 0h </span><br/>
    </div>
  </details>


### **[Sprint 4: OCs redesign and GTM completion](https://socialprotection.atlassian.net/secure/RapidBoard.jspa?rapidView=1&projectKey=SP&view=reporting&chart=sprintRetrospective&sprint=5)**
  <small class="sprint-date">From 06/04/2021 to 16/04/2021</small>

  <details>
    <summary>
      ✅✅✅✅✅✅✅✅✅✅✅✅✅✅✅🟦🟧🟧  Planned: 120h30m | Consumed: 55h
    </summary>
    <div style="background:#f2f2f2;padding:20px;">
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-246"> [ Non-technical ] Sprint control and monitoring </a><span style="float:right"> 4h | 4h15m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-18"> Stakeholder edit button when not published </a><span style="float:right"> 6h | 6h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-54"> Hidden OCs are visible on stakeholder's page </a><span style="float:right"> 9h | 9h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-115"> Create data attrubute to track home sections' links </a><span style="float:right"> 4h | 4h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-109"> Telephone numbers use the "tel:+" ahref tag </a><span style="float:right"> 6h | 6h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-110"> Email addresses use the "mailto:" ahref tag </a><span style="float:right"> 6h | 3h30m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-29"> Language not being displayed on publications' page </a><span style="float:right"> 10h | 10h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-245"> Check user login datalayer issues with key-value attributes </a><span style="float:right"> 2h | 2h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-221"> Remove sort by relevance from select list </a><span style="float:right"> 15m | 15m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-237"> Fix webinar access denied issue </a><span style="float:right"> 1h | 1h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-231"> Sort by select disappears after selection </a><span style="float:right"> 15m | 15m </span><br/>
      <hr style="background: lightgrey;"/>
      <h6> Other issues </h6>
      <span> no issues </span>
      <hr style="background: lightgrey;"/>
      <h6> Included to the sprint </h6>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-256"> change links for Connect and Learn pillars </a><span style="float:right"> 1h | 0h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-272"> removelink from podcasts series taxonomy </a><span style="float:right"> 1h30m | 30m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-284"> recover database information for some community pages </a><span style="float:right"> 3h | 2h45m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-285"> gtm share content event not firing for some types </a><span style="float:right"> 1h | 1h30m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-286"> gtm datalayers not supporting multiple key-value objects </a><span style="float:right"> 2h | 2h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-287"> Language, year and date filters returning inconsistent data </a><span style="float:right"> 2h | 2h </span><br/>
      <hr style="background: lightgrey;"/>
      <h6> Removed from Sprint </h6>
      🟦 <a href="https://socialprotection.atlassian.net/browse/SP-196"> [ IC ] Implementation of OC new layout and fixes.</a><span style="float:right"> 20h| 27h </span><br/>
      🟧 <a href="https://socialprotection.atlassian.net/browse/SP-244"> [ IC ] Setup a Drupal 8 data migration environment </a><span style="float:right"> 40h | 29h </span><br/>
      🟧 <a href="https://socialprotection.atlassian.net/browse/SP-191"> Legal & Policy Frameworks (LPF) should migrate all publication fields </a><span style="float:right"> 8h | 9h30m </span><br/>
    </div>
  </details>

### **[Sprint 5: OCs redesign and GTM completion again](https://socialprotection.atlassian.net/secure/RapidBoard.jspa?rapidView=1&projectKey=SP&view=reporting&chart=sprintRetrospective&sprint=6)**
  <small class="sprint-date">From 19/04/2021 to 30/04/2021</small>

  <details>
    <summary>
      ✅🟧🟧✅✅✅✅✅✅✅✅✅✅🟧✅✅🟧 Planned: 176h | Consumed: 160h50m
    </summary>
    <div style="background:#f2f2f2;padding:20px;">
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-312"> [ Non-technical ] Sprint control and monitoring </a><span style="float:right"> 12h | 16h30m </span><br/>
      🟧 <a href="https://socialprotection.atlassian.net/browse/SP-191"> Legal & Policy Frameworks (LPF) should migrate all publication fields </a><span style="float:right"> 26h | 19h </span><br/>
      🟧 <a href="https://socialprotection.atlassian.net/browse/SP-35"> Homepage map with worng numbers of members and stakeholders </a><span style="float:right"> 8h | 10h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-198"> Homepage 'Upcomming Events' fonts with different sizes </a><span style="float:right"> 1h30m | 2h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-293"> members search page's datalayer with malformed data </a><span style="float:right"> 2h | 2h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-301"> create learn_glossary_comment event for gtm </a><span style="float:right"> 4h | 5h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-302"> create learn_econference_add_econference event for gtm </a><span style="float:right"> 4h | 1h30m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-303"> create signup_error event for gtm </a><span style="float:right"> 6h | 6h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-297"> develop Drupal 8 migration roadmap </a><span style="float:right"> 10h | 10h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-296"> "Add members" button visible for non-members </a><span style="float:right"> 10h | 10h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-295"> "Community Resources" visible for non-members </a><span style="float:right"> 8h | 8h20m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-95"> redirect Stakeholder internal path to new urls </a><span style="float:right"> 5h | 5h </span><br/>
      🟧 <a href="https://socialprotection.atlassian.net/browse/SP-238"> user register form missing "no affiliation" option</a><span style="float:right"> 12h | 14h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-307"> Migrate code to a Bitbucket repository </a><span style="float:right"> 3h | 3h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-313"> include gtm data attribute to highlight links </a><span style="float:right"> 30m | 30m </span><br/>
      <hr style="background: lightgrey;"/>
      <h6> Other issues </h6>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-196"> [ IC ] Implementation of OC new layout and fixes.</a><span style="float:right"> 34h| 48m </span><br/>
      🟧 <a href="https://socialprotection.atlassian.net/browse/SP-244"> [ IC ] Setup a Drupal 8 data migration environment </a><span style="float:right"> 40h | 29h </span><br/>
      <hr style="background: lightgrey;"/>
      <h6> Included to the sprint </h6>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-320"> Infrastructure maintenance tasks </a><span style="float:right"> 3h | 3h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-28"> Translation fixes for "Related Content" </a><span style="float:right"> 2h | 2h </span><br/>
      <hr style="background: lightgrey;"/>
      <h6> Removed from Sprint </h6>
    </div>
  </details>

### **[Sprint 6: Consent management & Drupal 8 migration overview](https://socialprotection.atlassian.net/secure/RapidBoard.jspa?rapidView=1&projectKey=SP&view=reporting&chart=sprintRetrospective&sprint=7)**
  <small class="sprint-date">From 03/05/2021 to 14/05/2021</small>

  <details>
    <summary>
      ✅✅✅✅✅✅✅✅🟧✅✅✅✅✅🟧✅✅🟧✅✅✅✅✅ Planned: 142h | Consumed: 140h15m
    </summary>
    <div style="background:#f2f2f2;padding:20px;">
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-343"> [ Non-technical ] Sprint control and monitoring </a><span style="float:right"> 16h | 17h30m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-339"> Produce a heat map to identify data points, risks and security conditions </a><span style="float:right"> 24h | 14h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-352"> Infrastructure operations and maintenance </a><span style="float:right"> 3h | 4h30m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-258"> Add Instagram option to Stakeholder form </a><span style="float:right"> 6h | 5h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-331"> Setup 'MENASP' survey layout to support arabic </a><span style="float:right"> 6h | 4h40m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-238"> 'No Affiliation' option missing on Register form</a><span style="float:right"> 12h | 10h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-93"> Working groups should be hidden on hidden OC </a><span style="float:right"> 30m | 30m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-346"> Three-step register form </a><span style="float:right"> 24h | 24h </span><br/>
      🟧 <a href="https://socialprotection.atlassian.net/browse/SP-191"> Legal & Policy Frameworks (LPF) should migrate all publication fields </a><span style="float:right"> 26h | 25h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-34"> Search list css fixes for right side fields </a><span style="float:right"> 8h | 2h30m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-57"> No results option for webinars search page </a><span style="float:right"> 4h | 3h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-222"> "Add Comment" button visible for anonymous users </a><span style="float:right"> 2h | 1h45m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-253"> "Highlight" and "Featured Content" Blocks should be translated </a><span style="float:right"> 6h | 5h30m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-274"> Update breadcrumbs for current pillars pattern </a><span style="float:right"> 10h | 8h </span><br/>
      🟧 <a href="https://socialprotection.atlassian.net/browse/SP-336"> The platform provides several transactional email schemes </a><span style="float:right"> 80h | 17h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-55"> "Add community page" visible for non-members </a><span style="float:right"> 30m | 30m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-125"> Only OC admin should be able to add members and pages </a><span style="float:right"> 30m | 30m </span><br/>
      🟧 <a href="https://socialprotection.atlassian.net/browse/SP-156"> Redefine OCs permission structure </a><span style="float:right"> 64h | 6h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-347"> Check data collected by survey </a><span style="float:right"> 4h | 3h20m </span><br/>
      <hr style="background: lightgrey;"/>
      <h6> Other issues </h6>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-244"> [ IC ] Setup a Drupal 8 data migration environment </a><span style="float:right"> 40h | 54h </span><br/>
      <hr style="background: lightgrey;"/>
      <h6> Included to the sprint </h6>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-352"> Infrastructure maintenance tasks </a><span style="float:right"> 3h | 5h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-384"> Change COVID-19 link on Discover menu </a><span style="float:right"> 1h | 45m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-380"> Add COVID-19 link on top menu </a><span style="float:right"> 1h | 45m </span><br/>
      <hr style="background: lightgrey;"/>
      <h6> Removed from Sprint </h6>
    </div>
  </details>

### **[Sprint 7: Online Communities Permissions](https://socialprotection.atlassian.net/secure/RapidBoard.jspa?rapidView=1&projectKey=SP&view=reporting&chart=sprintRetrospective&sprint=8)**
  <small class="sprint-date">From 17/05/2021 to 28/05/2021</small>

  <details>
    <summary>
      ✅✅✅✅✅✅✅✅✅✅✅✅✅🟧🟦✅✅✅🟦✅✅✅✅ Planned: 170h | Consumed: 192h20m
    </summary>
    <div style="background:#f2f2f2;padding:20px;">
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-400"> [ Non-technical ] Sprint control and monitoring </a><span style="float:right"> 12h | 13h20m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-26"> Translate navigation buttons "next" and "prev" on search pages </a><span style="float:right"> 5h | 5h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-58"> Add "Description" field to Jobs search filters </a><span style="float:right"> 4h | 4h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-156"> Redefine permissions scheme for Online Communities </a><span style="float:right"> 34h | 40h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-238"> Add "No Affiliation" option to register form institution select </a><span style="float:right"> 12h | 12h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-346"> Three-step register form definition </a><span style="float:right"> 24h | 32h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-390"> User Session must include data required by GTM measurement model </a><span style="float:right"> 16h | 16h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-392"> Message for the "Other" institution option not being shown </a><span style="float:right"> 15m | 15m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-396"> Fix cyclical redirects on contributed modules </a><span style="float:right"> 8h | 12h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-397"> Add COVID-19 button to topbar menu </a><span style="float:right"> 2h | 2h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-404"> Create and prepare local development envirionment for drupal 8 </a><span style="float:right"> 12h | 12h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-405"> Fix members report export </a><span style="float:right"> 4h | 4h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-424"> Analyse network consumption betoween cluster nodes </a><span style="float:right"> 4h | 4h </span><br/>
      🟧 <a href="https://socialprotection.atlassian.net/browse/SP-234"> Redesign country Profile page </a><span style="float:right"> 36h | 41h50m </span><br/>
      🟦 <a href="https://socialprotection.atlassian.net/browse/SP-389"> Add Document type to Legal Frameworks migration </a><span style="float:right"> 6h | 6h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-341"> Determine data points' processing purpose </a><span style="float:right"> 30h | 4h </span><br/>
      <hr style="background: lightgrey;"/>
      <h6> Other issues </h6>
      <hr style="background: lightgrey;"/>
      <h6> Included to the sprint </h6>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-331"> Redefine MENASP survey layout for Arabic language</a><span style="float:right"> 3h | 10h25m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-415"> Infrastructure migration </a><span style="float:right"> -- | 2h </span><br/>
      🟦 <a href="https://socialprotection.atlassian.net/browse/SP-425"> Create generic popup block </a><span style="float:right"> 24h | 15h30m </span><br/>
      <hr style="background: lightgrey;"/>
      <h6> Removed from Sprint </h6>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-353"> Administrators view to content lists in drupal 8 </a><span style="float:right"> 50h | 24m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-391"> Size all Drupal 8 migration stories </a><span style="float:right"> 20h | -- </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-310"> integrate infrastructure deployment hooks with bitbucket </a><span style="float:right"> -- | 8h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-311"> Configure secure connection with docker swarm api </a><span style="float:right"> -- | 8h </span><br/>
    </div>
  </details>

### **[Sprint 8: Configurable forntpage blocks ](https://socialprotection.atlassian.net/secure/RapidBoard.jspa?rapidView=1&projectKey=SP&view=reporting&chart=sprintRetrospective&sprint=9)**
  <small class="sprint-date">From 31/05/2021 to 11/06/2021</small>

  <details>
    <summary>
      ✅✅✅✅✅✅✅✅✅✅✅🟦 Planned: 160h | Consumed: 151h20m
    </summary>
    <div style="background:#f2f2f2;padding:20px;">
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-448"> [ Non-technical ] Sprint control and monitoring </a><span style="float:right"> 12h | 17h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-402"> Drupal 8 search pages and facets evaluation </a><span style="float:right"> 2h | 3h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-426"> Cookie concent Popup block </a><span style="float:right"> 40h | 36h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-462"> Public network traffic issues </a><span style="float:right"> 8h | 8h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-433"> Comparative analysis between CMS alternatives </a><span style="float:right"> 12h | 13h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-333"> Highlights popup block </a><span style="float:right"> 30h | 7h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-191"> Legal and Policy Frameworks migration </a><span style="float:right"> 3h | 3h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-425"> Configurable Popup block </a><span style="float:right"> 15h | 15h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-43"> OCs content moderation </a><span style="float:right"> 40h | 27h20m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-396"> Size Drupal 8 migration stories </a><span style="float:right"> 20h | 19h </span><br/>
      🟦 <a href="https://socialprotection.atlassian.net/browse/SP-397"> Validate Country Profiles layout </a><span style="float:right"> 2h | 4h </span><br/>
      <hr style="background: lightgrey;"/>
      <h6> Other issues </h6>
      <hr style="background: lightgrey;"/>
      <h6> Included to the sprint </h6>
      <hr style="background: lightgrey;"/>
      <h6> Removed from Sprint </h6>
    </div>
  </details>

### **[Sprint 9: User profile enhancements and OC content moderation](https://socialprotection.atlassian.net/secure/RapidBoard.jspa?rapidView=1&projectKey=SP&view=reporting&chart=sprintRetrospective&sprint=10)**
  <small class="sprint-date">From 13/06/2021 to 26/06/2021</small>

  <details>
    <summary>
      ✅✅✅✅✅✅✅✅✅✅✅🟦 Planned: 140h | Consumed: 140h10m
    </summary>
    <div style="background:#f2f2f2;padding:20px;">
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-470"> [ Non-technical ] Sprint control and monitoring </a><span style="float:right"> 12h | 12h 50m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-493"> Update new candidates assignments </a><span style="float:right"> 1h | 20m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-426"> Cookie concent Popup block </a><span style="float:right"> 40h | 41h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-492"> Describe services and ports usage by application </a><span style="float:right"> 1h | 1h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-478"> Wagtail POC development </a><span style="float:right"> 40h | 30h15m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-490"> Devise platform's service requirements </a><span style="float:right"> 1h | 1h30m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-468"> Video icon does not appear on webinar button </a><span style="float:right"> 30m | 30m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-469"> Cannot publish stakeholder </a><span style="float:right"> 30m | 30m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-333"> Configurable highlights block </a><span style="float:right"> 40h | 30h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-467"> Enhance user profile layout </a><span style="float:right"> 30h | 25h50m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-43"> OC administrators can moderate content </a><span style="float:right"> 40h | 38h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-434"> The platform anonymizes user IP addresses </a><span style="float:right"> 8h | 8h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-427"> Hide langulage and gender fields from user profiles and search pages </a><span style="float:right"> 20h | 18h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-434"> Deploy Leagal and Policy Frameworks </a><span style="float:right"> 1h | 1h </span><br/>
      <hr style="background: lightgrey;"/>
      <h6> Other issues </h6>
      <hr style="background: lightgrey;"/>
      <h6> Included to the sprint </h6>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-489"> Review AI provider's proposal </a><span style="float:right"> 4h | 4h30m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-466"> e-TRANSFORM stakeholder not indexed on search page </a><span style="float:right"> 30m | 20m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-494"> Backlog review and new issues registration </a><span style="float:right"> 2h | 1h15m </span><br/>
      <hr style="background: lightgrey;"/>
      <h6> Removed from Sprint </h6>
    </div>
  </details>

### **[Sprint 10: Infrastructure migration and Wagtail proof of concept ](https://socialprotection.atlassian.net/secure/RapidBoard.jspa?rapidView=1&projectKey=SP&view=reporting&chart=sprintRetrospective&sprint=11)**
  <small class="sprint-date">From 28/06/2021 to 09/07/2021</small>

  <details>
    <summary>
      ✅🟧🟧✅🟧✅✅✅✅✅✅🟧✅✅✅🟧🟦 Planned: 120h | Consumed: 117h30m
    </summary>
    <div style="background:#f2f2f2;padding:20px;">
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-513"> [ Non-technical ] Sprint control and monitoring </a><span style="float:right"> 12h | 9h </span><br/>
      🟧 <a href="https://socialprotection.atlassian.net/browse/SP-417"> Infrastructure machines configuration </a><span style="float:right"> 10h | 12h </span><br/>
      🟧 <a href="https://socialprotection.atlassian.net/browse/SP-418"> New Cluster configuration </a><span style="float:right"> 4h | 4h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-528"> Cyber attack mitigations </a><span style="float:right"> 3h | 4h </span><br/>
      🟧 <a href="https://socialprotection.atlassian.net/browse/SP-478"> Wagtail proof of concept </a><span style="float:right"> 50h | 11h45m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-419"> Infrastructure migration roadmap </a><span style="float:right"> 15h | 15h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-509"> Microsoft support integration </a><span style="float:right"> 3h | 3h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-467"> Enhance user profile forms </a><span style="float:right"> 3h | 3h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-333"> Highlghts block configurations </a><span style="float:right"> 12h | 1h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-503"> Reestimate all Drupal 8 tasks </a><span style="float:right"> 5h | 5h50m </span><br/>
      🟧 <a href="https://socialprotection.atlassian.net/browse/SP-510"> Populate datalayer with stakeholder data </a><span style="float:right"> 20h | 15h 30m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-511"> Track OCs content creation </a><span style="float:right"> 5h | 3h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-264"> Change 'Macedonia' name on world map </a><span style="float:right"> 2h | 2h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-481"> Add translations to OC settings page </a><span style="float:right"> 5h | 5h </span><br/>
      🟧 <a href="https://socialprotection.atlassian.net/browse/SP-505"> Plan taxonomy structure by Content Type </a><span style="float:right"> 4h | 3h 20m </span><br/>
      🟦 <a href="https://socialprotection.atlassian.net/browse/SP-434"> IP anonymization </a><span style="float:right"> 4h | 4h </span><br/>
      <hr style="background: lightgrey;"/>
      <h6> Other issues </h6>
      <hr style="background: lightgrey;"/>
      <h6> Included to the sprint </h6>
      <hr style="background: lightgrey;"/>
      <h6> Removed from Sprint </h6>
    </div>
  </details>

### **[Sprint 11: Infrastructure migration and Wagtail proof of concept 2 ](https://socialprotection.atlassian.net/secure/RapidBoard.jspa?rapidView=1&projectKey=SP&view=reporting&chart=sprintRetrospective&sprint=12)**
  <small class="sprint-date">From 12/07/2021 to 23/07/2021</small>

  <details>
    <summary>
      ✅✅🟧🟧✅🟧✅🟧✅✅✅✅✅✅✅✅✅✅🟦✅🟧🟧✅ Planned: 140h | Consumed: 131h50m
    </summary>
    <div style="background:#f2f2f2;padding:20px;">
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-567"> [ Non-technical ] Sprint control and monitoring </a><span style="float:right"> 12h | 12h 50m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-553"> Bug fix for Administer content page </a><span style="float:right"> 5h | 4h 15m </span><br/>
      🟧 <a href="https://socialprotection.atlassian.net/browse/SP-417"> Infrastructure machines configuration </a><span style="float:right"> 12h | 3h </span><br/>
      🟧 <a href="https://socialprotection.atlassian.net/browse/SP-418"> New cluster configuration </a><span style="float:right"> 12h | 5h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-462"> Fixes for infrastructure network traffic </a><span style="float:right"> 20h | 9h </span><br/>
      🟧 <a href="https://socialprotection.atlassian.net/browse/SP-478"> Develop Wagtail proof of concept </a><span style="float:right"> 50h | 3h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-421"> Migrate deployment and CI routines </a><span style="float:right"> 12h | 7h 45m </span><br/>
      🟧 <a href="https://socialprotection.atlassian.net/browse/SP-420"> Migrate service stacks </a><span style="float:right"> 20h | 29h 50m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-589"> Style fixes for surveys </a><span style="float:right"> 3h | 3h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-510"> Populate datalayer with stakeholder data </a><span style="float:right"> 20h | 4h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-511"> Track OCs content creation </a><span style="float:right"> 10h | 4h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-534"> Users can search for podcasts </a><span style="float:right"> 8h | 6h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-564"> Remove public email addresses from user profile </a><span style="float:right"> 2h | 2h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-516"> Socialprotection authorship badges on news </a><span style="float:right"> 4h | 4h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-536"> Socialprotection authorship badges on podcasts </a><span style="float:right"> 4h | 4h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-495"> Users can filter podcasts by serie </a><span style="float:right"> 10h | 6h </span><br/>
      🟦 <a href="https://socialprotection.atlassian.net/browse/SP-474"> Add support to Country Profiles translations </a><span style="float:right"> 10h | 6h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-311"> Setup secure connection with docker registry </a><span style="float:right"> 4h | 3h 20m </span><br/>
      🟧 <a href="https://socialprotection.atlassian.net/browse/SP-541"> New taxonomy strucuture migration </a><span style="float:right"> 20h | 12h </span><br/>
      🟧 <a href="https://socialprotection.atlassian.net/browse/SP-542"> Validate new taxonomy sctructure on CI </a><span style="float:right"> 12h | 30m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-309"> Remove gitlab runner dependencies </a><span style="float:right"> 5h | 5h 20m </span><br/>
      <hr style="background: lightgrey;"/>
      <h6> Other issues </h6>
      <hr style="background: lightgrey;"/>
      <h6> Included to the sprint </h6>
      <hr style="background: lightgrey;"/>
      <h6> Removed from Sprint </h6>
    </div>
  </details>

### **[Sprint 12: Infrastrucutre services migration and Country Profiles Translations ](https://socialprotection.atlassian.net/secure/RapidBoard.jspa?rapidView=1&projectKey=SP&view=reporting&chart=sprintRetrospective&sprint=13)**
  <small class="sprint-date">From 25/07/2021 to 07/08/2021</small>

  <details>
    <summary>
      ✅🟦✅✅✅✅✅🟦🟧🟦🟦🟧🟦🟦🟧 Planned: 90h | Consumed: 96h
    </summary>
    <div style="background:#f2f2f2;padding:20px;">
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-609"> [ Non-technical ] Sprint control and monitoring </a><span style="float:right"> 6h | 10h </span><br/>
      🟦 <a href="https://socialprotection.atlassian.net/browse/SP-473"> Country Profiles translation support </a><span style="float:right"> 12h | 11h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-415"> Infrastrucuture migration </a><span style="float:right"> 20h | 27h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-606"> Academia  and Training fields definitions </a><span style="float:right"> 1h | 1h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-596"> Bug fix for legal and policy frameworks filters </a><span style="float:right"> 1h | 1h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-422"> Final services migration for Azure </a><span style="float:right"> 8h | 8h </span><br/>
      🟦 <a href="https://socialprotection.atlassian.net/browse/SP-607"> Visualization page for Academia and Training </a><span style="float:right"> 8h | 7h </span><br/>
      🟧 <a href="https://socialprotection.atlassian.net/browse/SP-608"> Academia and Training search page and filters </a><span style="float:right"> 15h | 14h </span><br/>
      🟦 <a href="https://socialprotection.atlassian.net/browse/SP-38"> Create content type Academia and Training </a><span style="float:right"> 9h 20m | 10h </span><br/>
      🟦 <a href="https://socialprotection.atlassian.net/browse/SP-507"> Create translation rotines for Country Profiles </a><span style="float:right"> 2h | 2h </span><br/>
      🟧 <a href="https://socialprotection.atlassian.net/browse/SP-602"> Change country profiles views to vinculate taxonomy instead of title </a><span style="float:right"> 8h | 6h </span><br/>
      🟦 <a href="https://socialprotection.atlassian.net/browse/SP-475"> Create API request by spanish </a><span style="float:right"> 2h | 2h </span><br/>
      🟦 <a href="https://socialprotection.atlassian.net/browse/SP-476"> Create API request by french </a><span style="float:right"> 2h | 2h </span><br/>
      🟧 <a href="https://socialprotection.atlassian.net/browse/SP-541"> New taxonomy strucuture migration </a><span style="float:right"> 20h | 15h </span><br/>
      <hr style="background: lightgrey;"/>
      <h6> Other issues </h6>
      <hr style="background: lightgrey;"/>
      <h6> Included to the sprint </h6>
      <hr style="background: lightgrey;"/>
      <h6> Removed from Sprint </h6>
    </div>
  </details>

### **[Sprint 13: Country Profiles & Academia and Training ](https://socialprotection.atlassian.net/secure/RapidBoard.jspa?rapidView=1&projectKey=SP&view=reporting&chart=sprintRetrospective&sprint=14)**
  <small class="sprint-date">From 09/08/2021 to 21/08/2021</small>

  <details>
    <summary>
      ✅✅🟧🟧✅🟧✅🟧✅✅✅✅✅✅✅✅✅✅🟧 Planned: 120h | Consumed: 129h
    </summary>
    <div style="background:#f2f2f2;padding:20px;">
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-682"> [ Non-technical ] Sprint control and monitoring </a><span style="float:right"> 12h | 12h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-614"> Custom message for user register form </a><span style="float:right"> 3h | 3h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-707"> AI processes structures </a><span style="float:right"> 20h | 1h 30m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-704"> Update COVID-19 page highlights </a><span style="float:right"> 2h | 1h 15m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-674"> Update Country Profiles capitals </a><span style="float:right"> 6h | 6h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-607"> Visualization page for Academia and Training </a><span style="float:right"> 10h | 9h 30m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-38"> Create content type Academia and Training </a><span style="float:right"> 12h | 4h </span><br/>
      🟦 <a href="https://socialprotection.atlassian.net/browse/SP-685"> Create new fields for Social Protection Approaches taxonomy </a><span style="float:right"> 10h | 7h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-679"> Add message for user social media accounts on register form </a><span style="float:right"> 4h | 4h </span><br/>
      🟧 <a href="https://socialprotection.atlassian.net/browse/SP-541"> New taxonomy strucuture migration </a><span style="float:right"> 20h | 44h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-681"> [CP] Create relation between Country Profile translations </a><span style="float:right"> 12h | 11h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-695"> Failures caused by docker registry cleanup process </a><span style="float:right"> 3h | 4h 30m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-709"> support for Nepal OC issues </a><span style="float:right"> 1h | 15m </span><br/>
      🟦 <a href="https://socialprotection.atlassian.net/browse/SP-36"> Create Databases content type </a><span style="float:right"> 30h | 29h </span><br/>
      🟧 <a href="https://socialprotection.atlassian.net/browse/SP-37"> Create Multimedia content type </a><span style="float:right"> 20h | 16h 20m </span><br/>
      <hr style="background: lightgrey;"/>
      <h6> Other issues </h6>
      <hr style="background: lightgrey;"/>
      <h6> Included to the sprint </h6>
      <hr style="background: lightgrey;"/>
      <h6> Removed from Sprint </h6>
    </div>
  </details>

### **[Sprint 14: Academia and Training, Databases & Multimedia ](https://socialprotection.atlassian.net/secure/RapidBoard.jspa?rapidView=1&projectKey=SP&view=reporting&chart=sprintRetrospective&sprint=15)**
  <small class="sprint-date">From 22/08/2021 to 04/09/2021</small>

  <details>
    <summary>
      ✅🟧🟧✅🟧✅✅✅✅ Planned: 40h | Consumed: 54h
    </summary>
    <div style="background:#f2f2f2;padding:20px;">
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-488"> AI-powered contnt classifier </a><span style="float:right"> 20h | 31h </span><br/>
      🟧 <a href="https://socialprotection.atlassian.net/browse/SP-472"> [CP] Country Profiles construction and maintenance </a><span style="float:right"> 5h | 5h 30m </span><br/>
      🟧 <a href="https://socialprotection.atlassian.net/browse/SP-137"> Databases' Construction and maintenance </a><span style="float:right"> 2h | 2h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-711"> [CP] Legal and Policy Frameworks </a><span style="float:right"> 5h | 4h </span><br/>
      🟧 <a href="https://socialprotection.atlassian.net/browse/SP-136"> Multimedia's construction and maintenance </a><span style="float:right"> 10h | 9h </span><br/>
      <hr style="background: lightgrey;"/>
      <h6> Other issues </h6>
      <hr style="background: lightgrey;"/>
      <h6> Included to the sprint </h6>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-590"> General platform maintenance </a><span style="float:right"> 0h | 30m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-138"> Academia and Training construction </a><span style="float:right"> 0h | 2h </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-612"> Webinars enhancements and maintenance </a><span style="float:right"> 0h | 30m </span><br/>
      ✅ <a href="https://socialprotection.atlassian.net/browse/SP-620"> Stakeholder's maintenance </a><span style="float:right"> 0h | 30m </span><br/>
      <hr style="background: lightgrey;"/>
      <h6> Removed from Sprint </h6>
    </div>
  </details>

### **[Sprint 15: Academia and Training, Databases & Multimedia ](https://socialprotection.atlassian.net/secure/RapidBoard.jspa?rapidView=1&projectKey=SP&view=reporting&chart=sprintRetrospective&sprint=16)**
  <small class="sprint-date">From 05/09/2021 to 18/09/2021</small>

  <details>
    <summary>
       Planned: 80h | Consumed: 54h
    </summary>
    <div style="background:#f2f2f2;padding:20px;">
      <hr style="background: lightgrey;"/>
      <h6> Other issues </h6>
      <hr style="background: lightgrey;"/>
      <h6> Included to the sprint </h6>
      <hr style="background: lightgrey;"/>
      <h6> Removed from Sprint </h6>
    </div>
  </details>

### Total time consumed

The hours used are executed under some contracts.

|                    |**Hours by contract**|                    |                   |
|:------------------:|:-------------------:|:------------------:|:-----------------:|
| **Contracts**      |     Other hours     |    Canvass 50      |       Total       |
| **Used hours**     |       4160          |         3          |       4163        |
|**Hours to be paid**|         0           |         0          |          0        |
| **Hours left**     |         0           |        47          |         47        |


<small>\**estimated time for contracts by product instead of hours.*</small>

## Overall information

The execution of the work can be financed by different sources of investiment, to have a centralized view of all the work in progress and what is yet to come, everything is presented here, but tagged by the source of investiment and the number of hours estimated and executed by product. The sprints are development cycles of two weeks with a defined goal to achieve and a set of tasks to be completed (the sprint backlog). There are 4 meetings that are executed to follow, adapt and improve the process, they are:

 - Planning: Define the goal of the sprint and select/define user stories from the backlog that can make the goal possible to be accomplished.
 - Daily meeting: A fast feedback of each person of the technical team about what they've done yesterday and what they are going to do today.
 - Review: The technical team presents the results of the sprint to all stakeholders and a shippable product can be deployed to production.
 - Retrospective: The technical team discusses what went right and areas for improvement in the Sprint, and make plans to improve the process.

Stakeholders are required only for Sprint Reviews, while the Product Owner also participates in Sprint Planning. The daily meeting is done asynchronously using a chat application and the retrospective happens every end of sprint, usually on fridays.

Each functionality is written as a User Story (US). The defined notation to write a US is: 

    "As a <USER ROLE>, i'd like to <DESCRIBE FUNCTIONALITY> for <EXPECTED RESULT>."

An example can be:
    
    "As a content manager, i'd like to write a content for publishing to the platform."

A US also has defined acceptance criteria, that defines specificities of the task that has to be included in order to consider the US done. For the example above, the following criteria could be defined:

    A Full html text editor is required;
    The text can be larger than 1000 characters;
    There must be a sumary to the text;
    The text must be longer than 10 characteres.

A US will have tasks to be executed that are listed and estimated in hours to give possibility to control the hours spent by the contract. Each week the hours of the completed tasks will be computed and the estimatives updated.

## Legends
  🔲 TO DO: Not started tasks
  🟧 DOING: Ongoing tasks
  🟦 READY: Executed but not validated/deployed tasks
  ✅  DONE: Validated and deployed tasks 

## Deliverables

There were defined a set of deliverables that were prioritized to development, which are:

### [**Country Profiles**](https://gitlab.com/groups/pencillabs/ipc-ig/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=Country%20Profiles)
  Develop features related to Country Profiles features and COVID-19 response matrix. 

  <details>
    <summary>
      🟧✅✅✅✅✅✅✅✅✅✅✅✅✅✅✅✅  [ 1 DOING | 16 DONE ]
    </summary>
    <div style="background:#f2f2f2;padding:20px;">
      ✅ <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/52">Fixes for search API index and filters</a><br/>
      ✅ <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/51">Update search filters for Legal and Policy Frameworks</a><br/>
      ✅ <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/50">Programmes count per type</a><br/>
      ✅ <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/49">Style review for Country Profile pages</a><br/>
      ✅ <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/48">Fetch content count by country on Country Profile page</a><br/>
      ✅ <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/47">Add link to country profile on homepage map</a><br/>
      ✅ <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/40">Additional updates</a><br/>
      ✅ <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/39">Data migration for Legal and Policy Frameworks</a><br/>
      ✅ <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/38">Legal and Policy Frameworks reports</a><br/>
      ✅ <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/37">cron update for country profiles fields populated via API</a><br/>
      ✅ <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/36">Country Profiles reports</a><br/>
      ✅ <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/43">Listing page for Legal and Policy Framework</a><br/>
      ✅ <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/42">Legal and Policy Framework menu link under Share pillar</a><br/>
      ✅ <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/35">Country Profile content type</a><br/>
      ✅ <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/34">Legal and Policy Frameworks content type</a><br/>
      ✅ <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/63">Minor layout fixes</a><br/>
      🟧 <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/62">Minor fixes for contexts and features</a><br/>
    </div>
  </details>
  <br/>

### [**Infrastructure updates**](https://gitlab.com/groups/pencillabs/ipc-ig/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=Infra%20%26%20Deploy)
  Creation of the DevOps pipeline, monitoring services and failure tolerance strutcture of the platform.

  <details>
    <summary>
      🟧✅✅✅✅✅✅✅✅✅ [ 1 DOING | 9 DONE ]
    </summary>
    <div style="background:#f2f2f2;padding:20px;">
      🟧 <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/56">Setup homolog environment</a><br/>
      ✅ <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/55">Add production machine to the cluster</a><br/>
      ✅ <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/55">Update gitlab-ci to run tests under ci environment</a><br/>
      ✅ <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/61">Fixes for NFS settings</a><br/>
      ✅ <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/41">Update certificates for cluster setup</a><br/>
      ✅ <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/27">Create custom stack for CI tests</a><br/>
      ✅ <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/26">Setup CI tests under development branches</a><br/>
      ✅ <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/25">Setup homologation environment</a><br/>
      ✅ <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/24">Develop custom stacks for Social Protection envs</a><br/>
      ✅ <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/23">Setup Docker Swarm and NFS</a><br/>
    </div>
  </details>
  <br/>

### [**Staff training**](https://gitlab.com/pencillabs/ipc-ig/reports/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=Capacita%C3%A7%C3%A3o)
  Training the technical team about procedures and technologies used to maintain and develop new features for the platform.

  <details>
    <summary>
      ✅✅✅✅✅✅✅ [ 1 READY | 6 DONE ]
    </summary>
    <div style="background:#f2f2f2;padding:20px;">
      ✅ <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/60">Knowledge transfer in application technologies</a><br/>
      ✅ <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/59">Training in infrastructure matters</a><br/>
      ✅ <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/58">Local environment setup and first steps</a><br/>
      ✅ <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/46">Training in Cypress tests</a><br/>
      ✅ <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/45">Training in theme development</a><br/>
      ✅ <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/29">Deploy theme changes automatically</a><br/>
      ✅ <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/23">Theme extraction to different repository</a><br/>
    </div>
  </details>
  <br/>

### [**Drupal 8+ data migration**](https://gitlab.com/groups/pencillabs/ipc-ig/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=Drupal%208)
  Development of a full Drupal 8 version of the platform.

  <details>
    <summary>
      🟧✅✅✅✅✅ [ 1 DOING | 5 DONE ]
    </summary>
    <div style="background:#f2f2f2;padding:20px;">
      🟧 <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/9">Consolidate migration report and test environment</a><br/>
      ✅ <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/22">Make initial Drupal 8 migration test</a><br/>
      ✅ <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/21">Migrate database and content types</a><br/>
      ✅ <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/27">Setup environment for full content migration</a><br/>
      ✅ <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/26">Install and configure critical modules</a><br/>
      ✅ <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/23">Develop migration script for translated content</a><br/>
    </div>
  </details>
  <br/>

### [**Online Communities Redesign**](https://gitlab.com/groups/pencillabs/ipc-ig/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=Infra%20%26%20Deploy)
  Development of the new visual and content structure of the online communities.
  
  <details>
    <summary>
      🔲🔲🔲🟧🟧✅✅ [ 3 TO DO | 2 DOING | 2 DONE ]
    </summary>
    <div style="background:#f2f2f2;padding:20px;">
      🔲 <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/33">Develop new tests for timeline and messages</a><br/>
      🔲 <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/32">Create notification types for OCs</a><br/>
      🔲 <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/31">Update OG module to include Message Stack</a><br/>
      🟧 <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/54">Redefine layout contexts for new OC strucutre</a><br/>
      🟧 <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/53">Update visual layout for OCs prototype</a><br/>
      ✅ <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/30">Remake blocks and views for new layout</a><br/>
      ✅ <a href="https://gitlab.com/pencillabs/ipc-ig/reports/-/issues/57">Develop functional prototype for new OCs layout</a><br/>
    </div>
  </details>
  <br/>

### [**Critical bugfixes**](https://gitlab.com/pencillabs/ipc-ig/social-protection/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=bug)
  Development of high priority fixes to be deployed.

### [**Automated testing process**](https://gitlab.com/pencillabs/ipc-ig/reports/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=SP%20bugfix)
  Continuous development of the platform's test suite.

### [**Knowledge base updates**](https://gitlab.com/pencillabs/ipc-ig/reports/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=SP%20bugfix)
  Create new tutorials and articles about the technical work being executed.

### [**Register Form Redesign**](https://gitlab.com/pencillabs/ipc-ig/reports/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=SP%20bugfix)
  Redefine layout structure, fields and behavior of the register form.

### [**Google Tag Manager Module Configuration**](https://gitlab.com/pencillabs/ipc-ig/reports/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=SP%20bugfix)
  Install and configure GTM Module to provide necessary functionality for Tag Manager.

### [**Centralized data reports & Analytics**](https://gitlab.com/pencillabs/ipc-ig/reports/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=SP%20bugfix)
  Integrate a denormalizer tool to batch updade data into a shared database with Power BI.

### [**Search Engine Version Update**](https://gitlab.com/pencillabs/ipc-ig/reports/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=SP%20bugfix)
  Update of the Solr engine to enhance platform performance and maintainability.

### [**Php and Drupal Core Version Update**](https://gitlab.com/pencillabs/ipc-ig/reports/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=SP%20bugfix)
  Update php to version 7.4 and Drupal core to 7.75 to increase performance and security.

### [**Events Section Updates**](https://gitlab.com/pencillabs/ipc-ig/reports/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=SP%20bugfix)
  Develop support for multiple content type search in Events section, enhanced calendat widget and layout improvements.

### [**New Content Types creation**](https://gitlab.com/pencillabs/ipc-ig/reports/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=SP%20bugfix)
  Multimedia, Databases, Livestreams, Podcasts and Academics.

### [**OCs Management Area Update**](https://gitlab.com/pencillabs/ipc-ig/reports/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=SP%20bugfix)
  Moderation functionalities for community members, contents and settings.

### [**Jobs adjustments**](https://gitlab.com/pencillabs/ipc-ig/reports/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=SP%20bugfix)
  Fix Jobs fields to be corelated to Institutions and Countries with geographic areas.

