---
title: UNDP Country Profiles development
date: 2021-01-19 16:15:07
thumbnail: /img/thumb_pattern_blue.svg
toc: true
categories:
 - Development
---

This module accumulates informations about countries such as their social protection indicators, if they have any program with Covid-19 response and other informations.

It is presented as a feature, that can be enabled and disable in the panel `admin/structure/features`. This feature also impacts other ones such as UNDP Search and UNDP Publications.

# Content Types

## Country Profile

Most of the fields in this content are populated automatically, such as: the flag, currency, capital, region and social indicators. However, there is also the featured content part that is selected by moderators. In addition, on its page, it presents some statistics related to the social protection platform, such as programs in the country, how much content there is from that country, such as webinars, communities, events, news and other statistics.

## Legal and Policy Framework

Stores data from two possible documents: Legal Instrument and Policy Strategy. Currently it is being populated by the script **convert_publication_to_legal_framework** in **undp_country_profiles.module**. This script takes Publications from the two types (Legal and Policy) that already existed in the social protection platform and convert to the new content type.

# Discover Menu

Two items were added in the Discover Menu, to represent the two new content type. Each one directs to a paginated listing page of all nodes of that type and has a search bar and some specific filters.



# External APIs

The IPC requested the acquisition of content for the Country Profile content type through the following sources:

 - https://data.worldbank.org/
 - https://products.wolframalpha.com/api/
 - https://www.social-protection.org/gimi/
 - http://hdr.undp.org/en/data


## Wolfram Alpha

Wolfram's results come with the notion of 'pods' which are pieces of information. When searching for the name of a country in wolfram, several data are selected, among them the flag, capital, currency and population. Completed requests can take a few seconds to complete, there are also simplified versions but a search was not found that returned the necessary data.

"https://api.wolframalpha.com/v2/query?input=countries&appid=DEMO&output=json&includepodid=Result";


It was not possible to obtain with a query only all currencies from all countries, the closest thing that occurred was when placing the string "country name currency" the data of the "input interpretation" pod has the currency used in the country.

When researching further, the source of the data (both flags and currencies) was from the wikipedia API, so the use of wolfram was discarded, to avoid its slower requests.


## World Bank Open Data

[API Documentation](https://datahelpdesk.worldbank.org/knowledgebase/articles/898581-api-basic-call-structures)

### Queries that are being used

#### Countries

Countries are saved as Country Profiles and their 3 letter iso code are used to identify them in the data search.

- "http://api.worldbank.org/v2/country?format=json&per_page=304"; 
	- Per_page = 304 was placed to get all country records in one query, as they are paginated
	- Result returns the following data for each country:
		-  3 letter ISO 3166-1 alpha-3 code
		-  2 letter ISO 3166-1 alpha-2 code
		-   Name
		-   Region: ID, name and World Bank 2 letter code
		-   Income Level: ID, name and World Bank 2 letter code
		-   Lending Type: ID, name and World Bank 2 letter code
		-   Capital City
		-   Longitude
		-   Latitude


### Indicators

The World Bank provides a [page with all the indicators it has](https://data.worldbank.org/indicator), divided into categories. In addition to the raw data there are also graphics and some analysis can be done directly on the website.

Para aplicação é importante manter os indicadores sempre atualizados. Dessa forma, toda vez que é rodado o script para atualização de indicadores, é realizada uma requisição enviando o ano atual - no momento, 2021 - e algumas flags para assegurar que o dado é o mais próximo desse ano.

The API allows using two flags, depending on the indicator, which are:
- MRV (Most Recent Value)
- MRNEV (Most recent non empty value)

### Output example

The return, when doing a search on indicators, is performed in the form of a two-position array, in the first position, information about the pagination and when the data was last updated, the results are placed in the second position.

| 0               |                     |
|-----------------|---------------------|
| page            | 1                   |
| pages           | 6                   |
| per_page        | 50                  |
| total           | 264                 |
| sourceid        | "2"                 |
| lastupdated     | "2020-12-16"        |



The table below indicates one of the results that was retrieved, as it can be presented as an array. 0, in the second line is the position of the first result of the array that corresponds to the search, and may have several, when searching all countries, usually has about 264 results.

| 1               |                     |
|-----------------|---------------------|
| 0               |                     |
| indicator       |                     |
| id              | "SP.POP.TOTL"       |
| value           | "Population, total" |
| country         |                     |
| id              | "1A"                |
| value           | "Arab World"        |
| countryiso3code | "ARB"               |
| date            | "2020"              |
| value           | null                |
| unit            | ""                  |
| obs_status      | ""                  |
| decimal         | 0                   |

In the next topics, the endpoints used to collect the indicators are presented

#### Population
- "http://api.worldbank.org/v2/country/all/indicator/SP.POP.TOTL?format=json&date=2020&per_page=264&mrv=1"; 

#### Population Growth
- "http://api.worldbank.org/v2/country/all/indicator/SP.POP.GROW?format=json&date=2020&per_page=264&mrv=1"; 

#### Age dependecy ratio
- "http://api.worldbank.org/v2/country/all/indicator/SP.POP.DPND?format=json&date=2020&per_page=264&mrnev=1"; 

#### Poverty headcount ratio at $1.90 a day (2011 PPP) (% of population) 

- "http://api.worldbank.org/v2/country/all/indicator/SI.POV.DDAY?format=json&date=2020&per_page=264&mrnev=1";

#### Poverty headcount ratio at national poverty lines (% of population)

- "http://api.worldbank.org/v2/country/all/indicator/SI.POV.NAHC?format=json&date=2020&per_page=264&mrv=1";

- Few countries have this data, the world bank returns null and does not have the implementation of flags like mrnev, to try to get only non-zero values, so there are less than 10 countries that actually present this data.

#### GDP

- "http://api.worldbank.org/v2/country/all/indicator/NY.GDP.MKTP.CD?format=json&date=2020";


#### GDP per capita


- "http://api.worldbank.org/v2/country/all/indicator/NY.GDP.PCAP.CD?format=json&date=2020";


# HDR UNDP

"The Human Development Report Office (HDRO) offers this REST API for the developers to query human development related data in JSON format. The data can be queried by indicator id(s), year(s) and country code(s) and group by any order."

An empty query: [http://ec2-54-174-131-205.compute-1.amazonaws.com/API/HDRO_API.php](http://ec2-54-174-131-205.compute-1.amazonaws.com/API/HDRO_API.php) will return all of the data in the HDRO database.

Example, that returns all countries with HDI data in 2017:
http://ec2-54-174-131-205.compute-1.amazonaws.com/API/HDRO_API.php/indicator_id=137506/year=2017

Returns an array composed of three other arrays:
  - indicator_name: the indicator code mapped with its full name;
  - country_name: maps a country's 3-letter code to its English name.
  - indicator_value: Composed of the indicator code, country code and the value in the year indicated.
 
 ### Output Example
```
  [indicator_name] => Array
      (
          [137506] => HDI: Human development index (HDIg) value
      ),
  [indicator_value] => Array
      (
           [AFG] => Array
                (
                    [137506] => Array
                        (
                            [2017] => 0.493
                        )

                ),...
      ),
     [country_name] => Array
   (
      [AFG] => Afghanistan
   ),
```



## How APIs calls are in code

All calls to the APIs are in the file **undp_country_profiles.module**

### Indicators

This are the functions that get and save indicators.

- country_gdp_per_capita
- country_gdp
- country_poverty_lines
- country_poverty_headcount
- country_population_growth
- country_population
- age_dependency
- basic_country_info
- country_hdi

### Flag and Currency

This are the functions that get and save this data, used less because their change is less probable.

- country_currency
- country_flag

Example of a country profile page using data from APIs:

<img src="/ipc-ig/reports/img/country-profile-example.png">


# List of countries that do not have indicators

Until then, these countries do not have data, it is believed that there really are no records.

- Wallis and Futuna
- Taiwan (Only income category and region)
- Guernsey
- Saint Helena
- Niue
- Aland Islands
- Anguilla
- Antarctica
- Bouvet Island
- Christmas Island
- Cocos (Keeling) Islands
- Cook Islands
- Falkland Islands
- French Guiana
- French Southern Territories
- Guadeloupe
- Heard Island and McDonald Islands
- Martinique
- Mayotte
- Montserrat
- Norfolk Island
- Pitcairn
- Reunion
- Saint Barthélemy
- Saint Pierre and Miquelon
- Svalbard and Jan Mayen
- Tokelau
- Vatican
- Western Sahara

