---
title: Infrastructure Configurations
date: 2022-10-21 18:18:01
thumbnail: /img/thumb_pattern.svg
toc: true
categories:
 - Development
 - Infrastructure
---

The infrastructure configuration processes aims to provide balanced resource consumption between machines and provide machine-independent environments for CI, homologation and production, for both Social Protection and IPC website. The redefinition is based on the following processes:

 - Reestructure of the stack configuration to use Swarm mode of Docker
 - Configuration of an NFS server to provide reliable and persistent data layer for databases
 - Configuration of Continuous Integration (CI) for projects
 - Setup of better deploy processes for both production and homologation environments
 - Configuration of backup and restore processes for production environments


This said, the following topics will dig deeper in what have been done up to now about each of these tasks.

# Stack configurations

The platform used to rely on a single-machine setup for main services, and a separate configuration for database. This is limited by the available resources of each machine and restricts the scalability and resource balance for each service. The following image shows how the production environment used to be configured and distributed among available virtual machines.

<img src="/ipc-ig/reports/img/infra-compose.png">

There is a set of identified issues created by this setup that is intended to be fixed on the new infrastructure. They are listed bellow:

  - Extremely complex to recreate: The SocialProtection.org and IPC-IG cannot rely on an infrastructure that is hard to reproduce. There is a constant need of migration between providers and servers that demands ease for migrations.
  - Hard to securely backup: All services are setup on a specific machine and they don't share a secure network layer among application stacks. File synchronization between nodes must be manually configured.
  - Non-scalable: Each service relies on resources from a single machine, what limits the capacity for scalability.
  - Not fault tolerant: If a single node is lost for a reason, the infrastructure cannot recover and all services cannot start.
  - Manual setup: To deploy the platform using this structure, it is required to access each node and execute a long list of commands to prepare them to start all services.
  - very low availabilty: The services frequently get unavailable and some machines are constantly in high CPU consumption while others are idle.

The following image presents how it is implemented now and what effort has been made to assure that these issues are going to be solved.

<img src="/ipc-ig/reports/img/infra-swarm.png">

To attend all the issues, the new infrastructure had to be configured to support any service, in any machine, at any moment. This was achieved using [Docker Swarm mode](https://docs.docker.com/engine/swarm/). The configuration files that specify how each service is provided had to be rewritten to provide the folloing machine independent settings:

 - Hostname and IP addresses
 - Data layers and file systems
 - Environment dependencies

Also, some new services had to be configured to make the overall cluster reliable and highly available. The following topics lists how each issue was addressed:

 - **Simpler setup:** The repository of the cluster contains all needed commands to setup the infrastructure. In shot, all that is required is Docker and NFS for all machines. The greater complexity that might be hard to overcome is Firewall rules to enalbe NFS and Docker Swarm to work. Refer to [This page](https://www.bretfisher.com/docker-swarm-firewall-ports/) to checkout Swarm ports and [this page](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/storage_administration_guide/s2-nfs-nfs-firewall-config) for NFS ports.

 - **Secure backups:** The Docker Swarm platform is a robust cluster environment for small and medium sized institutions. It provides a large documentantion and fast security updates, making it easier to maintain a better infastrucutre. Each service that needs to be backed up provides a backup that is stored by the NFS and can be synced by any other machine inside the cluster using secure SSL connections.

 - **Scalable:** The Swarm + NFS infrastructure is highly scalable. Using [labels](https://docs.docker.com/engine/reference/commandline/node_update/#add-label-metadata-to-a-node) it becomes easy to restrict resources to priority services and define rules that must be followed by any new service. To understand how to add new nodes to a Swarm cluster, refer to the [official docs](https://docs.docker.com/engine/swarm/swarm-tutorial/add-nodes/).

 - **Fault tolerant:** The swarm cluster is fault tolerant. If a certain node fails, the managers redirects all services to the next node with same presets and available resources to accomodate applications. The preset of the nodes has to be configured previously to provide higher reliability, even more when there are asymmetric machines, with different amount of resources, in the cluster.

 - **Automated setup** Every service, stack, application or volume used by the cluster has a set of configuration files versioned on the cluster repository. It is possible to recover older states and apply changes to the cluster by updating the codebase. Also, the cluster provides a private registry that stores all docker images used by custom services, keeping track of code changes and newer builds.

# Available Stacks

The list of stacks currently running is the following:
 
 - Base Production Stack: provides Portainer, Registry and Portainer Agents. This stack is the core of the cluster, it provides management services so that the cluster can be operated.
 - Reverse Proxy: provides a reverse proxy Nginx application. It is the first stack to be setup, since it is required to deploy any other stack. It provides the reverse proxy used by any external request to be redirected to the according application.
 - Development IPC-IG Website: provides MariaDB, Drupal, Nginx, Solr. This stack provides all services required to keep IPCIG up and running, but in a development environment.
 - Production IPC-IG Website: provides MariaDB, Drupal, Nginx, Solr. This stack provides all services required to keep IPCIG up and running.
 - Development SocialProtection: provides Memcached, mySQL, Nginx, Drupal and Solr. All mandatory services to keep Social Protection up and running.
 - Production SocialProtection: provides Memcached, mySQL, Nginx, Drupal, Varnish and Solr. Also provides a Varnish wab cache to improve platform performance on production.
 - Monitoring Stack: Zabbix web, Zabbix server, MySQL, Server Agents. Provides Zabbix services for monitoring and notifying on errors.

For more information about services and stacks, access our [Services page](/ipc-ig/reports/2022/11/12/infrastructure-services/)

# NFS server

The rewirten stack now can be enabled using a NFS server that provides a mount point for any machine configured in the cluster, this enables all services to access data layers and file systems independent of the host where they are running. The Swarm mode creates a single hostname resolver for each service, what makes them accessible anywhere inside the cluster. The Dockerfile settings enable a service to use a predefined list of dependencies and execute the same steps before running, that creates an independent environment for each service. The complete stack configurations can be found on the repository of the cluster.

The NFS strucutre defined has the following directiories:
 - statics: stores all media, files and source code needed by the services
 - nginx: stores certificates, config files and logs
 - data: stores database registries and configurations
 - backups: stores daily and weekly backups


