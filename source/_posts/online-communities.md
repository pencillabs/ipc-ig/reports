---
title: Online Communities - Redesign and Fixes
date: 2020-08-03 14:20:32
thumbnail: /img/thumb_pattern_blue.svg
toc: true
categories:
 - Development
 - Online Communities
---

The Online Communities are tools for knowledge sharing and capacity building that facilitate interactions, discussions, and collaborations among fellow experts. There are two major tasks identified by the team that are:

 - **Redesign** main user interactions and navigation.
 - **Fixes** for critical issues with administration area, like members management and content approval.

# Redesign

The design of the resourses and features were technically evaluated and some possible issues were raised. Let's understand the current layout and its content blocks to explore the possibilities for development.

<img src="/ipc-ig/reports/img/oc-redesign-blocks.png">

The Numbers identify each block configured by *Community Context* under `admin > structure > context > list > community`. There is a different view for each section inside the block labeled as 5, they were grouped considering the layout structure, as they compose an *accordion element*. The hypothesis raised with this analysis are listed bellow:

 **1 - Community's Image Block:** Can be best used as a cover image.
 **2 - Community's name and organizers:** Occupy a tall section, the column is narrow for most community titles, making it even taller.
 **3 - Search community's content:** The search box cannot be seen when the page loads, it requires a page scroll to be displayed. Even on large pages, the position of the element doesn't attract attention.
 **4 - Main Community Menu:** The main menu is not the main structure used for navigating through community's content. It is also not very highligted.
 **5 - Community's secondary menu:** The secondary menu holds a set of important information that can only be accessed after all other not-so-important information.
 **6 - Community's actions:** Shortcuts for important actions are ok.
 **7 - Community's content area:** Main content to br presented is not large enouth.

The image bellow shows the first view of the community when the page loads. Note that all navigation, menus and content are hidden.

<img src="/ipc-ig/reports/img/oc-redesign-first-view.png">

## Usability test

In order to verify if the hypothesis were valid, a usability test was made with the following questions to be answered by each of the ten testers.

### 1 Image block.

  > Comment what you think about the community image, its position, size and how you feel about it.
  
4 out of 10 said words like "not very helpfull", "makes the content harder to read", "hides the menu options". The other 6 said nothig about it. The conclusion is that the image is not the main problem but it interfeers negatively by hiding some more important features, like the search bar and navigation menus.

**Low priority, but can be improved.**

### 2 Title and Organizers

  > How do you evaluate the importance of the information inside the box 2? How do you feel about the disposition of the organizers links?

7 out of 10 said the name is very important, but the links are not very usefull. They didn't understand that the links are related to the organizers, looking more like links to content inside the community, not separate institutions. The other 3 didn't like either, but didn't know how to explain why.

**Medium priority, should be improved.**

### 3 Search community's content

  > Where can you search for specific content inside a community? How do you feel about it?

10 out of 10 could not find the search box initially. They all opted to use the global search and got frustrated, having to go back to the community page and to spend some time looking for how to search for content. Only 4 got it right before 3 minutes in this task.

**High priority, must be improved.**

### 4 Main Community Menu

  > What is the main navigation structure of the page? At first, do you think that all important content are accessible by these options? How do you feel about the menu options?

9 out of 10 believed that all content could be accessed by using the menu links some how. But when requested to look for a specific Comunity Resourse, they could not find it using the menu. The standard option to find community pages was to go through Documents. 5 out of 10 have good impressions about the menu layout, the other 5 didn't comment about it. 10 out of 10 did not understand the numbers on each button.

**High priority, must be improved.**

### 5 Secondary Menu

  > Where should you go to find community infomations, like administrators, working groups and related communities?

9 out of 10 answered they would look for it in About page. After they realize that the about was not a page, they got lost for a moment and then looked for the information on the current page. The other 1 said that could be under the About section, but located the other information before clicking the About.

**High priority, must be improved.**

### Content Area

  > What do you think about the main content area? How do you feel about it?

10 out of 10 thought that it should be large and that the left column makes it harder to access content.

**High priority, must be improved.**

After the test, most of the hypothesis were validated. There is a proposal for redesigning the navigation and content structure of the community's page. There are clearly some issues with Information Architecture and layout priorizations that could make a great difference on user satifasction and success.

## Results & Proposals

After executing the tests and analysing the resposes. Three things were selected as main causes for user frustration:

### Improvements

 **1 - Information Arquitecture Issues:** The content related to the content type fields, like about text, administrators and working groups should be grouped in a single area. The most straightforward way to do it, is to put all this information under an *About* page, where all testers were believing the information should be. The other thing that is missplaced is the Resources area, most of the testers labeled it as the main content area of the community. This should be raised to a page with the list of resources, and inside the page, the list of documents should be accessible. The documents were rarelly accessed of used (this might be related to the persona, all users were new to the platform and didn't have any experience with social protection activities).

 **2 - Navigation issues:** Most testers interpreted the *Accordion element* as a secondary navigation structure and the expectation was that the page were redirected, instead of the content toggle. This confusion were connected to the fact that the main navigation is not good enough. The second navigation structure should be avoided and the main menu should be redesigned to provide the expected results priorizing most accessed content intead of specific features.

 **3 - Priorization of layout elements:** The importance of the menus, search bar, Community name and organizers, photo and content are not being taken into account by the visual structure. The searchbar and menu should be visible on page load to create a feeling of organization and location. The Organizers links are taking a very important space in a valuable area of the page, they should be replaced by linked logos in a secondary area of the page.

New Information Architecture for community page.

<img src="/ipc-ig/reports/img/oc-redesign-info-arch.png">

The main changes are related to including some content into a new page called "About", reducing the number of action buttons on the *accordion element* and groupingsimilar information toghether; and moving the Resources to the main menu, creating an inside section for the documents, that were evaluated as community resources by many of the participants. With these two changes, there is no need for an additional secondary menu and makes the main layout cleaner, also valuing the most important features, such as search and navigation.

### First Iteration

An alternative layout for the main community page that implements these changes can be seen bellow.

<img src="/ipc-ig/reports/img/oc-redesign-layout.png">


For the internal pages, the layout can be redesigned in the form of submenus and separate pages for each content type or information.

**About**

The **About** page will include Administrators and Related Communities, as shown bellow.

<img src="/ipc-ig/reports/img/oc-redesign-admins.png">

The administrators page will be displayed as all standard members pages, side by side with two collumns. The About text will have its own page, some communities have very long descriptions and thus cannot be put inside an *Accourdion element*.

<img src="/ipc-ig/reports/img/oc-redesign-about.png">

**Resources**

The resources page will also include Documents, as shown.

<img src="/ipc-ig/reports/img/oc-redesign-resources.png">

### Second Iteration

There where found some issues about the new layout. The *Resources* are really important and must be visible to a member always to facilitate the navigation between pages inside the community and to access contents fast. The strucuture with the side menu would not hold a set of page links in a simple way that the user might easily understand and access any content inside the community.

We executed a second usability test to collect data about our first redesign option and found the following results:

 - **Community image is still an issue when set as a cover image:** The platform layout defines a limited width and the main menu bar uses a significant portion of the page height available to the first page load view.
 - **The Resources cannot be navigated:** None of the testers felt the need to navigate through different resources, but some of them noticed that they had to go back to the list page to look for other resources.
 - **The Organizers Logos are visualy poluting the page:** Some communities have more than 4 organizers and the available space cannot hold more than 3 logos.
 - **The home content can be unecessary:** Some collaborators indicated that the Community homepage is not very usefull and a block with recent updates can be enouth to show community activities.

The second iteration of the test provided us some inputs to develop a second layout, as follows:

<img src="/ipc-ig/reports/img/oc-redesign-about-v2.png">

The *Actions* dropdown will be replaced by a single action button if the list of actions contains only one option to avoid the overhead of multiple clicks when there's only one link.

The internal pages proposed before will be kept the same, just the Resources will be fixed in the sidebar to give quick access to the articles. If the integration of Etherpad is enabled for the community, we thought about a highlight button so that members could join a live chat, video meeting or document collaboration. Initially, the idea is to keep a single pad per Community, as shown bellow:

<img src="/ipc-ig/reports/img/oc-redesign-resources-v2.png">

The first view after the redesign process wil present all main components of the page, even in notebooks with low resolutions. The image bellow represets a HD screen resolution in 16:9 dimensions.

<img src="/ipc-ig/reports/img/oc-redesign-view-v2.png">

The Etherpad integration page might look like the following, the idea is to give more space to the editor by removing the sidebar and placing only the editor. Another option is to set the editor on a dialog view to provide better usage of the screen while using the Etherpad editor.

<img src="/ipc-ig/reports/img/oc-redesign-pad-v2.png">

# Fixes

The team members have identified a list of issues that must be fixed in order to keep the Online Communities working. Some of them are:

 - Verify content approval for community admins
 - Verify members approval/removal
 - Generate report of member emails to send message directly
 - Set About as default page for anonimous users
 - Check Timezones for events

There are also a list of improvements that can be done to make the OCs even better tools for colaboration like:

 - Tools: Add embed documents for collaboration (similar to google docs)
 - Tools: Add Surveys or Polls to community tools
 - Tools: Embed padlet inside OCs.
 - Discussions and comments must be set on top of the community (comments must reflect the discussion updated date)
 - Add Comment tags for users like (@username) to send notifications
 - Send a notification for users when the admin uploads a document and checks an option "also send via email"
 - Welcome mail for new members configurable by admins
 - Add option for documents to be indexed on social protection database on upload (default to Publications)
<!-- - Add zoom url for each community  -->

This issues were listed in order feasibility and priority combined. They will be handled one by one to be executed meanwhile the current month. The tasks estimated to be concluded are:

 - Verify content approval for community admins
 - Verify members approval/removal
 - Generate report of member emails to send message directly

Also, there are going to be developed tests for the following features:

 - Tools: Add embed documents for collaboration (similar to google docs)
 - Tools: Add Surveys or Polls to community tools
