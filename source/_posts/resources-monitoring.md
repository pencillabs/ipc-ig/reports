---
title: Moniotoring stack
date: 2020-09-08 18:18:01
thumbnail: /img/thumb_pattern_orange.svg
toc: true
categories:
 - Analysis
 - Infrastructure
---

There are some objectives that were initially identified as a motivation to develop a better monitoring system. The platform has only standard monitoring options provided by the infrastructure hosts, machine CPU, Memory, Disk and network traffic. As the platform runs over Docker, each service runs a single container that has its resources usage and limitations, also, the source code needs better monitoring to avoid critical issues to be deployed in production. With it all in mind, the prioritary goals identified are:

 - Analyzing idle resources and optime services distribution;
 - Analyzing source code issues and optimize critical spots;
 - Monitor critical resources and evaluate critical usage spikes;
 - Monitor critical access logs;
 - Ease a data pipeline reformulation for reports;

Some frameworks were evaluated to provide the features:

 - Orchestration: Docker Swarm / Kubernetes
 - Management: Portainer / Swarmpit
 - Monitoring: Grafana & Nagios / Zabbix

The alternatives were connected to provide better support for most of the features with simplicity and cost as priority. They all depend on a clusterization of the infrastructure, which is covered better [here](/ipc-ig/reports/infrastrucutre-cluster).

## Orchestration

The platform already uses a Docker to deploy services. The most simple way to orchestrate containers in a cluster is to enable Swarm mode and fix the compose files of the platform to work with it. The Kubernetes is an alternative, bt the cost to migrate de entire configuration and deploy would be high and could take very long to be delivered. The clusterization of the infrastrucutre will bring the benefits of fully consuming infrastrucutre resources and avoid the need of enlarging virtual machines when there are larger traffic over the platform. The evaluation made collected the following information about the infrastructure.

| Resource |     CPU cores    |  Memory (GB) | Disk Storage |
|:--------:|:----------------:|:------------:|:------------:|
| Available|         10       |       24     |      340     |
|   Used   |    4.23 (42,3%)  | 8.6 (35.8%)  | 160 (47%)    |

Cluster topology for better resource usage is represented bellow. The first image shows how the resources are currently used and how services can be redistributed so that the usage could be balanced between nodes.

<img src="/ipc-ig/reports/img/monitor-cluster-1.png">

The main bottleneck of the current infrastructure is that Social Protection is centralized in a single machine and is limited by the resources that it possesses, While all other machines have available CPU and memory that could be used to avoid unecessary costs and better performance of the platform. The image bellow shows an alternative topology that, in addition to an Orchestrator, could benefit the overall performance.

<img src="/ipc-ig/reports/img/monitor-cluster-2.png">

There are also some benefits for using a decentralized data storage and a Network File System (NFS), as providing regular backups without affecting platoform performance, setup automatic system recovery, and Drupal replicas among different machines.

## Management

The orchestrator should have a simple monitor and manager dashboard to store and give access to containers logs, consoles and health. It also serves as a way to deploy and perform optimizations on resources consumption. The tested management framework was Swarmpit, as shown bellow.

<img src="/ipc-ig/reports/img/monitor-swarmpit.png">

The Swarmpit allows control over all deployed stacks, that will include the IPC-IG website and any other websites that may use the cluster.

## Monitoring

For monitoring, there were two options: Setup a Grafana server, to collect Docker services statistics and a Nagios notification service to monitor and alert service downtime. The cost for development is high and the configurations are made manually, there is no dashboard for Nagios and that is a critical problem. 

The alternative to that stack is Zabbix, a fully featured monitor system with support to Docker agents and collecting docker containers statistics. The Zabbix is compatible with Docker Swarm and has a well documented deployment process. The following image presents a test executed with a Zabbix server to monitor a cluster with 4 machines and a some Docker stacks deployed to it.

<img src="/ipc-ig/reports/img/monitor-zabbix.png">

## Tools

The structure selected consists of using:

 - **Docker Swarm:** Simple implementation and faster delivery. While Kubernetes is a more robust alternative, it requires more resources and a larger amount of time to deliver good results.
 - **Swarmpit:** The Swarmpit cluster management dashboard is far more complete and intuitive than Portainer, and it provides fast resources dashboard that can help for quick fixes.
 - **Zabbix:** Simpler to install and full compatible with Docker Swarm, the Zabbix server will provide better results than Nagios and Grafana with lower implementation effort.

The defined stack will provide the features expected will low implementation costs. Also, it will help evaluate every new developed feature in terms of resource usage and will be a step forward to an [implementation of Continuos Integration and Delivery](/ipc-ig/reports/infrastructure-cluster).
