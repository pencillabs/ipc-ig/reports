---
title: Cron jobs using Elysia Cron
date: 2021-01-19 16:15:07
thumbnail: /img/thumb_pattern_blue.svg
toc: true
categories:
 - Development
---

Cron is a software utility used to schedule tasks. Drupal has a cron manager embedded in its system, that can be accessed by **admin/config/system/cron**.

<img src="/ipc-ig/reports/img/country-profile-example.png">


# Cron hook

Each module can implemente a hook cron, that is run every time the "run" button is clicked in the admin cron page. For example, if I created a module called undp_module i can implement a hook_cron in the file *** undp_module.module *** 

This function below just display a log that the function ran.

```php
function undp_module_cron () {
    watchdog('undp_module', 'hook_cron ran');
}
```

If you want to test that function, you can run it separately in your shell, by using:

```shell
$ make ssh
$ drush php-eval "module_load_install('undp_module'); undp_module_cron();"
```


# Elysa Cron

In order to do more complex stuff, such as implementing different crons that runs in different times, [Elysa Cron](https://www.drupal.org/project/elysia_cron) module was installed. Elysa cron "extends Drupal standard cron, allowing a fine grain control over each task and several ways to add custom cron jobs to your site."

The cron painel, remains on the route **admin/config/system/cron** but now allows much more control and customization, as it is reported in Elysa Cron documentation.

<img src="/ipc-ig/reports/img/elysa-cron-page.png">


In modules, such as UNDP Country Profiles, sometimes just one hook_cron doesn't cover it. And othertimes you just want to run one cron job at that time, so you can "Force run" an individual one or set a individual time for it to run.

In the settings tab of the cron configuration, there are a few options that are important to explain. The section "Click for help and cron rules and script syntax" can be used to consult how to write a cron command. Another way to do so is to use a generator, such as https://crontab-generator.org/.

<img src="/ipc-ig/reports/img/elysa-cron-settings.png">

Another section, which we use, is the "Script" one. Below, there is the format that is used for codes in there.
Where there are asterisks is the place to define how frequent should the script run.

```
# Comment, displayed in the status tab.
 *  *  *  *  * function();
```

In the country profile module, there are some tasks that have to run with different times. We use the same function, ***undp_contry_profiles_cron()*** but passing a different argument each time, because there is a switch declaration to select what command should run.

```
# Search if country has any covid response program once a day
 4 0 * * * undp_country_profiles_cron('countries_covid_response'); 

# Update country profiles indicators once a week
4 0 * * 0 undp_country_profiles_cron('countries_indicators');

 # Create remaining country profiles once a month
4 0 1 * *  undp_country_profiles_cron('create_countries');  

# Populate country profiles currency and flags once a month
4 0 1 * *  undp_country_profiles_cron('countries_flags');
```