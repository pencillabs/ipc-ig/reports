---
title: Online Communities - Integration tools evaluation
date: 2020-08-17 21:17:56
thumbnail: /img/thumb_pattern_orange.svg
toc: true
categories:
 - Analysis
 - Online Communities
---

The Online Communities can integrate a set of tools to provide better collaboration between members. This article structures the process of evaluation and integration tests that were executed in orther to provide the expected features, witch are:

 - Collaborative document edition
 - Realtime document edition
 - Web chat for realtime interaction
 - Video/Audio calls for better communication
 - Full Drupal integration

## Criteria

The criteria used to classify and rate each application were:

 - **Community Support:** The solution must have a considerably large community and be well maintained. Range [1-5]
 - **Ease of implementation:** The application should be easily integrated to Drupal and Social Protection. Range [1-5]
 - **Lower infrastrucutre cost:** The infrastructure cost shold be as lower as possible to avoid generating performance issues. Range [1-5]
 - **Design and theme customizations:** The application should be customizable to provide same *look-and-feel* of the platform. Range [1-5]
 - **Provided features:** The application should provide greater number of functionalities wanted. Range [1-5]

## Alternatives

These features motivated the exploratory search for platforms, plugins and applications that could bring the functionalities to Social Protection. The most promissing tools found were:

 - [Firepad](https://firepad.io/)
 - [OwnCloud](https://owncloud.org/)
 - [Gobby](https://gobby.github.io/)
 - [WebRTC](https://webrtc.org/)
 - [Etherpad](https://etherpad.org/)

### Firepad

The Firepad is a simple tool that provides a realtime collaborative editor for the web. It's very similar to CKEditor or WYSIWYG, but provides multiple user edition at the same time. Their website provides an interactive demo that can be accessed [here](https://demo.firepad.io/).

The Firepad provides 2 aout of the 5 wanted features. There is no webchat or video calls support and the Drupal integration would not be very simple, because it uses its own Rich Text Editor that is not supported by Drupal.

|                Community Support                       |                 Ease of Implementation                 |              Lower infrastructure cost                 |             Design and theme customization             |                       Provided Features                |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|

### OwnCloud

The Firepad is a simple tool that provides a realtime collaborative editor for the web. It's very similar to CKEditor or WYSIWYG, but provides multiple user edition at the same time. Their website provides an interactive demo that can be accessed [here](https://demo.firepad.io/).

The Firepad provides 2 out of the 5 wanted features. There is no webchat or video calls support and the Drupal integration would not be very simple, because it uses its own Rich Text Editor that is not supported by Drupal.

|                Community Support                       |                 Ease of Implementation                 |              Lower infrastructure cost                 |             Design and theme customization             |                       Provided Features                |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|

### OwnCloud

The OwnCloud is a toolset that enables much more than just collaborative document editing, but creates a complete environment for sharing, editing, exporting and chatting. The platform also provides LDAP and access management for files and folder structures. The limitations of the platform are related to customization and infrastructure costs that are critical.

The OwnCloud provides 4 out of the 5 wanted features. There is no support for Drupal integration, since it is a complex set of applications. The integration would also be very expensive to be developed.

|                Community Support                       |                 Ease of Implementation                 |              Lower infrastructure cost                 |             Design and theme customization             |                       Provided Features                |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|

### Gobby

The Gobby platform is a web based server application that is used in addition to a local desktop app to provide realtime text edition. There is no support for live chat nor video or audio calls.

The Gobby provides 2 out of the 5 wanted features. There is no support for Drupal integration and it would also be very expensive to be developed because of the desktop applications that are required.

|                Community Support                       |                 Ease of Implementation                 |              Lower infrastructure cost                 |             Design and theme customization             |                       Provided Features                |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_2.svg">|

### WebRTC

WebRTC is not an application or service, it's a web standard that can be used to provide live Video and audio chatting without the need of a server backend. The standard is supported by all major browsers and can be integrated to Drupal with some community modules.

The WebRTC gives support for 3 out of the 5 features wanted, but does not provide collaborative text edition.

|                Community Support                       |                 Ease of Implementation                 |              Lower infrastructure cost                 |             Design and theme customization             |                       Provided Features                |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_4.svg">|

### Etherpad

Ehterpad is a Rich text editor developed for realtime collaborations, it has a set of very [usefull plugins](https://static.etherpad.org/index.html) that can be integrated to provide a full set of functionalities required by the Social Protection Platform.

The Etherpad gives support for all 5 features wanted and can be easily integrated to Drupal Content-types as a full HTML editor.

|                Community Support                       |                 Ease of Implementation                 |              Lower infrastructure cost                 |             Design and theme customization             |                       Provided Features                |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_5.svg">|

Using the provided metrics, the standard option to be testes into a Drupal environment is Etherpad. To see the full steps for implementation in a development environment, follow [this article](/ipc-ig/reports/2020/08/24/etherpad-integration/).
