---
title: Evaluation Criteria
date: 2020-06-13 18:10:13
thumbnail: /img/thumb_pattern_orange.svg
toc: true
categories:
 - Analysis
---

# Goals

The criteria for analysing each module of the Social Protection platform were defined by taking into account the following goals:
 
## Performance

  > We need to enhance the performance of the overall php code and drupal page renderization pipeline.

Performance is still an issue for authenticated users in Social Protection. When multiple users are logged in simultaneously, this issue also extends to the anonimous users. There were already a set of fixes and fine tunnes for the web server and the implementation of a Varnish cache server on top of the drupal stack, with really good results for anonimous accesses, that can be seen [here](https://gitlab.com/pencillabs/ipc-ig/reports/-/blob/master/PERFORMANCE.md). Now it is necessary to work on a broader solution, that can also benefit authenticated users.

In this context, we listed two options for being evaluated: the first option is to migrate the platform to Drupal 8, that could bring a great number of cache enhancements like [Dynamic Page Cache](https://wimleers.com/article/drupal-8-dynamic-page-cache) and [BigPipe](https://www.drupal.org/docs/8/core/modules/big-pipe), generating a [load time reduction of 60%](https://wimleers.com/blog/drupal-8-page-caching-enabled-by-default) if compared to a Drupal 7 standard installation with all Drupal 8 Core features enabled, even in high consuming resources pages, like search and community pages; the second option is to improve Authcache hits, developing the necessary hooks and policies accordingly to [Authcache documentation](https://www.drupal.org/node/2290611).

The drawbacks of a Drupal 8 migration are the support of the currently installed modules and the community activity over developing new features and rapidly fixing new bugs. On the other hand, the choice of tunning Authcache is risky because it can take a long time to get accurate and Drupal 7 is now approaching its end of life.

## Maintainability

  > We need to better document and easily maintain/attend the platform necessities.

The knowledge related to development processes, used tools, modules, deploy flows, and infrastructure has to be structured and shared. The platform lacks documentation and the knowledge is spread among different sources. The reduction of modules and the use of more community and core modules is a priority for keeping the health and continuation of the system.

The drawbacks here are the cost of migration of the current used modules to community maintained ones and the implications of the removal of unused modules from the source code. Regardless of the choice adopted to mitigate performance issues, the impact on manutenability has to be as lower as possible, with an affordable cost of implementation.

## New development

  > We need to continue developing new features and fixing bugs while migrating the platform.

The development of the platform should not be interrupted for very long, new features are necessary for contracts, and in times of COVID-19, a lot of new functionalities may appear. Also, bugs have to be fixed as soon as possible and shouldn't impact much on the work in progress.

The drawbacks here are related to the amount of time that will be needed to execute the migration or implementation of an Authcache support that does not interfere on the platform flow.

# Criteria

Based on these three goals and considering the differences between the contrib and the custom modules, the criteria were defined as follows.

## Community Modules

For the community contributed modules, the criteria are:

 - Activity: The module's community last udated date, normalized. Range [1 - 5].
 - Code Health: The normalized number of new bugs divided by the number of fixed bugs in the last semester. Range [1 - 5].
 - Bug reports: The normalized number of bugs reported in the last semester. Range [1 - 5].
 - D8 Support: The support of the community to a Drupal 8 production ready version. Range [1 - 5].
 - D7 Authcache support: Module gives support to authcache strategy. Boolean [true/false].
 - Total Score: The average of all provided metrics ignoring **D7 Authcache Support**. Range [1 - 5].

## UNDP Modules 

For the custom modules developed internally, the criteria are:

 - Activity: Last updated date normalized by all modules date range. Range [1 - 5].
 - Simplicity: Number of hooks implemented by the module times average method size, normalized. Range [1 - 5].
 - Cacheable: Verify current cacheability of the module. Boolean [true/false].
 - Authcache support: The complexity to implement Authcache support. Range [1 -5].
 - D8 ease of migration: The ease for migrating the module to Drupal 8. Range [1 - 5].
 - Frequence of editions: The normalized number of editions. Range [1 - 5].
 - Total Score: The average of the provided metrics ignoring **cacheable**. Range [1 -5].

Evaluating only the support for Authcache and the feasibility of implementig the support to the custom modules, the following indicators will be used:

 - Code Health
 - Authcache support
 - Simplicity
 - Cacheability

For evaluating the migration to Drupal 8, all the criteria are relevant.

The badges representing each range score are the following:


<div style="text-align:center">
<img width="50" src="/ipc-ig/reports/img/badge_1.svg">
<img width="50" src="/ipc-ig/reports/img/badge_2.svg">
<img width="50" src="/ipc-ig/reports/img/badge_3.svg">
<img width="50" src="/ipc-ig/reports/img/badge_4.svg">
<img width="50" src="/ipc-ig/reports/img/badge_5.svg">
</div>

Boolean values are defines as Yes or No, as follows:

<div style="text-align:center">
<img width="50" src="/ipc-ig/reports/img/badge_y.svg">
<img width="50" src="/ipc-ig/reports/img/badge_n.svg">
</div>

If any of the criteria does not apply to the module, the X badge is provided:

<div style="text-align:center">
<img width="50" src="/ipc-ig/reports/img/badge_x.svg">
</div>


The higher the score, the better the result.

# Collecting data

To mesure the cummunity contributed modules, the data were collected accessing the following platforms:
 - The official Drupal Projects Repository: https://git.drupalcode.org/projects
 - The official Issues Tracker Platform: https://www.drupal.org/project/issues
 - The project page on Drupal.org: https://www.drupal.org/project/project_module
 - The Drupal 8 contrib modules migration tracker: https://contribkanban.com/board/contrib_tracker

The list of collected data for each contributed module is:
 - Number of opened issues in 2020.
 - Number of closed issues in 2020.
 - Number of commits during 2020.
 - Number of Drupal 8 releases (including dev, unstable and recomended versions).
 - Search for Authcache support registered issues, opened or closed.
 - Versions released vs current used.

The list of collected data for each UNDP Module is:
 - Last update date.
 - Number of hooks and methods.
 - The proper use of Drupal cache flags.
 - Cachehit in authcache statistics.
 - Number of commits.
 - Use of unsupported Drupal 8 hooks and features.
 - Average time ellapsed between commits.

The main source of information for developed modules is the [code repository](https://bitbucket.org/iteliosbrasil/itelios.ipc.socialprotection/) of the project.

The scores attributed to each module can be found [here](/ipc-ig/reports/2020/06/13/modules/).
