---
title: Infrastructure Issues
date: 2023-01-24 15:08:00
thumbnail: /img/thumb_pattern.svg
toc: true
categories:
 - Development
 - Infrastructure
---

The list of errors and mitigation strategies applyed to each of them, as follows:


|   ID   | Date  |         Error          | Mitigation | Correction |
|--------|-------|------------------------|------------|------------|
| SP-320 | 04/21 | High memory/CPU usage | turn off development environments | Fix service placements to balance resourses |
| SP-383 | 05/21 | MySQL slow logs with high disk space usage | Truncate database slow log files | Disabled slow query logs from mysql |
| SP-387 | 05/21 | Solr looses indexeddata on service restart | reindex content on every deploy | Create a docker volume to store Solr indexed data |
| SP-424 | 05/21 | High network data traffic | none | Fix Swarm network interface configurations to use private IPs |
| SP-1204 | 06/22 | NFS server fails to restart because of volume mount error | Mount disk /data mannually to nfs directory | Create a FS rule to mount NFS volume on system startup |
| SP-1270 | 10/22 | Docker fails to initialize after system restart | restart docker service mannually | Monitor machine services to avoid docker errors on restart. The error never occured again. |
| SP-1278 | 10/22 | NFS machine got unavailable for several hours. No backup was available outside the machine | none | Duplicate all backups on different machines to restore the production environment |
| SP-1283 | 10/22 | Administrator profile was unable to access vm 004 | none | Contact Azure support and verify whether the access has been re-established |
| SP-1279 | 10/22 | NFS machine got unavailable for several hours. There was no infrastructure documentation available | none | Generate a complete set of documents to provide information about infrastructure issues and capabilities |
| SP-1303 | 11/22 | High memory and CPU usage on machine 003 | turn of development environments | Reorganize service placements in order to rebalance resourses |
