---
title: Drupal 8 - Initial Migration Report
date: 2020-10-19 10:20:32
thumbnail: /img/thumb_pattern_blue.svg
toc: true
categories:
 - Development
 - Drupal 8
---

According to the analysis previously made and the data collected on the migration of the platform to Drupal 8, a task was initiated to migrate the data and consolidate a viable strategy for updating the platform.

## Migration problems

Some migration problems are already known by the community, and need to be treated with more attention when doing the update:

  - **Plain Text Fields** -> In Drupal 7, the text processing settings are defined on Field instance settings. In other words, the same field can be used on two (or more) content types and the text processing settings can be Plain text on one content type and Filtered text on another. Drupal 8 has separate field storage types Text (plain) and Text (formatted). There's also Text (plain, long) and Text (formatted, long). The important part here is that this selection is done on field storage level. In other words, the plain/formatted selection can't be changed per content type.
  - **Views** -> Views are not migrated, you will need to create the views in Drupal 8 manually.
  - **Translations** -> In Drupal 6 and 7, node translations were stored in different nodes while in Drupal 8 they are now combined with their source language nodes. The migration system will do the merging of the node translations, but that means that some links might now point to nodes that do not exist anymore. In some cases, the database tables may interpret the "language" field as not being the "langcode" field, equivalent in Drupal 8, in which case it is necessary to modify the database schemas so that the equivalence is interpreted in the correct way.
  - **ID conflicts** -> In this case, some errors may occur in the identification of the contents due to their translation. The translation of a content was separated into two different identifiers, and from the version update, they will have the same identifier, which may generate conflicts with other identifiers and identifiers without reference.
  - **URL Aliases** -> When migrating URL aliases for a language that is not enabled on the new Drupal 8 site, none of the aliases will work until you enable the language on the new Drupal 8 website.

## Initial Migration Status

### Modules Status

Before we start the data migration, Drupal reports on which modules will be migrated, and which cannot be migrated automatically. The platform currently has many modules that cannot be migrated automatically, some may be installed manually after the migration, without affecting your data or other installed modules.

Some modules have been integrated into the platform core, these will not need any attention after the process. Other modules, those customized for the UNDP context, will need to undergo a complete reconstruction and adaptation to the Drupal 8 context.

Here are the results of the initial migration of the modules to Drupal 8:

|  Modules upgraded  | Modules not upgraded | Total |
|:------------------:|:--------------------:|:-----:|
|        46          |          200         |  246  |

[Complete report](/ipc-ig/reports/img/migration-modules-report.png)


### Errors Report

During the migration process, some errors were reported by the platform and some problems were also observed, these are limitations of the migration itself and we will have to find ways to be avoided.

#### Translation errors

The migration process failed to identify and migrate items from languages other than English. We suspect that it will be necessary to redo the migration process by previously adding the necessary languages to the new platform. The migration manual does not make it very clear how to proceed in these cases, so we will have to test possibilities to solve the problem.

<img src="/ipc-ig/reports/img/translations-error-report.png">

#### Views

As mentioned above, the migration to Drupal 8 does not cover views. One of the possible solutions to the problem would be to use a migration module [View Migration](https://www.drupal.org/project/view_migration) to solve the problem without having to reimplement all the views again.

#### Content types dependencies

Some Content Types have direct dependencies with some modules that cannot be migrated automatically. Two examples are the Content Type of Forum, and the one of communities. Both have been migrated incompletely because their related modules have not been migrated. One possible approach to solving the problem would be to install these modules beforehand before migrating.

### Migration Results

As previously mentioned, only the English content was migrated at first. That said, we present below some results obtained with the migration. These results show the totality of migrated content, as well as what types of content were migrated and if they have problems. We made a cut with the main types of content of the platform.

*Status of migration of some Content Types:*

|     Content Type      |                       Status                                   |
|:---------------------:|:--------------------------------------------------------------:|
|     Stakeholder       |      Migrated                                                  |
|     Community         |      Not completely migrated because of module dependency      |
|     Publication       |      Migrated                                                  |
|     Jobs              |      Migrated                                                  |
|     News              |      Migrated                                                  |
|     Forum subject     |      Not completely migrated because of module dependency      |

*Percentage of content migrated from the platform:*

|  Content Type  | Nodes Migrated  | Total Nodes | Percentage |
|:--------------:|:---------------:|:-----------:|:----------:|
|   Stakeholder  |      1523       |    1523     |   100%     |
|   Community    |       73        |     80      |   91.25%   |
|   Publication  |       3900      |    6239     |   62.51%   |
|   News         |       3241      |    3593     |   90%      |
|   Jobs         |       1139      |    1262     |   90%      |
 
### Next Steps

The next steps will be to evaluate the integrity of the migration, reviewing the types of content and whether their respective fields have been migrated correctly. In addition, we will seek solutions to the translation problem, thus making new migration tests with some pre-established configurations.

Another important factor is to check the possibility of migrating the views through a plugin, because if it is not possible, we will need to draw a viable strategy to do the manual migration of all the views of the platform, which is very expensive.

We will also test the installation of some modules after the migration process in order to verify if their behavior and settings are similar to those made in the previous version of Drupal.
