---
title: Contrib Modules versions
date: 2020-07-01 11:18:57
thumbnail: /img/thumb_pattern_orange.svg
toc: true
categories:
 - Analysis
---
Versions of local modules and the respective most recent released version.


# Address Field
Current Version:  7.x-1.1
Latest Version: 7.x-1.3

# Administration menu
Current Version:  7.x-3.0-rc5
Latest Version: 7.x-3.0-rc6

# Administration views
Current Version:  7.x-1.6
Latest Version:  7.x-1.7

# Advanced CSS/JS Aggregation (AdvAgg)
Current Version:  7.x-2.33
Latest Version:  7.x-2.34

# Advanced Forum
Current Version:  7.x-2.5
Latest Version: 7.x-2.8

# AJAX Comments
Current Version:  7.x-1.3
Latest Version: 7.x-1.3

# Authcache
Current Version:  7.x-2.2
Latest Version: 7.x-2.3

# Autocomplete Unblock
Current Version: Undefined
Latest Version: 7.x-1.x

# Backup and Migrate
Current Version:  7.x-3.0
Latest Version: 7.x-3.7

# Better Exposed Filters
Current Version:  7.x-3.3
Latest Version: 7.x-3.6

# Better Formats
Current Version:  7.x-1.0-beta1
Latest Version: 7.x-1.0-beta2

# Block Class
Current Version:  7.x-2.3
Latest Version: 7.x-2.4

# Boost
Current Version:  7.x-1.2
Latest Version: 7.x-1.2

# Calendar
Current Version: Undefined
Latest Version: 7.x-3.5

# CAPTCHA
Current Version:  7.x-1.3+3-dev
Latest Version: 7.x-1.7

# Caption Filter
Current Version:  7.x-1.2
Latest Version: 7.x-1.3

# Chosen
Current Version:  7.x-2.0-beta4
Latest Version: 7.x-2.1

# cURL HTTP Request
Current Version:  7.x-1.8
Latest Version: 7.x-1.9

# CKEditor Media
Current Version:  7.x-1.x-dev
Latest Version: 7.x-2.14

# Clientside Validation
Current Version:  7.x-1.41
Latest Version: 7.x-1.47

# Conditional Fields
Current Version:  7.x-3.0-alpha2
Latest Version:  7.x-3.0-alpha2

# Context
Current Version:  7.x-3.6
Latest Version: 7.x-3.10

# Context Entity Field
Current Version:  7.x-1.1
Latest Version:  7.x-1.1

# Countries
Current Version:  7.x-2.3
Latest Version:  7.x-2.3

# Code per Node
Current Version:  7.x-1.7
Latest Version:  7.x-1.7

# Chaos tools
Current Version:  7.x-1.12
Latest Version:  7.x-1.15

# Custom breadcrumbs
Current Version:  7.x-2.0-beta1
Latest Version:  7.x-2.0-beta1

# Custom Publishing Options
Current Version:  7.x-1.4
Latest Version:  7.x-1.4

# Date
Current Version:  7.x-2.8
Latest Version:  7.x-2.10

# Devel
Current Version:  7.x-1.5
Latest Version:  7.x-1.7

# Devel node access
Current Version:  7.x-1.5
Latest Version:  7.x-1.5

# Diff
Current Version:  7.x-3.2
Latest Version:  7.x-3.4

# Domain Registration
Current Version:  7.x-1.3
Latest Version:  7.x-1.3

# Draggableviews
Current Version:  7.x-2.1
Latest Version:  7.x-2.1

# Email
Current Version:  7.x-1.3
Latest Version:  7.x-1.3

# Entity API
Current Version:  7.x-1.6
Latest Version:  7.x-1.9

# Entity tokens
Current Version:  7.x-1.6
Latest Version:  7.x-1.6

# Entity Reference Autocreate
Current Version:  7.x-1.1
Latest Version:  7.x-1.1

# Entity Reference
Current Version:  7.x-1.1
Latest Version:  7.x-1.5

# Entity Translation
Current Version:  7.x-1.0
Latest Version:  7.x-1.1

# Facet API Bonus
Current Version:  7.x-1.1
Latest Version:  7.x-1.3

# Facet API
Current Version:  7.x-1.5
Latest Version:  7.x-1.6

# Fast Token Browser
Current Version:  7.x-1.5
Latest Version:  7.x-1.5

# FE Block
Current Version:  7.x-1.0
Latest Version:  7.x-1.2

# FE Date
Current Version:  7.x-1.0
Latest Version:  7.x-1.0

# FE Nodequeue
Current Version:  7.x-1.0
Latest Version:  7.x-2.2

# FE Profile
Current Version:  7.x-1.0
Latest Version:  7.x-1.0

# Features
Current Version:  7.x-2.5
Latest Version:  7.x-2.11

# Features user role plus
Current Version:  7.x-1.0
Latest Version:  7.x-1.2

# Feed Import
Current Version:  7.x-3.4
Latest Version:  7.x-3.4

# Feeds extensible parser
Current Version:  7.x-1.0-beta1
latest Version:  7.x-1.0-rc1

# Feeds
Current Version:  7.x-2.0-beta1
Latest Version:  7.x-2.0-beta4

# Feeds Tamper
Current Version:  7.x-1.0
Latest Version:  7.x-1.2

# Field Group
Current Version:  7.x-1.4
Latest Version:  7.x-1.6

# Flag actions
Current Version:  7.x-3.6
Latest Version:  7.x-3.9

# Flag
Current Version:  7.x-3.6
Latest Version:  7.x-3.9

# Font Awesome
Current Version:  7.x-2.1
Latest Version:  7.x-3.13

# Google Analytics Counter
Current Version:  7.x-3.3
Latest Version:  7.x-3.4

# Google Analytics
Current Version:  7.x-2.1
Latest Version:  7.x-2.6

# Google Analytics Reports
Current Version:  7.x-3.1
Latest Version:  7.x-3.1

# Internationalization
Current Version:  7.x-1.13
Latest Version:  7.x-1.27

# Menu translation (Node)
Current Version:  7.x-1.x-dev
Latest Version:  OBSOLETE

# Internationalization Views
Current Version:  7.x-3.0-alpha1
Latest Version:  7.x-3.0-alpha1

# Imagecache Actions
Current Version:  7.x-1.9
Latest Version:  7.x-1.12

# Image Link Formatter
Current Version:  7.x-1.1
Latest Version:  7.x-1.1

# Inline Entity Form
Current Version:  7.x-1.8
Latest Version:  7.x-1.9

# Invite
Current Version:  7.x-4.0-beta2
Latest Version:  7.x-4.1-rc1 

# Job Scheduler
Current Version:  7.x-2.0-alpha3
Latest Version:  7.x-2.0

# jQuery Update
Current Version:  7.x-2.7
Latest Version:  7.x-2.7

# Localization update
Current Version:  7.x-2.1
Latest Version:  7.x-2.4

# Language Switcher Dropdown
Current Version:  7.x-2.6
Latest Version:  7.x-2.6

# LESS CSS Preprocessor
Current Version:  7.x-4.0
Latest Version:  7.x-4.0

# Libraries
Current Version:  7.x-2.2
latest Version:  7.x-2.5

# Link
Current Version:  7.x-1.4
Latest Version:  7.x-1.7

# MailChimp
Current Version:  7.x-3.3+8-dev
Latest Version:  7.x-5.6

# Mail System
Current Version:  7.x-2.35
Latest Version:  7.x-3.0-beta1 

# Masked Input
Current Version:  7.x-2.1
Latest Version:  7.x-2.1

# Masquerade
Current Version:  7.x-1.0-rc7
Latest Version:  7.x-1.0-rc7

# Maxlength
Current Version:  7.x-3.0
Latest Version:  7.x-3.3

# Media
Current Version:  7.x-1.5
Latest Version:  7.x-2.26

# Memcache
Current Version:  7.x-1.6
Latest version: 7.x-1.7-rc1

# Menu attributes
Current Version:  7.x-1.0-rc3
Latest Version:  7.x-1.1 

# Metatag
Current Version:  7.x-1.20
Latest Version:  7.x-1.27 

# Module Missing Message Fixer
Current Version:  7.x-1.7
Latest Version:  7.x-1.8

# Multistep Nodeform
Current Version:  7.x-1.5
Latest Version:  7.x-1.5

# Node Save Redirect
Current Version:  7.x-1.3+11-dev
Latest Version:  7.x-1.3

# OAuth2 Server
Current Version:  7.x-1.7
Latest Version:  7.x-1.7

# Organic Groups Bulk Add
Current Version:  7.x-1.3
Latest Version:  7.x-1.3

# Organic groups
Current Version:  7.x-2.7
Latest Version:  7.x-2.10

# Organic groups subgroups
Current Version:  7.x-2.0
Latest Version:  7.x-2.0

# Options element
Current Version:  7.x-1.12
Latest Version:  7.x-1.12

# Pathauto i18n
Current Version:  7.x-1.5
Latest Version:  7.x-1.5

# Pathauto
Current Version:  7.x-1.2
Latest Version:  7.x-1.3

# Profile2
Current Version:  7.x-1.3
Latest Version:  7.x-1.7

# Queue UI
Current Version:  7.x-2.0-rc1
Latest Version:  7.x-2.0-rc1

# Quote
Current Version:  7.x-1.1-beta3+1-dev
Latest Version:  7.x-2.0-rc1

# reCAPTCHA
Current Version:  7.x-2.2
Latest Version:  7.x-2.3

# reCAPTCHA Mailhide
Current Version:  7.x-1.12
Latest Version:  7.x-1.14

# Redirect
Current Version:  7.x-1.0-rc1
Latest Version:  7.x-1.0-rc3

# Reg With Pic
Current Version:  7.x-1.0
Latest Version:  7.x-1.0

# Responsive Favicons
Current Version:  7.x-1.2
Latest Version:  7.x-1.3

# Database search
Current Version:  7.x-1.4
Latest Version:  7.x-1.7

# Search API
Current Version:  7.x-1.14
Latest Version:  7.x-1.26

# Solr search
Current Version:  7.x-1.12
Latest Version:  7.x-1.15

# Select (or other)
Current Version:  7.x-2.22
Latest Version:  7.x-2.24

# Shield
Current Version:  7.x-1.3
Latest Version:  7.x-1.3

# Smart Trim
Current Version:  7.x-1.5
Latest Version:  7.x-1.6

# SMTP Authentication Support
Current Version:  7.x-1.7
Latest Version:  7.x-1.7

# OneAll Social Login, Core
Current Version:  7.x-2.1
Latest Version:  7.x-2.12

# OneAll Social Login, Interface
Current Version:  7.x-2.1
Latest Version:  7.x-2.12

# OneAll Social Login, Widget
Current Version:  7.x-2.1
Latest Version:  7.x-2.12

# Strongarm
Current Version:  7.x-2.0
Latest Version:  7.x-2.0

# Sub-pathauto
Current Version:  7.x-1.3
Latest Version:  7.x-1.3

# Taxonomy term depth
Current Version:  7.x-1.4
Latest Version:  7.x-1.5

# Term Reference Tree
Current Version:  7.x-1.10
Latest Version:  7.x-1.11

# Token
Current Version:  7.x-1.6
Latest Version:  7.x-1.7

# Translation helpers
Current Version:  7.x-1.0
Latest Version:  7.x-1.0

# Ultimate Cron
Current Version:  7.x-2.8
Latest Version:  7.x-2.8

# User Relationships
Current Version:  7.x-1.0-alpha5
Latest Version:  7.x-1.0-alpha6

# UUID Features
Current Version:  7.x-1.0-alpha4
Latest Version:  7.x-1.0-rc2

# Universally Unique ID
Current Version:  7.x-1.0-alpha6
Latest Version:  7.x-1.3

# Variable Check
Current Version:  7.x-1.4
Latest Version:  7.x-1.4

# Variable
Current Version:  7.x-2.5
Latest Version:  7.x-2.5

# Views Bootstrap
Current Version:  7.x-3.1
Latest Version:  7.x-3.2

# Actions permissions (VBO)
Current Version:  7.x-3.3
Latest Version:  7.x-3.6

# Views Bulk Operations
Current Version:  7.x-3.3
Latest Version:  7.x-3.6

# Views Conditional
Current Version:  7.x-1.3
Latest Version:  7.x-1.3

# Views Data Export
Current Version:  7.x-3.2
Latest Version:  7.x-3.2

# Views PHP
Current Version:  7.x-1.0-alpha3
Latest Version:  7.x-1.0-alpha3

# Views
Current Version:  7.x-3.11
Latest Version:  7.x-3.24

# Views UI
Current Version:  7.x-3.11
Latest Version:  7.x-3.24

# Webform Localization
Current Version:  7.x-4.10
Latest Version:  7.x-4.14

# Webform Share
Current Version:  7.x-1.2
Latest Version:  7.x-1.3

# Webform
Current Version:  7.x-4.8
Latest Version:  7.x-4.23

# Word Filter
Current Version:  7.x-1.0+33-dev
Latest Version:  7.x-1.0+33-dev

# X Autoload
Current Version:  7.x-5.7
Latest Version:  7.x-5.8

# xhprof
Current Version:  7.x-1.0-beta3
Latest Version:  7.x-1.0-beta3
