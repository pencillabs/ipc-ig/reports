---
title: Notification Modules
date: 2020-09-18 15:15:19
thumbnail: /img/thumb_pattern_orange.svg
toc: true
categories:
 - Analysis
 - Notifications
---

Notifications are critical to delivery user feedbacks and keep collaboration flow on a community content. The platform uses a set of custom developed modules which handle the dispatch of email notifications, but there are some issues related to the main module that forced it to be disabled.

## Module Structure

The figure bellow shows in a simplified diagram the structure of notification modules currently used.

<img src="/ipc-ig/reports/img/notifications-structure.png">

Each color in the diagram represents a set of functions and configurations that are specific and includes a level of complexity to the project.

### Notifications Core

The core of the module represents the greater part of its features, it is responsible for great part of the monitoring functions of user settings, content and delivery of notifications. It also uses a set of more specific modules responsible to configure content specific notifications. Each entity that has a notification, has a custom module implemented to it. Underneath the notifications layer, the module uses some cron job definitions and some libraries that do the hard work. THe notifications core also includes some contexts and UI elements that are used by user notifications page.

The setup of custom email contents, like adding the body of a node and some other fields to the email, would require the implementation of a separate email builder for each entity type. Also, the notifications core is very complex and has some issues could not be tracked to the source or replicated effectivelly.

### Notifications Entities

Each notification entity has a submodule that is responsible for handling a content update, creation or deletion that will trigger a new notification. These modules are small sets of hooks that handles notification creations for that specific type. This architecture creates a great number of hooks being called on every content update, what is a clear bottleneck for performance updates.

### Contexts and UI

The UI of the notifications module is not well defined and is required by the UNDP Search, because the notifications bar icon block is defined inside a context element of the search module. This is a problem because the module cannot be removed or disabled without interfeering on the platform layout.

### Cron job definitions

The cron definitions are functions that are called when the module is installed to setup a schedue of monitoring for newer notifications. This sctructure allows the module to define when Drupal will enqueue notifications for delivery, the problem with the approach is that the notifications will always be delayed by at least an hour, what creates a gap between collaborators that are trying to communicate in a discussion or comment.

### Included Libraries

The libraries used by the module are php files that doesn't seem to be written by the team. They are similar to some third party modules that were imported to simplify the development process. It is recomended to discard this files and implement only the methods that are really necessary for the module to work.

## Analysis

The process to evaluate all notification features included the steps:

 - Deep code analysis of the module, taking into account the [same criteria](/ipc-ig/reports/2020/06/13/evaluation-criteria/) used in modules analysis, but specific for each source file.
 - Instalation and configuration of an email logger module, the one used was [MailLog](https://www.drupal.org/project/maillog).
 - Development of automated [scripts](https://gitlab.com/pencillabs/ipc-ig/reports/-/tree/master/notifications/testcases) to execute notification operations such as comments and node criations.
 - Analysis of results and register of most significant impressions, as follows.

## Bugs and code *smells*

The module has many [code smells](https://en.wikipedia.org/wiki/Code_smell) that are very hard to trace and fix. Since the platform has no tests for custom implemented modules, it gets even harder to mitigate and track issues that are related to backend code. Some of the problems found are listed bellow:

 - Possible copy/pasted code from community modules;
 - Maintainance and evolution difficulty;
 - Contrived complexity, making the code very hard to understand;
 - Lack of documentation;
 - Feature envy, indicating that the module uses many methods from other files;
 - Large unused and commented lines of code, makes it hard to debug and fix possible issues;
 - Instability under heavy usage, when cron takes larger time to enqueue notifications, it can occur duplicities and strange behaviors with notification content.

The problems listed are critical and there are simple and more effective solutions to handle email notifications using common used modules, the next topic presents alternatives.

## Alternatives

There are [some threads](https://groups.drupal.org/node/42688) on the official Drupal platform discussing implementations of mailing to [Organic Groups](http://drupal.org/project/og), the main alternatives are:

 - [Message Stack](https://www.drupal.org/node/2180145): That includes the modules Message, Message Notify, and Message Subscribe. They can provide any notification system integration to Drupal, like SMS, Email and Push notifications. It's also the [recommended stack](https://www.gizra.com/content/og-message-stack-drupal8/) for handling notifications in Organic Groups for latest versions of Drupal 7 and 8.
 - [Notifications](http://drupal.org/project/notifications) + [Messaging](http://drupal.org/project/messaging): This alternative was listed as more stable, but it has fallen out of use and is currently not ported to Drupal 8+.

Any of these solutions can be integrated to [Mail comment](http://drupal.org/project/mailcomment) + [Mailhandler](http://drupal.org/project/mailhandler) to allow users to answer threads by email. Despite all that, this solution is not compatible with Drupal 8+ and is not going to be available for very long. In several websites that used the Mail comment features, they are being replaced by chat + push notification setups, such as [DrupalChat](https://www.drupal.org/project/drupalchat) and [Push Notifications](https://www.drupal.org/project/web_push_notification) of simply being disabled/removed.

## Conclusion

The analisys has shown that the custom notification module cannot be used as an alternative to sending and receiving email notifications. There is no administrative UI and the settings are confusing. There is no documentation and it has a large number of issues that are hard to track and fix.

The community provides better and more robust solutions, the Organic Groups module currently uses the Message Stack to provide notifications, but that would require an upgrade of the OG module to support it.

The integration of notification features with email threads and replies are not being used by websites anymore, most of the platforms that once used mailing lists are now migrating to realtime chatting with push notifications. Instead of spending resources with the implementation of email replies and mentions on comments, the communities should provide better web chatting tools.

A possible roadmap for accomplishing the upgrade of the module is:

 - Update Organic Groups module to latest version (with Message Stack)
 - Migrate notifications settings for each content and member
 - Develop notifications Panel for header, Content Page and hooks
 - Refactor UNDP Search to remove any reference of the UNDP Notifications module
 - Remove UNDP Notifications Module
 - Develop tests for Notifications and Search Modules

This process would require a large amount of time to be spent in processes that are not directly related to notifications, like refactoring code and upgrading outdated modules and will be the bigger source code change that happens to the platform in many years. A secondary benefit of this change, is that it will start the process of preparation for migrating to a Drupal 8+ version of the platform, that will also use a Message Stack and the OG module can be upgraded with lower effort. A complete list of use cases can be seen [here](https://www.drupal.org/node/2180149).

The analysis of an implementation of realtime web messaging for communities is ongoing [here](/ipc-ig/reports/).
