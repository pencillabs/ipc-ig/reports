---
title: Getting started
date: 2020-06-02 14:38:55
thumbnail: /img/thumb_pattern_gray.svg
toc: true
categories:
 - General
 - Information
---
Welcome to [Social Protection Wiki](https://pencillabs.gitlab.io/ipc-ig/reports/)! Check out the [documentation](/about) for more info. If you find something that is not very clear, ask us on [Gitlab](https://gitlab.com/pencillabs/ipc-ig/reports/-/issues).

## Quick Start

TODO: Add some content about getting started with the social protection platform

### Development and IT

TODO: Add some content about technologies, practices and skills needed for the development and maintainance of the Social Protection platform.

### Glossary

TODO: Add some content about terms and definitions that are commonly used on the platform and its collaborators.

### History

TODO: Add some content about the platform's history and its current moment.

## More Info

More info: [Instructional content](/about)
