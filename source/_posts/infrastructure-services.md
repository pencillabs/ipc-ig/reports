---
title: Infrastructure Services
date: 2022-11-12 15:08:00
thumbnail: /img/thumb_pattern.svg
toc: true
categories:
 - Development
 - Infrastructure
---

The complete list of services currently provided by the infrastrucutre are separated into 6 stacks, as follows:

## Base
Stack that provides base infrastructure services:
  - **Nginx:** Responsible for managing certificates and configurations for serving the base stack applications
  - **Registry:** Custom Docker image registry that securely stores all versioned application images of the entire cluster.
  - **Webhook:** Responsible for capturing new commits and triggering continuous integration tests and automated deploys. This service runs outside the docker in the main machine.
  - **Portainer:** Application that manages and monitors stacks and services deployed
  - **Swarmpit (deprecated):** Application that manages stacks and services deployed
  - **Agents:** Small monitor tasks that collect Docker data for Portainer dashboards. These agents are started on every machine included in the cluster and cannot be placed for specific nodes.

## Zabbix
Infrastructure resource manager and monitor. The zabbix stack runs as separe compose to avoid issues when cluster suffers of downtime.
  - **Zabbix App:** Web application that displays dashboards and controls over all registered machines.
  - **Zabbix Server:** Zabbix webserver that stores and captures data collected by Zabbix agents, handles hooks and notifications.
  - **Zabbix MySQL:** Stores all zabbix applications' data.
  - **Zabbix Agents:** Every machine runs a zabbix agent that colects usage data such as CPU, Disk I/O, Disk usage, Memory and more.

## Reverse Proxy
Global Nginx instance that works exclusively as a reverse proxy for other services running on the cluster.
  - **Nginx:** Nginx reverse proxy. To avoid increasing network latency, the proxy is executed through tunneling.

## Socialprotection
All SocialProtection.org services
  - **Nginx:** Social Protection internal nginx service that manages configurations and log setting for all SocialProtection.org applications.
  - **Drupal php:** php-fpm server that runs Drupal instance. This service is replicated in 2 machines to increase delivery rate and availability.
  - **MySQL:** Database that handles socialprotection.org's data.
  - **Varnish:** In-memory webcache application to speedup anonymous requests.
  - **Solr:** Index and search engine used by Drupal.
  - **Memcached:** Application query cache used to store common database query to improve performance

## IPC-IG Website
All ipcig.org services
  - **Nginx:** Internal nginx service that manages configurations and log setting for all ipcig.org applications.
  - **Drupal php:** Drupal 8.x instance
  - **MariaDB:** Database that handles ipcig.org's data.
  - **Solr:** Index and search engine used by Drupal.

## Content Classifier
The AI-powered content classifier of SocialProtection.org
  - **Flask server:** Simple REST APIs for sendind and receiving new classifications
  - **Redis server:** Queue application to handle multiple classification requests
  - **MySQL:** Database that stores classification data


# Settings & Placements 

Every stack has a development and a production envorionments. The development environment serves for testing and homologation processes. After automated testing and manual testing, the deploy of new features is triggered for production.

## Machines

The cluster is composed of 4 machines, two of them with 8GB RAM and the other two with 16GB RAM. The current cluster has its main issue in memory availability, most of the services are high memory usage applications, such as php (Drupal), MySQL, MariaDB and Varnish. there are three labels that define in which machines a service can be placed and also a replication set, to define which service can be scaled up. The following image shows each service and their placements on each virtual machine.

<img src="/ipc-ig/reports/img/cluster_vis.png">

The high resources machines are provided with the label "sporg", so that any socialprotection.org service can be setup in any of these machines. The "main" label is defined on the NFS server machine, the Reverse Proxy is placed in this machine to avoid latency serving static files. The other machine has the "ipcig" label, that is responsible for all IPC-IG websites services for both development and production environments. The monitor services are placed in the IPC-IG machine because it is the most stable machine with less usage spikes.

the machines are the following:

   SPORG / WORKER  -- 001: 20.64.178.16  / 10.0.0.4
   SPORG / MANAGER -- 002: 20.57.128.129 / 10.0.0.5
   NFS / MANAGER   -- 003: 20.64.176.50  / 10.0.0.6
   IPCIG / MONITOR -- 004: 20.57.128.11  / 10.0.0.7

## Placements

Each service has a placement that defines in which nodes (or virtual machines) it can be initiated. The approach used here is to define the placement by node labels. Any socialprotection production service can be placed on any of the two SPORG machines The development services can be executed only on SPORG WORKER node. This provides a simple way to create highly available services without the need to setup a complex [service mesh](https://avinetworks.com/glossary/kubernetes-service-mesh/).

The following table describes the placements for each service and their stacks.

| Stack | Environment | Service | Placement  | Replicas |
|-------|-------------|---------|------------|----------|
| Proxy | production  |  Nginx  |    NFS     |    1x    |
|                                                       |
| Base  | production  |  Nginx  |    NFS     |    1x    |
| Base  | production  | Registry|    NFS     |    1x    |
| Base  | production  |Portainer|    NFS     |    1x    |
|                                                       |
| Zabbix| production  |   Web   |   IPCIG    |    1x    |
| Zabbix| production  | Server  |   IPCIG    |    1x    |
| Zabbix| production  |  MySQL  |   IPCIG    |    1x    |
|                                                       |
| SPorg | production  |   PHP   |   SPORG    |    2x    |
| SPorg | production  |  MySQL  |    NFS     |    1x    |
| SPorg | production  | Varnish |   SPORG    |    1x    |
| SPorg | production  |   Solr  |   SPORG    |    1x    |
| SPorg | production  |  Nginx  |   SPORG    |    1x    |
| SPorg | production  |Memcached|   SPORG    |    1x    |
|                                                       |
| SPorg | development |   PHP   |   SPORG    |    1x    |
| SPorg | development |  MySQL  |SPORG WORKER|    1x    |
| SPorg | development |   Solr  |   SPORG    |    1x    |
| SPorg | development |  Nginx  |   SPORG    |    1x    |
| SPorg | development |Memcached|   SPORG    |    1x    |
|                                                       |
| IPCIG | production  |   PHP   |   IPCIG    |    1x    |
| IPCIG | production  | MariaDB |    NFS     |    1x    |
| IPCIG | production  |  Nginx  |   IPCIG    |    1x    |
| IPCIG | production  |   Solr  |   IPCIG    |    1x    |
|                                                       |
| IPCIG | development |   PHP   |   IPCIG    |    1x    |
| IPCIG | development | MariaDB |    NFS     |    1x    |
| IPCIG | development |  Nginx  |   IPCIG    |    1x    |
| IPCIG | development |   Solr  |   IPCIG    |    1x    |
|                                                       |
| Class.| development |  Flask  |SPORG WORKER|    1x    |
| Class.| development |  MySQL  |SPORG WORKER|    1x    |
| Class.| development |  Redis  |SPORG WORKER|    1x    |

Some Stacks do not have a development environment due to its nature. Base, Reverse Proxy and Zabbix are not Application Stacks that are evolved and tested. They are ment for the maintenance of the Cluster itself and should not be tested on a production environment. The cluster repository has all the configurations needed for running a local cluster on your machine in order to test Infrastructure settings without using the production cluster.
