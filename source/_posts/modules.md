---
title: Modules
date: 2020-06-13 21:11:26
thumbnail: /img/thumb_pattern_orange.svg
toc: true
categories:
 - Analysis
 - Drupal 8
---

This post presents the complete list of modules used by the platform. For UNDP Modules, it presents a brief information on health, complexity, updates and possible costs with a migration to Drupal 8 or development of Authcache support. For the Contrib Modules installed and not maintained by the Social Protection team, there will be different criteria, such as number of unresolved issues, community activity, bug fixing and support for Drupal 8 or Authcache. The criteria for each of these items are better explained [here](/ipc-ig/reports/2020/06/13/evaluation-criteria/).

# External Modules (Contrib)

The platform includes a great number of modules installed that are maintained by the community and depends on them to keep functioning well. Here they'll be presented and evalluated based on the stablished criterias. The core modules will be ignored in this analysis.

## Address Field

Address Field defines a new field type to store international postal addresses. [Official Page](https://www.drupal.org/project/addressfield) - Drupal 8 Equivalent module is [Address](https://www.drupal.org/project/address).

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_5.svg">|

## Administration views

Replaces administrative overview/listing pages with actual views for superior usability. [Official Page](https://www.drupal.org/project/admin_views) - Drupal 8 **included in Core Modules**.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_4.svg">|

## Administration Menu

Provides a theme-independent administration interface (aka. navigation, back-end). [Official Page](https://www.drupal.org/project/admin_menu) - Drupal 8 equivalent module is **Admin Toolbar (Core Module)**.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_2.svg">|

## Advanced JS/CSS Aggregation

AdvAgg allows you to improve the frontend performance of your site. [Official Page](https://www.drupal.org/project/advagg) - Drupal 8 compatible with stable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_5.svg">|

## Advanced Forum

Advanced Forum builds on and enhances Drupal's core forum module. [Official Page](https://www.drupal.org/project/advanced_forum) - Drupal 8 porting stopped in 2016.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_n.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_2.svg">|

## Ajax Comments

Provides ajax comments to Drupal sites (commenting like a social networking sites. [Official Page](https://www.drupal.org/project/ajax_comments) - Drupal 8 release candidate.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_4.svg">|

## Authcache

The Authcache module offers page caching for both anonymous users and logged-in authenticated users. [Official Page](https://www.drupal.org/project/authcache) - Drupal 8 equivalent [Dynamic Page Cache Core Module](https://www.drupal.org/docs/8/core/modules/dynamic-page-cache/overview).

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|

## Backup Migrate

Back up and restore your Drupal MySQL database, code, and files or migrate a site between environments. [Official Page](https://www.drupal.org/project/backup_migrate) - Drupal 8 stable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_5.svg">|

## Better Exposed Filters

The Better Exposed Filters module replaces the Views' default single- or multi-select boxes with radio buttons or checkboxes, respectively. [Official Page](https://www.drupal.org/project/better_exposed_filters) - Drupal 8 release candidate.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_y.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_4.svg">|

## Better Formats

Better formats is a module to add more flexibility to Drupal's core input format system. [Official Page](https://www.drupal.org/project/better_formats) - Drupal 8 alternative [Allowed Formats](https://www.drupal.org/project/allowed_formats).

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_n.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_2.svg">|

## Block Class

Block Class allows users to add classes to any block through the block's configuration interface. [Official Page](https://www.drupal.org/project/block_class) - Drupal 8 stable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_5.svg">|

## Calendar

Display any Views date field in calendar formats, including CCK date fields, node created or updated dates. [Official Page](https://www.drupal.org/project/calendar) - Drupal 8 release candidate.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|

## Captcha

The CAPTCHA module provides a challenge-response test to determine whether the user is human when facing a web form on a Drupal site. [Official Page](https://www.drupal.org/project/captcha) - Drupal 8 stable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_n.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_4.svg">|

## Caption Filter

This module provides a very simple caption input filter that can be used to attach captions to an image or embeddable object. [Official Page](https://www.drupal.org/project/caption_filter) - Drupal 8 **included in Core Modules**.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_5.svg">|

## Chaos Tools

This suite is primarily a set of APIs and tools to improve the developer experience. [Official Page](https://www.drupal.org/project/ctools) - Drupal 8 stable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_4.svg">|

## Chosen

Chosen uses the Chosen jQuery plugin to make your \<select\> elements more user-friendly. [Official Page](https://www.drupal.org/project/chosen) Drupal 8 stable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_5.svg">|

## Ckeditor Media

Media CKEditor provides a bridge between Media and the stand-alone CKEditor module, allowing files to be embedded within a textarea using the media browser. [Official Page](https://www.drupal.org/project/media_ckeditor) - Drupal 8 not compatible.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_2.svg">|

## Conditional Fields

Define dependencies between fields based on their states and values. [Official Page](https://www.drupal.org/project/conditional_fields) - Drupal 8 release candidate.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_n.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|

## Context

Context allows you to manage contextual conditions and reactions for different portions of your site. [Official Page](https://www.drupal.org/project/context) - Drupal 8 release candidate.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_y.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|

## Context Entity Field

This module creates a condition for Context. The condition causes a reaction if an entity (e.g. a node, user, ...) contains a field with a specific value. [Official Page](https://www.drupal.org/project/context_entity_field) - Drupal 8 unstable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|

## Countries

This module provides many country related tasks. [Official Page](https://www.drupal.org/project/countries) - Drupal 8 porting stopped in 2017.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_2.svg">|

## Code Per Node (CPN)

Code per Node makes it possible to add custom CSS and Javascript per node, per content type, per block, and globally. [Official Page](https://www.drupal.org/project/cpn) - Drupal 8 not supported.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_2.svg">|

## Custom Publishing Options

This module allows you to create custom publishing options for nodes. [Official Page](https://www.drupal.org/project/custom_pub) - Drupal 8 unstable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_y.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_2.svg">|

## Date API

This package contains both a flexible date/time field type Date field and a Date API that other modules can use. [Official Page](https://www.drupal.org/project/date) - Drupal 8 **included in Core Modules**. 

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_y.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_5.svg">|

## Diff 

This module adds a tab for sufficiently permissioned users that shows all revisions of an Entity. [Official Page](https://www.drupal.org/project/diff) - Drupal 8 stable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|

## Domain Registration

An extremely light weight module which either allows or restricts registration on your site based on the persons email address domain. [Official Page](https://www.drupal.org/project/domain_registration) - Drupal 8 stable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_5.svg">|

## Draggable Views

DraggableViews makes rows of a view "draggable" which means that they can be rearranged by Drag'n'Drop. [Official Page](https://www.drupal.org/project/draggableviews) - Drupal 8 stable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_y.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_5.svg">|

## Email

This module provides a field type for email addresses. [Official Page](https://www.drupal.org/project/email) - Drupal 8 **included in Core Modules**.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_4.svg">|

## Entity API

This module extends the entity API of Drupal core in order to provide a unified way to deal with entities and their properties. [Official Page](https://www.drupal.org/project/entity)- Drupal 8 **included in Core Modules**.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_y.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|

## Entity Reference

Provides a field type that can reference arbitrary entities. [Official Page](https://www.drupal.org/project/entityreference) - Drupal 8 **included in Core Modules**.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_y.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|

## Entity Reference Autocreate

Provides a simple way to create nodes on the fly in order to refer to them via entityreference. [Official Page](https://www.drupal.org/project/entityreference_autocreate) - Drupal 8 not supported.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_1.svg">|

## Entity Translation

Allows (fieldable) entities to be translated into different languages, by introducing entity/field translation for the new translatable fields capability. [Official Page](https://www.drupal.org/project/entity_translation) - Drupal 8 **included in Core Modules**.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_y.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_4.svg">|

## Facet API

The Facet API module allows site builders to easily create and manage faceted search interfaces. [Official Page](https://www.drupal.org/project/facetapi) - Drupal 8 equivalent [Facets](https://www.drupal.org/project/facets).

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_y.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|

## Facet API Bonus

Facet API Bonus for Drupal 7 is a collection of additional Facet API plugins and functionality, foremost filter and dependency plugins – And a place to collect more additional Facet API extensions. [Official Page](https://www.drupal.org/project/facetapi_bonus) - Drupal 8 not ported in favor of [Facets](https://www.drupal.org/project/facets).

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_y.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|

## Fast Token Browser

Extends the Token module with a faster and more usable interface for browsing and inserting Tokens. [Official Page](https://www.drupal.org/project/fast_token_browser) - Drupal 8 not supported.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|

## Features

Provides a UI and API for taking different site building components from modules with exportables and bundling them together in a single feature module. [Official Page](https://www.drupal.org/project/features) - Drupal 8 stable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_5.svg">|

## Features Extra

Provides faux exportables (via Features) of several site-building components. [Official Page](https://www.drupal.org/project/features_extra) - Drupal 8 **included in Core Modules**.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|

## Features User Role Plus

This module allows you to export a user role with all enabled permissions. [Official Page](https://www.drupal.org/project/features_user_role_plus) - Drupal 8 not supported.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_2.svg">|

## Field Group

Fieldgroup will, as the name implies, group fields together. All fieldable entities will have the possibility to add groups to wrap their fields together. [Official Page](https://www.drupal.org/project/field_group) - Drupal 8 stable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|

## Flag

Flag is a flexible flagging system that is completely customizable by the administrator. [Offial Page](https://www.drupal.org/project/flag) - Drupal 8 unstable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_2.svg">|

## Fontawesome

Font Awesome Icons. [Official Page](https://www.drupal.org/project/fontawesome) - Drupal 8 stable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_5.svg">|

## Google Analytics

Adds the Google Analytics web statistics tracking system to your website. [Official Page](https://www.drupal.org/project/google_analytics) - Drupal 8 stable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_5.svg">|

## Google Analytics Counter

The Google Analytics Counter module is a scalable, lightweight page view counter which stores data collected by Google Analytics API in Drupal. [Official Page](https://www.drupal.org/project/google_analytics_counter) - Drupal 8 unstable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_4.svg">|

## Google Analytics Reports

Google Analytics Reports module provides graphical reporting of your site's tracking data. [Official Page](https://www.drupal.org/project/google_analytics_reports) - Drupal 8 unstable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_5.svg">|

## Internationalization (i18n)

This is a collection of modules to extend Drupal core multilingual capabilities and be able to build real life multilingual sites. [Official Page](https://www.drupal.org/project/i18n) - Drupal **included in Core Modules**.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_y.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_5.svg">|

### i18n Menu Node

Allows a single node menu item to point to different node translations belonging to the same translation set depending on the current language. [Official Page](https://www.drupal.org/project/i18n_menu_node) - Drupal 8 not supported.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_n.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_1.svg">|

### i18n Views

Translate views using Internationalization. [Official Page](https://www.drupal.org/project/i18nviews) - Drupal 8 **included in Core Modules**.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_n.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_1.svg">|

## ImageCache Actions

This module provides a suite of additional image effects that can be added to image styles. [Official Page](https://www.drupal.org/project/imagecache_actions) - Drupal 8 equivalent module is [Image Effects](https://www.drupal.org/project/image_effects).

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_5.svg">|

## Image Link Formatter

This module is the result of the discussions around a requested feature to allow an image field to be displayed with a link to a custom URL. [Official Page](https://www.drupal.org/project/image_link_formatter) - Drupal 8 unstable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_2.svg">|

## Inline Entity Form

Provides a widget for inline management (creation, modification, removal) of referenced entities. [Official Page](https://www.drupal.org/project/inline_entity_form) - Drupal 8 release candidate.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_4.svg">|

## JQuery Update

Upgrades the version of jQuery in Drupal core to a newer version of jQuery. [Official Page](https://www.drupal.org/project/jquery_update) - Drupal 8 **included in Core Modules**.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_4.svg">|

## Localization Update (l10n)

utomatically downloads and updates your translations by fetching them from localize.drupal.org or any other Localization server. [Official Page](https://www.drupal.org/project/l10n_update) - Drupal 8 **included in Core Modules**.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_4.svg">|

## Language Dropdown

Provides a block using the Dropbutton element to switch site language. [Official Page](https://www.drupal.org/project/dropdown_language) - Drupal 8 stable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_y.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_5.svg">|

## LESS CSS Preprocessor

This is a preprocessor for LESS files. [Official Page](https://www.drupal.org/project/less) - Drupal 8 port stopped in 2018.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|

## Libraries

The common denominator for all Drupal modules/profiles/themes that integrate with external libraries. [Official Page](https://www.drupal.org/project/libraries) - Drupal 8 release candidate.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|

## Link

Provides a standard custom content field for links. [Official Page](https://www.drupal.org/project/link) - Drupal 8 **included in Core Modules**.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_y.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_5.svg">|

## Mailchimp

This module provides integration with Mailchimp, a popular email delivery service. [Official Page](https://www.drupal.org/project/mailchimp) - Drupal 8 stable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_5.svg">|

## Mail System

Provides an Administrative UI and Developers API for managing the used mail backend/plugin. [Official Page](https://www.drupal.org/project/mailsystem) - Drupal 8 stable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_5.svg">|

## Media API

The Media module provides an extensible framework for managing files and multimedia assets. [Official Page](https://www.drupal.org/project/media) - Drupal 8 **included in Core Modules**.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_4.svg">|

## Memcache

rovides integration between Drupal and Memcached with the API for using Memcached and the PECL Memcache or Memcached libraries and Memcache backends.[Official Page](https://www.drupal.org/project/memcache) - Drupal 8 stable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_4.svg">|

## Menu Attributes

This simple module allows you to specify some additional attributes for menu items such as id, name, class, style, and rel. [Official Page](https://www.drupal.org/project/menus_attribute) - Drupal 8 unstable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_y.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_2.svg">|

## Metatag

Automatically provides structured metadata, aka "meta tags", about a website. [Official Page](https://www.drupal.org/project/metatag) - Drupal 8 stable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_y.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_5.svg">|

## Module Missing Message Fixer

This module displays a list of missing modules that appear after the Drupal 7.50 release and lets you fix the entries. [Official Page](https://www.drupal.org/project/module_missing_message_fixer) - Drupal 8 stable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_5.svg">|

## Multistep Nodeform (msnf)

For each step you can define which fields should be included in this step. [Official Page](https://www.drupal.org/project/msnf) - Drupal 8 already supports ajax forms, see a [Multi Step Form Example](https://www.drupal.org/project/ms_ajax_form_example).

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_2.svg">|

## OAuth2 Server

Provides OAuth2 server functionality based on the oauth2-server-php library. [Official Page](https://www.drupal.org/project/oauth2_server) - Drupal 8 release candidate.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|

## Organic Groups

Allow users to create and manage their own 'groups'. Each group can have subscribers, and maintains a group home page where subscribers communicate amongst themselves. [Official Page](https://www.drupal.org/project/og) - Drupal 8 release candidate.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_n.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|

## Organic Groups Bulk Add Users

OG Bulk Add Users is a module to easily add users to an Organic Group group. [Official Page](https://www.drupal.org/project/og_bulkadd) - Drupal 8 not supported.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_2.svg">|

## Options element

Provides a better mechanism to specify select list, checkbox, and radio button options. [Official Page](https://www.drupal.org/project/options_element) - Drupal 8 unstable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_2.svg">|

## Pathauto

Automatically generates URL/path aliases for various kinds of content (nodes, taxonomy terms, users) without requiring the user to manually specify the path alias. [Official Page](https://www.drupal.org/project/pathauto) - Drupal 8 stable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_5.svg">|

## Pathauto I18n (!)

Provides functionality to create aliases for each language. [Official Page](https://www.drupal.org/project/pathauto_i18n) - Drupal 8 not supported yet.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_1.svg">|

## Profile2

Provides a new fieldable 'profile' entity. [Official Page](https://www.drupal.org/project/profile2) - Drupal 8 equivalent module is [Profile](https://www.drupal.org/project/profile).

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_n.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|

## Queue UI

A user interface to viewing and managing Drupal queues created via the Queue API which began in Drupal 7. [Official Page](https://www.drupal.org/project/queue_ui) - Drupal 8 stable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_4.svg">|

## Quote

Adds a 'quote' link below nodes and comments. When clicked, the contents of the node or comment are placed into a new comment form. [Official Page](https://www.drupal.org/project/quote) - Drupal 8 release candidate.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_4.svg">|

## Recaptcha

Uses the Google reCAPTCHA web service to improve the CAPTCHA system. [Official Page](https://www.drupal.org/project/recaptcha) - Drupal 8 stable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_5.svg">|

## Redirect 

Provides the ability to create manual redirects and maintain a canonical URL for all content, redirecting all other requests to that path. [Official Page](https://www.drupal.org/project/redirect) - Drupal 8 stable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_4.svg">|

## Rigister With Picture

Exposes the 'upload picture' element to the registration form. It also gives you the ability to set a picture as required in the module settings. [Official Page](https://www.drupal.org/project/reg_with_pic) - Drupal 8 not supported.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_y.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_1.svg">|

## Responsive Favicons

This module adds the favicons generated by http://realfavicongenerator.net/ to your site. [Official Page](https://www.drupal.org/project/responsive_favicons) -Drupal 8 stable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_4.svg">|

## Search API

rovides a framework for easily creating searches on any entity known to Drupal, using any kind of search engine. [Official Page](https://www.drupal.org/project/search_api) - Drupal 8 stable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_y.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_5.svg">|

## Search API DB

Provides a backend for the Search API that uses a normal database to index data. [Official Page](https://www.drupal.org/project/search_api_db) - Drupal 8 - deprecated in favor of [Search API](https://www.drupal.org/project/search_api).

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|

## Search API Solr

Provides a Apache Solr backend for the Search API module. [Official Page](https://www.drupal.org/project/search_api_solr) - Drupal 8 stable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_5.svg">|

## Smart Trim

Implements a new field formatter for textfields (text, text_long, and text_with_summary) that improves upon the "Summary or Trimmed" formatter. [Official Page](https://www.drupal.org/project/smart_trim) - Drupal 8 stable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_4.svg">|

## SMTP Authentication Support (smtp)

Allows Drupal to bypass the PHP mail() function and send email directly to an SMTP server. [Official Page](https://www.drupal.org/project/smtp) - Drupal 8 release candidate.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_4.svg">|

## Strongarm

Gives site builders a way to override the default variable values that Drupal core and contributed modules ship with. [Official Page](https://www.drupal.org/project/strongarm) - Drupal 8 **included in Core Modules**.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_2.svg">|

## Taxonomy term depth

Adds crucial property called "depth" which indicates the term depth with a value 1, 2, 3 and further if needed. [Official Page](https://www.drupal.org/project/taxonomy_term_depth) - Drupal 8 stable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|

## Term Reference Tree

Provides an expandable tree widget for the Taxonomy Term Reference field. [Official Page](https://www.drupal.org/project/term_reference_tree) - Drupal 8 release candidate.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|

## Token

Provides additional tokens not supported by core (most notably fields), as well as a UI for browsing tokens. [Official Page](https://www.drupal.org/project/token) - Drupal 8 stable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_4.svg">|

## Translation helpers

Enables other modules to respond to changes in the "source translation" of a set of translated content. [Official Page](https://www.drupal.org/project/translation_helpers) - Drupal 8 not supported.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_1.svg">|

## UUID

Provides an API for adding universally unique identifiers (UUID) to Drupal objects, most notably entities. [Official Page](https://www.drupal.org/project/uuid) - Drupal 8 **included in Core Modules**.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|

## UUID Features Integration

Provides a mechanism for exporting content (nodes, taxonomy, fields) into a features module. [Official Page](https://www.drupal.org/project/uuid_features) Drupal 8 not supperted.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_1.svg">|

## Variable

Provides a registry for meta-data about Drupal variables and some extended Variable API and administration interface. [Official Page](https://www.drupal.org/project/variable) - Drupal 8 deprecated in favor of new [configuration API](https://www.drupal.org/docs/drupal-apis/configuration-api/configuration-api-overview).

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_2.svg">|

## Variable Check

Lists all entries in the {variable} table that cause unwanted PHP notices on production sites. [Official Page](https://www.drupal.org/project/variablecheck) - Drupal 8 deprecated.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_2.svg">|

## Views

Provides a simple configurable way to defining different views than the ones provided by default. [Official Page](https://www.drupal.org/project/views) - Drupal 8 **included in Core Modules**.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_y.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_5.svg">|

## Views Bootstrap

Views Bootstrap module enables you to create components following the theme structure of the Bootstrap framework all within the configuration settings of Views. [Official Page](https://www.drupal.org/project/views_bootstrap) - Drupal 8 stable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_4.svg">|

## Views Bulk Operations

augments Views by allowing bulk operations to be executed on the displayed rows. [Official Page](https://www.drupal.org/project/views_bulk_operations) - Drupal 8 stable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_5.svg">|

## Views Conditional

Allows you to define conditionals (if x then y) with fields in views. [Official Page](https://www.drupal.org/project/views_conditional) - Drupal 8 stable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_4.svg">|

## Views Data Export

Provide a way to export large amounts of data from views. It provides a display plugin that can rendered progressively in a batch. [Official Page](https://www.drupal.org/project/views_data_export) - Drupal 8 stable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_4.svg">|

## Views PHP (!!!)

Allows the admin account (user 1) to add fields, filters and sorts to views which use PHP code. [Official Page](https://www.drupal.org/project/views_php) - Drupal 8 unstable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_n.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_2.svg">|

## Webform

Provides a set of features for making forms and surveys in Drupal. [Official Page](https://www.drupal.org/project/webform) - Drupal 8 stable release.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_5.svg">|

## Webform Localization

Provides multilingual features to the Webform Module. [Official Page](https://www.drupal.org/project/webform_localization) - Drupal 8 not supported.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_2.svg">|

## Word Filter

Provides a suitable base for manual filtering of profanity ("bad words") as well as for dynamic keyword replacement. [Official Page](https://www.drupal.org/project/wordfilter) - Drupal 8 release candidate.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|

## X Autoload (!!)

Performant and comprehensive PHP class loading suite for Drupal 7. [Official Page](https://www.drupal.org/project/xautoload) - Drupal 8 not supported.

|                  Activity                              |                     Code Health                        |                Bug Reports                            |                        D8 Support                       |                D7 Authcache Support                         |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_2.svg">|


A complete analysis of the collected data is presented [here](/ipc-ig/reports/2020/06/13/contrib-modules-analysis/). It brings also a comparison between the results among Drupal 7 and 8 distributions.

Now let's take a better look into custom UNDP Modules.

# UNDP Modules

The Social Protection platform uses a set of custom modules that are under development. Here they'll be presented with their particularities and purposes. It's important to have in mind that the platform uses the [Features module](https://www.drupal.org/project/features) to export/import configurations between instances, but was it not aways like this. The project might have been migrated to this structure after the development of some modules and it was necessary to incorporate a [Strongarm module](https://www.drupal.org/project/strongarm) to allow old modules to be exportable as features. This and some other important notes about the platform are covered [here](/ipc-ig/reports/2020/06/13/undp-modules-analysis/).

## Custom Email Templates

This module is basically a fork of the official [Custom Email Templates](https://www.drupal.org/project/custom_email_template). As the module stopped being maintained, the team had to add it to the source of the project to make small fixes, mainlly for layout issues.

|                     Activity                           |                     Simplicity                         |                   Cacheable                            |                   Authcache support                    |                D8 ease of migration                         |                 Frequence of editions                  |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|

## UNDP Blog

This module is responsible for the Blog features and blocks of the platform, including Author's Page, Related Posts, Blog Tag Cloud, Blog Archives, Authors Summary, Blog Page Header and Contributors blocks. See more comments [here](/ipc-ig/reports/2020/07/01/comments-undp-modules/)

|                     Activity                           |                     Simplicity                         |                   Cacheable                            |                   Authcache support                    |                D8 ease of migration                         |                 Frequence of editions                  |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_n.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|

## UNDP Breadcrumb

Empty module. Was responsible for breadcrumb field, but after a structure change, all code was removed from it. 

|                     Activity                           |                     Simplicity                         |                   Cacheable                            |                   Authcache support                    |                D8 ease of migration                         |                 Frequence of editions                  |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_2.svg">|


## UNDP Common

Responsible for handling duplicated content ajax requests, 'claer all' options in search results, user hidden communities handler (and fixes for it), internal content verifications, global field query for entities and http/https alias generation.

|                     Activity                           |                     Simplicity                         |                   Cacheable                            |                   Authcache support                    |                D8 ease of migration                         |                 Frequence of editions                  |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_y.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|


## UNDP Community

Provides the secondary functionalities surrounding Organic Groups Module, including discussions, news, documents, members and views. 

|                     Activity                           |                     Simplicity                         |                   Cacheable                            |                   Authcache support                    |                D8 ease of migration                         |                 Frequence of editions                  |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_n.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|


## UNDP Contact

Just the contact form as a webform base64 serialized string.

|                     Activity                           |                     Simplicity                         |                   Cacheable                            |                   Authcache support                    |                D8 ease of migration                         |                 Frequence of editions                  |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_y.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|


## UNDP Defaults

Standard blocks like privacy policy toast (maybe all modals should be placed here), favicons, Context Definitions and Generic page styles.

|                     Activity                           |                     Simplicity                         |                   Cacheable                            |                   Authcache support                    |                D8 ease of migration                         |                 Frequence of editions                  |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_y.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|


## UNDP Events

Includes Event content type, views, blocks and context.

|                     Activity                           |                     Simplicity                         |                   Cacheable                            |                   Authcache support                    |                D8 ease of migration                         |                 Frequence of editions                  |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_y.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|


## UNDP Favorites

Provide functionalities of favorite content for users.

|                     Activity                           |                     Simplicity                         |                   Cacheable                            |                   Authcache support                    |                D8 ease of migration                         |                 Frequence of editions                  |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_n.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_2.svg">|


## UNDP Field Access

 Allow users to set their own field access settings.

|                     Activity                           |                     Simplicity                         |                   Cacheable                            |                   Authcache support                    |                D8 ease of migration                         |                 Frequence of editions                  |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_1.svg">|


## UNDP Followers

Allow users to follow other members.

|                     Activity                           |                     Simplicity                         |                   Cacheable                            |                   Authcache support                    |                D8 ease of migration                         |                 Frequence of editions                  |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_y.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_2.svg">|


## UNDP Forums

Forums with Advanced Forums with updated topic fields, blocks, context for Connect and search functionality.

|                     Activity                           |                     Simplicity                         |                   Cacheable                            |                   Authcache support                    |                D8 ease of migration                         |                 Frequence of editions                  |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_y.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_2.svg">|


## UNDP Frontpage

Defines all frontpage blocks and contents that are displayed and custom forms to configure view blocks.

|                     Activity                           |                     Simplicity                         |                   Cacheable                            |                   Authcache support                    |                D8 ease of migration                         |                 Frequence of editions                  |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_y.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_4.svg">|


## UNDP Glossary

Glossary Terms Definitions and Social Protection Definitions content types and views.

|                     Activity                           |                     Simplicity                         |                   Cacheable                            |                   Authcache support                    |                D8 ease of migration                         |                 Frequence of editions                  |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_y.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|


## UNDP Help

Creates an unecessary help content type that's not being used.

|                     Activity                           |                     Simplicity                         |                   Cacheable                            |                   Authcache support                    |                D8 ease of migration                         |                 Frequence of editions                  |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|


## UNDP Institutions

Stakeholder content type, menu actions and views.

|                     Activity                           |                     Simplicity                         |                   Cacheable                            |                   Authcache support                    |                D8 ease of migration                         |                 Frequence of editions                  |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_n.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_2.svg">|


## UNDP Learningtools

Provides View Block, and settings for Learning Tools Publication types.

|                     Activity                           |                     Simplicity                         |                   Cacheable                            |                   Authcache support                    |                D8 ease of migration                         |                 Frequence of editions                  |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_n.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_2.svg">|


## UNDP Locale

Provides configurations for language options and custom language dropdown styles.

|                     Activity                           |                     Simplicity                         |                   Cacheable                            |                   Authcache support                    |                D8 ease of migration                         |                 Frequence of editions                  |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_y.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|


## UNDP Menu

Regions menu itens with pre-defined classes, footer menu and social links.

|                     Activity                           |                     Simplicity                         |                   Cacheable                            |                   Authcache support                    |                D8 ease of migration                         |                 Frequence of editions                  |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_y.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_4.svg">|


## UNDP Moodle

Provides OAuth integration for Moodle.

|                     Activity                           |                     Simplicity                         |                   Cacheable                            |                   Authcache support                    |                D8 ease of migration                         |                 Frequence of editions                  |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|


## UNDP News

Content type "News" from UNDP-IPC Social Protection Site with News View and blocks.

|                     Activity                           |                     Simplicity                         |                   Cacheable                            |                   Authcache support                    |                D8 ease of migration                         |                 Frequence of editions                  |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_4.svg">|


## UNDP Notifications

Email notifications for subscribed users on specified topics: add content (to community), blog post comment, blog post comment reply, favorite content, new coment (forums), new reply (forums), new follower, oc discussion (admin), oc document (admin), oc event (admin), oc news (admin), oc new membership, oc request membership, oc discussion comment, oc comment discussion reply.

|                     Activity                           |                     Simplicity                         |                   Cacheable                            |                   Authcache support                    |                D8 ease of migration                         |                 Frequence of editions                  |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_2.svg">|


## UNDP Online Courses

Enable Online Courses Content Type, form and view.

|                     Activity                           |                     Simplicity                         |                   Cacheable                            |                   Authcache support                    |                D8 ease of migration                         |                 Frequence of editions                  |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_y.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_4.svg">|


## UNDP Permissions

Users roles and custom permissions.

|                     Activity                           |                     Simplicity                         |                   Cacheable                            |                   Authcache support                    |                D8 ease of migration                         |                 Frequence of editions                  |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|


## UNDP Programme

Content type "Programme" from UNDP-IPC Social Protection Site with updated fields and conditionals.

|                     Activity                           |                     Simplicity                         |                   Cacheable                            |                   Authcache support                    |                D8 ease of migration                         |                 Frequence of editions                  |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_y.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_4.svg">|


## UNDP Publications

Content type "Publications" with discover pages context. Global "Dicover" elements are defined here.

|                     Activity                           |                     Simplicity                         |                   Cacheable                            |                   Authcache support                    |                D8 ease of migration                         |                 Frequence of editions                  |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_n.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|


## UNDP Register

Include custom user register form and profile fields, user form settings with reCaptcha, newsletter subscription and login page.

|                     Activity                           |                     Simplicity                         |                   Cacheable                            |                   Authcache support                    |                D8 ease of migration                         |                 Frequence of editions                  |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_n.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_2.svg">|


## UNDP Reports

Reports views, google analytics module

|                     Activity                           |                     Simplicity                         |                   Cacheable                            |                   Authcache support                    |                D8 ease of migration                         |                 Frequence of editions                  |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_n.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_4.svg">|


## UNDP Search

Search pages and functionality for the entire site.

|                     Activity                           |                     Simplicity                         |                   Cacheable                            |                   Authcache support                    |                D8 ease of migration                         |                 Frequence of editions                  |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_y.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|


## UNDP Share

Provides custom validations for Share menu actions and a dynamic Share page. All menu items under Share section must be defined by code to work properly.

|                     Activity                           |                     Simplicity                         |                   Cacheable                            |                   Authcache support                    |                D8 ease of migration                         |                 Frequence of editions                  |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_n.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_2.svg">|


## UNDP Social Space

Provides social network features like follow/unfollow options, public profile activities, contact pages for members (like a chat), member communities. The module is integrated to members search (UNDP Search) and communities (UNDP Community).

|                     Activity                           |                     Simplicity                         |                   Cacheable                            |                   Authcache support                    |                D8 ease of migration                         |                 Frequence of editions                  |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_n.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_2.svg">|


## UNDP Survey

Views, context and Survey content type for the Survey Area.

|                     Activity                           |                     Simplicity                         |                   Cacheable                            |                   Authcache support                    |                D8 ease of migration                         |                 Frequence of editions                  |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_y.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|


## UNDP Taglines

Taglines for main sections, mainly earch, Virtual Campus and Learn pages.

|                     Activity                           |                     Simplicity                         |                   Cacheable                            |                   Authcache support                    |                D8 ease of migration                         |                 Frequence of editions                  |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_4.svg">|


## UNDP Taxonomy

Import taxonomies and terms from original site.

|                     Activity                           |                     Simplicity                         |                   Cacheable                            |                   Authcache support                    |                D8 ease of migration                         |                 Frequence of editions                  |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|


## UNDP Textformats

Provides text formats and CK Editor Profiles.

|                     Activity                           |                     Simplicity                         |                   Cacheable                            |                   Authcache support                    |                D8 ease of migration                         |                 Frequence of editions                  |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|


## UNDP Timeline

Provides user timeline features.

|                     Activity                           |                     Simplicity                         |                   Cacheable                            |                   Authcache support                    |                D8 ease of migration                         |                 Frequence of editions                  |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_n.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_2.svg">|


## UNDP Translation

Defines custom entity translation variables as exportables.

|                     Activity                           |                     Simplicity                         |                   Cacheable                            |                   Authcache support                    |                D8 ease of migration                         |                 Frequence of editions                  |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|


## UNDP Urls

URL redirects, bulk update views and patterns.

|                     Activity                           |                     Simplicity                         |                   Cacheable                            |                   Authcache support                    |                D8 ease of migration                         |                 Frequence of editions                  |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|


## UNDP User Import

Bulk import users from admin panel.

|                     Activity                           |                     Simplicity                         |                   Cacheable                            |                   Authcache support                    |                D8 ease of migration                         |                 Frequence of editions                  |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_1.svg">|


## UNDP Virtual Campus

Virtual Campus views and contexts. Provides integrations with online courses, webinars and search.

|                     Activity                           |                     Simplicity                         |                   Cacheable                            |                   Authcache support                    |                D8 ease of migration                         |                 Frequence of editions                  |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_n.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_4.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_2.svg">|


## UNDP Webinar

Provides menu links, blocks, content type and views for webinar.

|                     Activity                           |                     Simplicity                         |                   Cacheable                            |                   Authcache support                    |                D8 ease of migration                         |                 Frequence of editions                  |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_n.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_3.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_2.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_2.svg">|


## UNDP Word Filter

Common functions to use to filter words.

|                     Activity                           |                     Simplicity                         |                   Cacheable                            |                   Authcache support                    |                D8 ease of migration                         |                 Frequence of editions                  |
|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------:|
| <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_x.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_5.svg"> | <img width="50" src="/ipc-ig/reports/img/badge_1.svg"> |

|                   Average Score                      |
|:----------------------------------------------------:|
|<img width="50" src="/ipc-ig/reports/img/badge_3.svg">|


More details about each module and a compilation of the results can be found [here](/ipc-ig/reports/2020/07/01/comments-undp-modules/)
