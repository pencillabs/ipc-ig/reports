---
title: Querying content type nodes in Database
date: 2021-01-20 12:24:38
thumbnail: /img/thumb_pattern_blue.svg
toc: true
categories:
 - Development
---

In order to manipulate content programmatically, sometimes you need to access Database. To do so you can use a drupal class called ***EntityFieldQuery***, its documentation can be found [here](https://www.drupal.org/docs/7/creating-custom-modules/howtos/how-to-use-entityfieldquery-for-drupal-7). 


```php
function undp_module_function(){
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'content_type_machine_name')
    // Remove the following line if you don't need to get only published nodes.
    ->propertyCondition('status', NODE_PUBLISHED)
    // Keep the following line, so Drupal won't check if the currently logged-in user has access to the nodes and the fields.
    ->addTag('DANGEROUS_ACCESS_CHECK_OPT_OUT');
    $result = $query->execute();
    print_r($result);
}
```


If you want to test that function, you can run it separately in your shell, by using:

```shell
$ make ssh
$ drush php-eval "module_load_install('undp_module'); undp_module_function();"
```

Some explanations about the code above

- `->entityCondition($name, $value, $operator = NULL)`: It can be used to look at the entity_type (can be 'node', 'taxonomy_term', 'comment', 'user', 'file') or can look at the bundle (name of a content type, for example);
- `->propertyCondition($name, $value, $operator = NULL)`: Can be a base value for an entity type such as title, status, type, uid, URI;
- `->fieldCondition($field, $column = NULL, $value = NULL, $operator = NULL, $delta_group = NULL, $language_group = NULL)`: Conditions to a specic field. You have to pass a field name (machine name, can be looked up in admin panel) and can pass a column to look up such as value, or for example, in an image field, 'fid', 'alt', and 'title'; 
- `$operator`: Can be '=', '<>', '>', '>=', '<', '<=', 'STARTS_WITH', 'CONTAINS': These operators expect $value to be a literal of the same type as the column.
'IN', 'NOT IN': These operators expect $value to be an array of literals of the same type as the column.
'BETWEEN': This operator expects $value to be an array of two literals of the same type as the column.
The operator can be omitted, and will default to 'IN' if the value is an array, or to '=' otherwise.


# Dealing with results

After getting the query result you may want to edit the data that you received and for that you can use a entity wrapper, as an example below:

```php
    // $result['node'] contains the array of nodes that resulted from the query
    if (isset($result['node'])){
        foreach($result['node']) {
            $wrapped_node = entity_metadata_wrapper('node', $node);
            $wrapped_node->field_name->set('new_value');
            $wrapper_node->save();
        }
    }
```

Other ways to manipulate can be `node_load()` function, that also let you pass an array of node ids and load the nodes from database.

