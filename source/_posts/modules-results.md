---
title: Results for modules analysis
date: 2020-07-05 21:17:00
thumbnail: /img/thumb_pattern_orange.svg
toc: true
categories:
 - Analysis
 - Drupal 8
---

# Outdated Modules

In order to proceed to a migration or to the implementation of new features on the platform, some modules need to be updated to the latest minor version available. The table below presents the number of modules that need update.

| Outdated Modules | Up to date Modules | Critical Modules | Total |
|:----------------:|:------------------:|:----------------:|:-----:|
|         79       |         45         |        13        |  137  |


The critical modules are the ones that have minor releases changed. The release number pattern follows the structure *M.m.f-x*. The *M* is the major version and follows drupal releases, the *m* is related to minor changes and follows the module API, when this number changes, it implies a change in a feature of the module that impacts backward support. The *f* refers to bug fixes and security patches only, and the x, the stability of the version (such as *alpha*, *beta*, *dev* or *rc*).

This leads us to a total of 12 modules that will be incompatible with the current state of the platform and should be updated, independent of the Drupal 8 migration.

# Drupal 8 Support

Another point to be taken into account is the number of modules that directly support Drupal 8, some of them have been included in core modules and others have released stable versions. There are still 4 modules that did not conclude a stable release and others that will never give support to newer versions.

|  Stable Modules  | Under port Modules | Critical Modules | Total |
|:----------------:|:------------------:|:----------------:|:-----:|
|         75       |          7         |        18        |  100  |

The 18 critical modules must be evaluated individually. Some of them are not necessary on Drupal 8, like X Autoload that was replaced by the [Framework](https://symfony.com/) standard class loader.

The difference between the total modules is because the disabled or submodules were not listed on the Drupal 8 evaluation once they don't represent a risk for the Drupal 8 migration.

# Drupal 7 vs 8 Maintenance

The other aspect raised by this evaluation is the health of the modules under Drupal 7 version, recent commits and responses to bug reports.

| Version |   Active Modules    | Deprecated modules | Abandoned Modlues | Cold Modules |
|:-------:|:-------------------:|:------------------:|:-----------------:|:------------:|
| Drupal 8|          82         |          0         |        4          |      16      |
| Drupal 7|          41         |          3         |        22         |      34      |



# Drupal 8 database migration results

The standard data migration resulted in a good percentage of content imported, but it lost relations between some entities and configurations of language and translations.

| Migrated content (%) |    Migrated Users   |  Migration issues  |    Executed tasks    |
|:--------------------:|:-------------------:|:------------------:|:--------------------:|
|          68%         |          98%        |        122         |         27843        |

There were also issues migrating and enabling some of the contributed modules. This is related to the outdated modules that are not ready to be migrated, most recent releases give support for Drupal 8 migration using migrate-tools without the need of any other setup.

# Drupal 7 Authcache implementation paths

There are some community modules that do not provide a clear path to supporting Authcache and many of the custom UNDP Modules do not even allow standard Drupal cache, which is a starting point for working with Authcache. It doesn't make sense to compare the cacheability between Drupal versions, since all Drupal 8 modules are cacheable by default.

| Module type | Already Supports | Can be done | Very costly | Can't be done | Total |
|:-----------:|:----------------:|:-----------:|:-----------:|:-------------:|:-----:|
|  Community  |        16        |      3      |      3      |       2       |   24  |
|  Custom     |        5         |      13     |      7      |       0       |   25  |

There are 50 modules that need to be evolved in order to give support for authcache. The more expensive modules to implement support for Authcache are: Organic Groups, Profile 2 and Views PHP. There are two modules that cannot have support: i18n Menu Node and i18n Views because they were abandoned.
Our custom modules have support for cache, but they need some updates to use Authcache properly. The more expensive modules are related to large areas of the platform, such as search pages, Communities, Stakeholders, Profiles and Social Space features, like user timeline.

# Costs, drawbacks and benefits

The analysis leads to the measurement of the most significant benefits for each approach. On the one hand, the Drupal 8 migration and, on the other, support for authcache.

## Drupal 8 migration

Upon conclusion of the migration, the following benefits will be expected:

 - **Performance:** Expected gain of 30% of time per request.
 - **Maintenance:** Fewer modules to keep tracking and developing, also better community support.
 - **Deploy cicles:** Recent registered bugs are related to visual changes, like texts, positions and element styles. In Drupal 8, a frontend collaborator will be able to work on templates without demanding php knowledge.

The main raised drawbacks are:

 - **Complexity:** Lack of documentation and code appropriation, which makes work difficult.
 - **Time:** Devoted time to a complete migration will be long and dependant of the community modules that are not production ready.
 - **Not fully Supported:** Some modules do not yet fully support Drupal 8.

The migration to Drupal 8 will be madatory at some point, but the community doesn't yet support all necessary modules to put a Drupal 8 version of the platform under production.

## Authcache support implementation

Authcache implementation has the following benefits:

 - **Performance:** Expected gain of near 20% of time per request.
 - **Ease of implementation:** Compared to a Drupal 8 migration, implementing authcache support will be a simpler task.
 - **New Features:** The development of this support does not require the development of new features to be suspended.

The drawbacks of this approach are:

 - **Rework on modules:** Authcache support will have implications on developed features, some of them might present undesired results due to caching critical resources. These tests will have to be developed accordingly.
 - **Lack of support:** The Drupal 7 modules are receiving less support during the time and some of them are already not being maintained.
 - **Compatibility:** A large effort spent on a full implementation of Authcache will be lost in a future Drupal 8 migration.

The Authcache implementation may be an easier way to improve the performance for authenticated users, but it will require code intervention and some implementations to give better results. Also, the decision of keeping the platform on Drupal 7 is risky, because fewer bugs are being fixed and at some point, the migration will be a mandatory change, not an option.

## Suggested Approach (Mixed)

An approach that can minimize the impacts and provide the expected results is to start a review of the components to support Authcache that are not intrusive. Hence, many features can be improved without requiring big changes and thus, can be done without interfering on a possible migration. In parallel, we can define short-term goals to achieve in Drupal 8 migration and every one or two months we can work on prioritizing features and next steps. The benefits intended for this approach are:

 - **Performance:** Faster development of performance enhancements with authcache.
 - **Maintenance:** Starting the migration early will help to keep following the community and improve the migration quality. Besides it will give time to face problems with not supported modules, when they might get stable. It also makes the codebase closer to Drupal 9, which has very few differences from Drupal 8 and is backward compatible.
 - **New features:** Avoid blocking new development cycles while Drupal 8 version is not production ready.
 - **Better testing:** This approach also allows the development of better test cycles since the migration won't be done in a single moment, every month or two a new test with a production database can be performed, which will require an extensive test suite for migrated resources.

The drawbacks and risks are:

 - **Time:** It will take longer to complete the migration.
 - **Focus:** To keep the work focused on the migration will be a tough task when new bugs are always appearing.

# Final considerations

The purpose of this analysis was to evaluate alternatives to improve the platform's performance, keeping in mind sustainability and the new cycles of development that are yet to come. The cost, time and cost-benefit estimates for each approach are presented in the table bellow:

|  Approach  |  Estimated Cost  |  Time (months)  |   Risk   |   Cost-benefit [1-5]  |
|:----------:|:----------------:|:---------------:|:--------:|:---------------------:|
| Authcache  |   R$ 20-50  k    |       2-6       |   High   |           1           |
|  Drupal 8  |   R$ 50-100 k    |       6-12      |  Medium  |           2           |
|   Mixed    |   R$ 60-100 k    |       8-14      |   Low    |           4           |

The Drupal 8 migration can be a best suited approach to improve performance and maintenance, but it poses a risk if new development cycles need to be suspended while the migration is not complete. The pressure for a faster delivery can reverberate in lower quality and bugs that could affect users after a release. It will also require the development of functionalities that are currently provided by contributed modules, which can be very complex. Meanwhile, these modules can be ported and the work will be expendable.

The implementation of the authcache support is the cheaper solution, but in one year will be irrelevant because of incompatibilities with Drupal 8. The development of a full implementation of Authcache will take some time and some rework over already developed modules. Also, besides the performance gains, the platform will need to be migrated to Drupal 8 at some point until 2022, and a large effort will be lost due to lack of compatibility with the new cache system.

The mixed approach takes advantage of both options, avoiding some time-consuming development of Authcache support and, instead, migrating modules fully supported by Drupal 8. After each milestone, a new module can be prioritized for migration and optimization, postponing unsupported modules and giving them more time to get stable.

The critical issue arround all this evaluation is that both Drupal 8 and 7 will be reaching end of life on November 2021, and a migration to Drupal 9 will be necessary. As Drupal 9 is much closer to Drupal 8 and backward compatible, furter migrations will be much simpler, it's just very important to keep tracking of [deprecated APIs](https://dev.acquia.com/drupal9/deprecation_status) during the migration in order to avoid using them.

# References

[Symfony releases](https://symfony.com/blog/category/releases)
[Upgrading from D8 to D9](https://www.drupal.org/docs/upgrading-drupal/upgrading-from-drupal-8-to-drupal-9-or-higher)
[Preparing a site for upgrade to Drupal 8](https://www.drupal.org/docs/upgrading-drupal/preparing-a-site-for-upgrade-to-drupal-8)
[Learn key Drupal 8 concepts prior to upgrading](https://www.drupal.org/docs/upgrading-drupal/learn-key-drupal-8-concepts-prior-to-upgrading)
[Upgrading multilingual Drupal 7 to Drupal 8](https://www.drupal.org/docs/upgrading-drupal/upgrading-multilingual-drupal-7-to-drupal-8)
[Drupal 8 migrate modules](https://www.drupal.org/docs/upgrading-drupal/drupal-8-migrate-modules)
[Upgrade using Drush](https://www.drupal.org/docs/upgrading-drupal/upgrade-using-drush)
[Known issues when upgrading from Drupal 6 or 7 to Drupal 8](https://www.drupal.org/docs/upgrading-drupal/known-issues-when-upgrading-from-drupal-6-or-7-to-drupal-8)
[Customize migrations when upgrading to Drupal 8](https://www.drupal.org/docs/upgrading-drupal/customize-migrations-when-upgrading-to-drupal-8)
[Deprecated APIs Drupal 8 to 9](https://dev.acquia.com/drupal9/deprecation_status)
[Rector automating code to Drupal 9](https://www.drupal.org/project/rector)
[Drupal Rector](https://github.com/palantirnet/drupal-rector)
[Jumpstart Your Drupal 9 Upgrade with Drupal Rector](https://www.palantir.net/blog/jumpstart-your-drupal-9-upgrade-drupal-rector)
[Page Cache Performance](https://www.drupal.org/project/drupal/issues/2501989#comment-10000905)
[Drupal 8 performance profiling](https://www.drupal.org/project/drupal/issues/2470679)
[Drupal 8 performance profiling Post](https://kazanir.github.io/profiling/)

