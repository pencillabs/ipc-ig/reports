---
title: Comments for each UNDP Module
date: 2020-07-01 23:42:02
thumbnail: /img/thumb_pattern_orange.svg
toc: true
categories:
 - Analysis
 - Drupal 8
---

Some specificities of each module needed to be registered somewhere. This article is a compilation of all important notes that were taken while analysing and collecting information about the source code.

# UNDP Blog

Even with a well-defined purpose and general simplicity, the module includes some globally used methods that can be moved to `UNDP Common`.

# UNDP Common

Provides a set of methods that are not very clear, including a custom js for duplicated content. It also implements a `menu_hook` to execute a content duplication. The hook is no longer supported by Drupal 8 and this feature must be refactored in order to keep the functionality. Shared code between modules might be moved to UNDP Commons. Some affected modules are: `UNDP Search`, `UNDP_Defaults`, `UNDP_Blog`.

# UNDP Community

Most of database calls are not cacheable and thus, high resources consuming. Methods that should not be necessary for updating node aliases and paths on form submit should be avoided and a url pattern should be defined instead. The `undp_community_get_permissions_from_og` executes extense loops over all platform communities, organic groups roles and permissions by community type. This might have implications on performance issues. It's early to state, but these functionalities might be defined by community types and member roles instead.

# UNDP Defaults

The UNDP Defaults can be moved into UNDP Common. Some issues related to context override are preventig the module to keep the right markdown for Privacy Policy toast. It also defines simple page layout and styles that can be improved.

# UNDP Events

Seems to be a module with frequent bugs, since the history indicates a set of changes overiding same code sectinos.

# UNDP Favorites

Uses a custom hook implemented in UNDP Register (`hook_undp_register_user_data_menu`) to create custom flags for each user's favorite content. Also depends on Views PHP that is a very problematic module.

# UNDP Field Access

Defines a custom access structure for permissions on the platform. This might be related to the excessive code for handling permissions on a large number of modules, like UNDP Register, UNDP Moodle, UNDP Community and Profile2.

# UNDP Followers

Uses the `hook_undp_register_user_data_menu` to register custom menu options to user pages, for Followers and Following links.

# UNDP Forums

Depends on UNDP Blog and UNDP Taxonomy because it uses some methods that should be moved to UNDP Common. Implements a `menu_hook` that must be migrated to a Controller in a Drupal 8 migration.

# UNDP Institutions

Provides a set of custom validations for form fields that are not being used. Also creates a set of menu links that might not be supported by Drupal 8. Some actions will be moved to separate controllers.

# UNDP Learning Tools

Creates a custom function for validating publication content type as Learning Tools, used also in UNDP Search module. Should not be setup this way.

# UNDP Menu

Creates a very large number of menu links that are not supported by Drupal 8. This module will have to be migrated to a routing module with custom controller and maybe templates for eack custom link markup.
 
# UNDP News

Depends on many modules that might not be necessary: UNDP Blog, UNDP Frontpage, UNDP Help UNDP Institutions, UNDP Taxonomy. Also implements `hook_menu` that will have to be replaced by custom Controller. Uses the UNDP Comon `duplicated_content` ajax verification.

# UNDP Notifications

Email notifications for subscribed users on specified topics: add content (to community), blog post comment, blog post comment reply, favorite content, new coment (forums), new reply (forums), new follower, oc discussion (admin), oc document (admin), oc event (admin), oc news (admin), oc new membership, oc request membership, oc discussion comment, oc comment discussion reply. Each of this topics is a submodule of UNDP Notifications with its particular settings. This is a very complex system to be evolved or rebuild on top of this structure.

# UNDP Online Courses

Some changes made to online courses are related to members export feature. This change creates a set of exportable fields in many content types that can block async requests when they need to recreate cached resources.

# UNDP Permissions

Permissions might need a review.

# UNDP Publications

Implements ajax callback to count the number of accesses per publication, a minor issue, but it's worth to register.

# UNDP Register

IMplements custom hook used by other modules.

# UNDP Reports

The Views Data Export module has [many unresolved issues](https://www.drupal.org/project/issues/views_data_export?status=All&categories=All&page=11) related to low performance during the execution of large reports. An integration with [Denormalizer](https://www.drupal.org/project/denormalizer) to produce a data flow is an alternative, avoiding database locks and providing a separate database to directly integrate in Power BI. This can provide dynamic and flexible reports. There is a reference [here](https://events.drupal.org/seattle2019/sessions/drupal-data-warehouse-everybody-data-lake)

# UNDP Search

Search pattern definitions are not very well structured, there are many manual conditions to check the redirects, this might be related to the "all fields" issue that always redirects to the general search page. The use of an old version of Solr is not restricted by the module, so we can try updating its version along with a migration to Drupal 8.

# UNDP Share

The module implements `hook_menu` that is not supported on Drupal 8.

# UNDP Taxonomy

Not necessary if we migrate the database as it is now. Just move preprocess fields hooks to each content type modules.

# UNDP User Import

Uses external libs included in source code of the project. The upgrade on php version might cause failures that will be very hard to track and fix. A possible solution is to find a community module to implement this feature.

# UNDP Webinar

Implements `hook_menu` and must be replaced for a custom Controller in Drupal 8.

# UNDP Word Filter

This module should migrate to UNDP Common ou UNDP Defaults.
