---
title: Search API - Fields, Facets & Exposed Forms
date: 2020-07-28 14:38:55
thumbnail: /img/thumb_pattern_blue.svg
toc: true
categories:
 - Development
---

This tutorial aims to go through all the steps to generate new fields, facets and exposed forms for Search API and Solr.

## The Search page structure

A search page in Social Protection platform has the following components:

 - **The Exposed Form:** Search fields that will be used in a text filter.
 - **The Facets:** Checkbox filters used to restrict results.
 - **Applyed Filters:** Applied filters.
 - **Search Results:** Returned results for applied filters.

The image bellow visually presents each of these items on a search page.

<img src="/ipc-ig/reports/img/search-page-areas.png">

Now, let's dig deeper into each area.

## Creating a new Search Page

All search pages that are not related to user database are stored under **Search Pages** in admin > structure > views > search_pages. To create a new Page, open the search pages' View and click on the ![](/ipc-ig/reports/img/search-page-add-button.png). The new page will have access to the filter criteria that will be applied to the content in order to return what you expect. The following image presents the creation of a filtered view for the Job Content Type and also filters only nodes that the `application deadline` are in the furete (`>now`) and exposes filters for Title and Fulltext Search to users.

<img src="/ipc-ig/reports/img/search-page-settings.png">

It's important to note that the option `exposed form in block` is configured as **Yes**, as hilighted above. This is important to expose filters as shown in image 1.

## Exposed Filters

If you don't know what are exposed filters in Drupal, please refer to [this article](https://www.bryanbraun.com/2013/08/06/drupal-tutorials-exposed-filters-with-views/). Other really useful content is the [Search API Series](https://modulesunraveled.com/blog/search-api-series-creating-search-page-views-faceted-searches-using-autocomplete-while-typing) that goes deeper into creating and customizing views with Solr as Search API backend. If you already know about Search API, Solr and Exposed Filters in general, it's time to learn more about how they are implemented in socialprotection.org.

The Exposed filters in social protection are built under `undp_search.module`, and configured in `undp_search_block_view_alter` that overides all exposed filter blocks #markup to add **Advanced Search** and **Filters** titles. In a standard Drupal 7 application, if you add custom filters that are not textual, the selects would be presented as visual components inside the exposed form area, but because the use use of the Facets API, these filters cannot be exposed to users, the right way of doing it is by creating new Facets. Also, if the field cannot be found in the *Filter Criteria* list fo fields, then you have to index that field in Search API Index. The source bellow show the view alter:

```php
function undp_search_block_view_alter(&$data, $block) {
  
  // add your view block delta inside $deltaBlocksSearch array.
  $deltaBlocksSearch = array("862eee09f81959668025922529f2af37", "-exp-search_pages-news_search", // many other deltas... );
  
  // ...

}
```

## Indexing Fields and Facets

In order to create filter criterias under a custom search page, you have to index the related filed in UNDP Solr Index. Access the *UNDP Index* page in admin > config > search API > undp index and then click the *FIELDS* tab, the following page will open.

<img src="/ipc-ig/reports/img/search-page-index-field.png">

Look for the field you want to index, select the checkbox under *INDEXED* column and click on save, at the bottom of the page. After that, you will ave to reindex all content of the platform in order to make the field available in filter criteria. At this moment, your field will be available under *FACETS* tab, in this case, we are going to create facets for *City* and *Country* for the Job Content Type, as shown bellow.

<img src="/ipc-ig/reports/img/search-page-facets.png">

Click under *Configure Display* and set your facet like this. It's important to set No Limit, to avoid "show more/fewer" options that are not present in platform layout. Also, correctly sort the display items on the right to keep the same layout of all other facets.

<img src="/ipc-ig/reports/img/search-page-facet-display.png">

## Search Results

The layout of each result row can be configured directly under **Search Pages** in admin > structure > views > search_pages. Now, take a look at the *FORMAT* and *FIELDS* configurations. To use the same layout as all other search pages, set `Format` as `unformatted list` and `Show` as `fields`, this way you can configure and override each field markup properties.

<img src="/ipc-ig/reports/img/search-page-result-fields.png">

## Context, Features & Export configurations

To finish your page layout, you have to tell Drupal that your **Exposed Filters** and **Facets** are going to be rendered under your page, in our example, the Jobs Search page. To setup this layout, open the **Context UI** under admin > structure > context and create a new Context for your page, in this case we created a `search_jobs` context. Under **Reactions** click on *Blocks*, the main areas that we are interested in are:

 - Content: Where the results will be presented.
    - Under this section we have to add `current search: standard` to tell Drupal to add the applied filters when a search is performed using either the fulltext serach or the exposed facets.
 - Secondary: Where the search field and facets will be presented.
    - Under ths section we select the exposed filter we created inside our custom search page. In this example it's the `Exposed form: search_pages-jobs_search_list`.
    - We also add all of our Facets that we created before: City, Country and also Language, that were already enabled and used by some other search pages.

After that, all the setup is done, but you have to export all this setting to code. 

### IT IS REALLY IMPORTANT TO DO THIS CONFIGURATIONS ON CODE. IF SOME OF THIS SETTINGS ARE NOT EXPORTED, THEY WILL BE OVERRIDEN AFTER A NEW DEPLOY AND SOME FEATURES WILL BE LOST!!

To export the search page to code, you have to export the following configurations:

 - **The UNDP Search module:** that automatically holds Views configurations.
 - **The Context:** That will keep the layout settings of your page.
 - **The Facets:** That will keep indexed fields and enabled Facets.
 - **The Exposed Form ID:** Important to add *Advanced Search* and *Filters* title on #markup.

### Exporting Search Module

Open your terminal and navigate to the repository folder, access the ssh container and inside it, update undp_search module. That will export search view updates to `undp_serach` module.

```bash
  make ssh
  drush features-update undp_search
```

If your environment is not running, follow the repository [README.md](https://bitbucket.org/iteliosbrasil/itelios.ipc.socialprotection/src/develop/README.md) instructions.

### Exporting Context

Open context page again and under your search page context, in this example it is `search_jobs`, click export, as shown bellow.

<img src="/ipc-ig/reports/img/search-page-export-context.png">

Copy the content of the export field and paste into `undp_search.context.inc`. By the end of the copied content, add `$export['<YOUR_SEARCH_PAGE>'] = $context` like the complete example bellow.

```php

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'search_jobs';
  $context->description = '';
  $context->tag = 'Search';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'search_pages:jobs_search_list' => 'search_pages:jobs_search_list',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'undp_help-undp_help_message' => array(
          'module' => 'undp_help',
          'delta' => 'undp_help_message',
          'region' => 'search',
          'weight' => '-10',
        ),
        'current_search-standard' => array(
          'module' => 'current_search',
          'delta' => 'standard',
          'region' => 'content',
          'weight' => '-10',
        ),
        'views-862eee09f81959668025922529f2af37' => array(
          'module' => 'views',
          'delta' => '862eee09f81959668025922529f2af37',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'facetapi-jv1WBENqfQvpgq8RRGThI40g1LX0CU10' => array(
          'module' => 'facetapi',
          'delta' => 'jv1WBENqfQvpgq8RRGThI40g1LX0CU10',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
        'facetapi-YUzJc2zLqR11ezrFCNZ8LfDjE57uR0Ub' => array(
          'module' => 'facetapi',
          'delta' => 'YUzJc2zLqR11ezrFCNZ8LfDjE57uR0Ub',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
        'facetapi-ellz1pRXfmrmKBqIeSRo4VILDnEaqE4o' => array(
          'module' => 'facetapi',
          'delta' => 'ellz1pRXfmrmKBqIeSRo4VILDnEaqE4o',
          'region' => 'sidebar_second',
          'weight' => '-7',
        ),
      ),
    ),
    'theme_html' => array(
      'class' => 'search-page theme-discover',
    ),
  );
  $context->condition_mode = 0;
  
  // Translatables
  // Included for use with string extractors like potx.
  t('Search');
  
  // Include this for the export to be effective.
  $export['search_jobs'] = $context;

```

After that, we have to export Facets configurations.

### Exporting Facets and Indexed Fields

Open Facets page again and search for your facet under Facets tab (/admin/config/search/search_api/index/undp_index/facets). Under the *Configure Display* Dropdonw, select **Export Configuration**. Copy the content of the configuration to `undp_search.facetapi_defaults.inc`. If you just added the index, but did not enable a Facet, you will have to export the facet anyway. The simple `application_deadline` indexed field that do not have a fecet enabled.

```php
	$facet = new stdClass();
  $facet->disabled = FALSE; /* Edit this to true to make a default facet disabled initially */
  $facet->api_version = 1;
  $facet->name = 'search_api@undp_index::field_application_deadline';
  $facet->searcher = 'search_api@undp_index';
  $facet->realm = '';
  $facet->facet = 'field_application_deadline';
  $facet->enabled = FALSE;
  $facet->settings = array(
    'operator' => 'and',
    'hard_limit' => 50,
    'dependencies' => array(
      'facets' => array(),
      'force_deactivation' => TRUE,
      'regex' => FALSE,
    ),
    'facet_mincount' => 1,
    'facet_missing' => 0,
    'flatten' => 0,
    'individual_parent' => 0,
    'query_type' => 'date',
    'limit_active_items' => 0,
  );

  // Add this to make the export efefective
	$export['search_api@undp_index::field_application_deadline'] = $facet;
```

Any field will have the same structure, the facets enabled will have some more configurations like limit and orders, but the structure will be the same.

## Submitting to repository

To submit this editions to the repository, follow the steps:

 - Generate a database dump with your settings and customizations `drush sql-dump --result-file="my-dump.sql"`. If something go wrong, you can restore it and review your exports.
 - Drop the database and import the most recent production database that you have `drush sqlc < backup-prod-latest.sql`. Clean your environment to avoid submitong garbage to the repository.
 - Apply all features and clean all cache with `drush fra -y && drush cc all`.
 - Access your search page and check if everything is working fine. If it doesn't, restore your dump database and check all your exports.
 - If everything is correct, add all search changes to git with `git add docroot/sites/all/modules/undp-features/undp_search` and commit.


