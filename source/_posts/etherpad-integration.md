---
title: Online Communities - Etherpad integration
date: 2020-08-24 11:21:02
thumbnail: /img/thumb_pattern_blue.svg
toc: true
categories:
 - Development
---

The Online Communities can integrate a set of tools to provide better collaboration between members. See [this article](/ipc-ig/reports/2020/08/17/oc-tools-evaluation/) to undertand the process of evaluation of tools to provide the wanted features:

 - Collaborative document edition
 - Realtime document edition
 - Web chat for realtime interaction
 - Video/Audio calls for better communication
 - Full Drupal integration

The [Etherpad](https://etherpad.org/) provides all the features and can be integrated into a Drupal environment easily using a [community module](https://git.drupalcode.org/project/etherpad) that provides such integration. This article will cover all the steps required to execute the integration.

## instalation

The required applications and modules to be installed are:

 - [Etherpad backend](https://github.com/ether/etherpad-lite)
 - [etherpad-lite-client library](https://github.com/TomNomNom/etherpad-lite-client)
 - [etherpad-lite-jquery-plugin library](git://github.com/ether/etherpad-lite-jquery-plugin)
 - [etherpad module](https://git.drupalcode.org/project/etherpad)
 - [wysiwyg module](https://www.drupal.org/project/wysiwyg)
 - [libraries module](https://www.drupal.org/project/wysiwyg) already installed on social protection

To install the Etherpad backend as instructed in [the official repository](https://github.com/ether/etherpad-lite), execute the following:

```
    curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
    sudo apt install -y nodejs
    git clone --branch master https://github.com/ether/etherpad-lite.git && cd etherpad-lite && bin/run.sh
```

After that, you should be able to access etherpad instance in `http://localhost:9001`.

<img src="/ipc-ig/reports/img/etherpad-page.png">

To integrate ehterpad in Drupal, you will need to access a poblic url of your Etherpad. Get your ip address running the command `ip a` and use it instead of the `localhost`. In my case, the url is:

```
  http://192.168.1.8:9001/
```

Save this url because it will be necessary when configuring ehterpad module in drupal.

To install the libraries, change directory to the root of the drupal installation containing
   `index.php`. Then execute following commands:

```
    cd docroot/sites/all/libraries
    git clone git://github.com/TomNomNom/etherpad-lite-client.git
    git clone git://github.com/ether/etherpad-lite-jquery-plugin.git
```

This will download the libraries into the correct folder. Now we have to install the modules:

```
    make ssh
    drush dl etherpad wysiwyg  # libraries - add libraries if it is not installed
    drush en etherpad # enable etherpad
```

Now go to the configuration of your drupal installation (admin/config/content/etherpad-lite) and check that the etherpad URL is correct. Use the one with your ip address.

<img src="/ipc-ig/reports/img/etherpad-config.png">

Copy the content of the APIKEY.txt file inside the etherpad-lite directory into the field and test it, also, provide the ip address as the cookie domain. Note that the url of the platform and the etherpad should be the same, if they are different, the eterpad won't be able to use Drupal cookies and it will not work. The image bellow shows the access of the editor without the cookies enabled.

<img src="/ipc-ig/reports/img/etherpad-error.png">

To finish the setup, go to configuration of your text formats (?q=admin/config/content/formats) and add a new text format for Etherpad HTML with only one enabled filters - Limit allowed HTML tags. The list of allowed tags should be as follows:

<img src="/ipc-ig/reports/img/etherpad-filter.png">

Then go to configuration of your wysiwyg (?q=admin/config/content/wysiwyg) and add etherpad to your Etherpad HTML input format. Congratulations, you have configured etherpad-lite integration with Drupal

To enable several users working on the same node in the same time you should use the `Local Editable` formatter for the text fields i.e. go to the Manage Display tab on your content type (e.g. ?q=admin/structure/types/manage/article/display) and choose Local Editable for the text field you want to have etherpad-lite enabled.

The final result is the following.

<img src="/ipc-ig/reports/img/etherpad-editor.png">
<img src="/ipc-ig/reports/img/etherpad-doc.png">

The integration can also be accessed directly by the content edition block, inside the page. This allows the module to be integrated in a custom page and area of the Online Community Module.

## Full demonstration

The video presents a demo of the usage of the Etherpad module.

<figure class="video_container">
  <video controls="true" allowfullscreen="true">
    <source src="/ipc-ig/reports/img/drupal-etherpad-demo.mp4" type="video/mp4">
  </video>
</figure>

## Usage limitations

The Document editor has some errors for saving documents multiple times. Sometimes the plugin does not update the Drupal field with the pad html. This has to be fixed in the etherpad-lite-jquery-plugin probably, but it is not a critical issue, since the operation works intermitently, this might be a simple javascript error.

The module has to be related to a content type field, specifically a filtered text field. This creates some limitations to usage in a broader way, like a meeting room or a simple chat. The interaction design must take into account these characteristics to provide a consise UX to the feature. The following list of items indicates the concerns and tasks to be executed when developing the integration:

 - There is a bug when saving a document multiple times, sometimes the newer content is not updated. The pad does keep the changes, but the Drupal block doesn't get the most recent content.
 - The design implications are very important. The etherpad module has very nice resources, but if the integration with Communities are not well designed, these features may not be very usefull to common users.
 - Audio and Video streaming are p2p (peer to peer) resources, this is an alternative to lower server costs, but generates more internet usage for users.
 - There will be necessary to create an Etherpad service in the Docker stack. This is not expensive, but will requeire some work to be done in infrastrucutre levels.

## Infrastructure concerns

The platform runs seven services to fully function: 

 - [Nginx server and reverse proxy](https://www.nginx.com/):
 - [Drupal php-fpm](https://www.php.net/manual/en/install.fpm.php):
 - [Drush shell](https://www.drush.org/):
 - [Solr Search](https://lucene.apache.org/solr/):
 - [Varnish Cache](https://varnish-cache.org/):
 - [Node js tools](https://nodejs.org/en/):
 - [MySQL](https://www.mysql.com/):
 - [Memcached](https://memcached.org/):

The Implications of integrating Etherpad to the platform will be small, but the stack will have to include an Etherpad API and a mysql database instance. The best approach is to create a new database using the same mysql service used by Drupal and add only the Etherpad service. The Etherpad cannot be hosted on the same machine as the php and nginx services because of the resources issues.

The Etherpad backend is a simple socket handler to keep editors connected, it communicates small sets of text data and does not compromise the network throughput. What could be more expensive in terms of machine resources are video/audio calls, but the platform uses a recently developed web standard called WebRTC (also evaluated as an option to integrate video calls directly to Drupal). The standard uses a peer-to-peer connection, what means that the video and audio streams doesn't cross the server, avoiding performance issues when multiple users are connected.

To learn more about the WebRTC standard access the [full documentation](https://webrtc.org/). To get more details about the Etherpad implementation of the WebRTC video/audio calls, read the [article](https://mclear.co.uk/2020/04/04/etherpad-vs-zoom-re-security/) about it.
