---
title: Additional Results
date: 2020-07-06 14:38:55
thumbnail: /img/thumb_pattern_orange.svg
toc: true
categories:
 - Analysis
---

During the process, some other problems were identified and must be pointed out. All of them are related to performance, but they also have an impact on other resources and how work is currently being done.

# Reports

The UNDP reporting module uses the Views Data Export contribution module to generate xls files. This module has a very large number of unresolved performance-related issues when generating a report, including Gateway Timeouts and long response times for commonly requested and unrelated pages.

There is a good solution proposed by a group of developers that consists of a batch denormalization of data in a separate read-only database. These data can be consumed by Power BI, in combination with other datasets, such as Mailchimp and Google Analytics, to generate much better and more flexible reports and dashboards. There is a presentation about this work and a sample configuration [here](https://www.drupal.org/project/denormalizer).

<div style="text-align:center">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/mPm3DEX_6F8?start=886" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

This will allow the team to use and create reports at any time without waiting while the platform struggles to generate very large xls files that do not provide all the necessary information.

# Databases

Some [materials](https://www.valuebound.com/resources/blog/a-beginners-guide-to-performance-optimization-drupal-8) indicate the use of [MariaDB](https://mariadb.org/) instead of [MySQL](https://www.mysql.com/) in combination with Drupal. Some benchmarks indicate that, in a Drupal environment, MariaDB has a better and more stable performance than MySQL, which is a considerable benefit, as the platform has some resource consumption problems that can be mitigated by changing the database technology. There are no open issues related to migrating from MySQL to MariaDB on Drupal and the Memcache module works well with both technologies.

Some threads about the topic: [MariaDB Thread pools and Drupal Performance](https://mariadb.com/kb/en/mariadb-vs-mysql-features/); [Thread pool, XtraDB MariaDB performance](https://mariadb.com/kb/en/mariadb-vs-mysql-features/).

# Notifications

The Drupal community has a large set of custom notification systems, as [Menu Notifications](https://www.drupal.org/project/menu_notifications), [Content Notifications](https://www.drupal.org/project/content_notifications), [Notifications Bar](https://www.drupal.org/project/notification_bar), [Notifications](https://www.drupal.org/project/notifications), [Messaging](https://www.drupal.org/project/messaging) and [Notification Widget](https://www.drupal.org/project/notificationswidget).

The latter option works well in Drupal 8 and has support for custom content type notification. It also shows the "dot" with a counter for unread notifications. The UNDP Notification Module is very complex and will require a lot of work to deliver the expected results.

# Registration Form

A Drupal 8+ version of the platform will support the implementation of [flexible registration forms](https://www.drupal.org/forum/support/module-development-and-code-questions/2017-05-12/how-to-customize-default-user). The until then, it is possible to deliver a better experience just by refactoring the layout into something more meaningfull. The example bellow shows a simple change that makes the usability much better.

<figure class="video_container">
  <video controls="true" allowfullscreen="true" poster="/ipc-ig/reports/img/multistep-register.png">
    <source src="/ipc-ig/reports/img/multistep-register.mp4" type="video/mp4">
  </video>
</figure>

Other small changes can make even more difference, such as the addition of a "Previous" button, better styles for each panel, the removal of many opitional fields and a step counter to indicate user registration completion.

The registration module is responsible for more than just the register/login, it is also related to all user options and Account, Profile and image editions, a big set of functionalities that are not described by the module name.
