# Relatório final e resultados alcançados

## Melhoria no tempo de resposta de acesso anônimo

O acesso anônimo foi otimizado através do [cache HTTP com Varnish](VARNISH.md),
foi adicionado ao servidor de homologação e produção uma instância do Varnish
configurado para realizar cache de todas as páginas de acesso anônimo. Os
resultados mostram uma redução no tempo médio de requisições de 8 segundos para
menos de 2 segundos.

|   **(a) Tempo de resposta sem Varnish**                           |   **(b) Tempo de resposta com Varnish**  |
| ----------------------------------------------------------------- | ---------------------------------------- |
| ![jmeter grafico anonimo](assets/jmeter/homolog-auth-vs-anon.png) | ![jmeter frafico autenticado](assets/jmeter/homolog-auth-vs-anon-varnish.png) |

O gráfico (a) acima mostra que o tempo de resposta entre requisições anônimas e
autenticadas é basicamente o mesmo, enquanto o gráfico (b) mostra com o uso do
Varnish que o tempo de resposta anônimo é bastante reduzido em comparação com
acesso autenticado.

## Memcached piora em 40% o tempo de resposta

A investigação à respeito da lentidão do portal mostrou que o memcached torna o
tempo médio de respostas 40% mais lento,
[testes com JMeter em ambiente local](NEW_RELIC_REPORT.md#solução-2-revisar-configuração-atual-do-memcached-e-quais-views-estão-incluídas)
mostraram um aumento de 5 segundos para 7 segundos no tempo médio das
requisições. Este problema requer um tempo maior de investigação e testes para
ser mitigado pois envolve uma intervenção invasiva no atual setup e
configuração do portal com alto potencial de efeitos colaterais.

|   **(a) Tempo de consumo na camada de banco de dados**            |   **(b) Tempo de resposta de transações Web**  |
| ----------------------------------------------------------------- | ---------------------------------------------- |
| ![time database](assets/newrelic/time-consuming-database-2.png)   | ![time transactions](assets/newrelic/web-transactions-time-memcache-2.png) |

O gráfico (a) acima exibe dois momentos de coleta de dados distintos, o
primeiro com o memcached ativo onde é possível visualizar um maior tempo de
consumo, e na segunda metade do gráfico o memcached foi desligado, o gráfico
(b) representa dados do mesmo período com memcached ativado e desativado, é
possível perceber o impacto do maior tempo de consumo pelo memcached refletido
no tempo de transações web, ou tempo de resposta a requisições.

## Módulos UNDP com performance ruim

[Relatório de profiling realizado com o New Relic](NEW_RELIC_REPORT.md) mostram
que o o módulo `undp_community` consome 3 vezes mais tempo que os outros
módulos do portal e as views `user_info_header` e `favorites` são 10 vezes mais
lentas que qualquer outra view do portal.

## FPM ...

(em construção)

## Testes automatizados

Foi implementado um conjunto de [testes automatizados][testes] cobrindo as
principais funcionalidades do portal, os testes podem ser utilizados em
ambiente de desenvolvimento, homologação ou produção, sendo útil para garantir
controle de qualidade em cada um destes ambientes. São 49 casos de teste para
verificar se páginas são renderizadas corretamente, se há erros de timeout ou
para conferir conteúdo ou resultados de buscas.

[testes]: https://gitlab.com/pencillabs/ipc-ig/social-protection-tests
