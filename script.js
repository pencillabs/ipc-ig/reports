$ = jQuery;
_p = $('#edit-account').parent();
_p.prepend('<div class="panel panel-default form-wrapper" id="edit-account-wrapper"><legend class="panel-heading"><span class="panel-title fieldset-legend">Account Info</span></legend><div class="panel-body"></div></div>');
acc = $('#edit-account');
acc_wrapper = $('#edit-account-wrapper');
img = $('.form-item-files-picture-upload');
acc.prepend(img);
acc_wrapper.find('.panel-body').append(acc);
agg_ = $('.form-item-field-user-agreement-und');
acc_wrapper.find('.panel-body').append(agg_);
form = $('.user-info-from-cookie');
$('.user-info-from-cookie > div').css('display','flex');
$('.user-info-from-cookie').css('width','100%');
$('.user-info-from-cookie').css('overflow-x','hidden');
$('form > div .panel-default').css('min-width','100%');
$('form > div .panel-body').css('max-height','600px');
$('form > div .panel-body').css('overflow-y','auto');
btn_next = `<button class="btn-next btn btn-success form-submit" style="
  margin: 0;
  float: right;
  background: transparent !important;
  color: black;
  border: none;
  margin-top: -5px;
  height: 35px;
  "> Next <span class="icon glyphicon glyphicon-chevron-right" aria-hidden="true"></span></button>`;

$('#edit-profile-main').find('legend').append(btn_next);
acc_wrapper.find('legend').append(btn_next);

$('.btn-next').click((e) => { 
  e.preventDefault();
  panel = $(e.currentTarget).parents('.panel');
  console.log(form);
  form.animate({ scrollLeft: form.scrollLeft() + panel.outerWidth() }, 350);
});

$('.form-item .progress').css({
  float: 'unset',
  margin:'0',
  borderRadius: '0',
  height: '35px',
  boxShadow: 'none',
});

sbmt = $('#edit-submit');
$('#edit-profile-profile').find('legend').append(sbmt);

sbmt.attr('style', 'background: transparent !important');
sbmt.css({
  margin: 0,
  float: 'right',
  color: 'black',
  border: 'none',
  marginTop: '-5px',
  height: '35px',
});
