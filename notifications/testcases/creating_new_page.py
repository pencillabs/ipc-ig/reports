# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re, pdb
from login_sp import LoginFullScreen as login

class CreateNewPageTestCase(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = "https://www.google.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_create_new_page_test_case(self):
        login.test_login_full_screen(self)
        driver = self.driver
        driver.get("http://socialprotection.local/connect/communities/maillog")
        driver.find_element_by_xpath("//div/ul/li/a[@class='btn btn-info hollow theme-share']").click()
        driver.find_element_by_id("edit-title").click()
        driver.find_element_by_id("edit-title").clear()
        driver.find_element_by_id("edit-title").send_keys(u"testando criar uma página com selenium")
        driver.find_element_by_id("switch_edit-body-und-0-value").click()
        driver.find_element_by_id("edit-body-und-0-value").click()
        driver.find_element_by_id("edit-body-und-0-value").send_keys("Página criada usando selenium")
        driver.find_element_by_id("edit-submit").click()
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
