
<!--
If you are asking a question rather than filing a bug, try one of these instead:
- Send an email to contact@socialprotection.org
- Ask a question in Trello Questions board
-->

<!-- Instructions For Filing a Bug: https://gitlab.com/pencillabs/ipc-ig/reports/FILLING_BUGS.md -->
### Description
<!-- Example: Error thrown when opening `/discover/publications`. No publications are shown. -->

#### Live error
<!-- Upload images or videos that provide examples of the problem described -->
<!-- ...or provide an URL that reproduces the error you experienced -->

#### Steps to Reproduce
<!--
Example:

1. Open `/discover`
2. Scroll to publications section and click it.
3. The page of publication is loaded empty.
-->

#### Expected Results
<!-- Example: A list of publications should be returned -->

#### Actual Results
<!-- Example: No publications are returned -->

### Browsers Affected
<!-- If its a visual error, check all browsers that apply -->
- [ ] Chrome
- [ ] Firefox
- [ ] Edge
- [ ] Safari 11
- [ ] Safari 10
- [ ] IE 11
