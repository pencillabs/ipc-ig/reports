# Testes de performance para o socialprotection.org

Testes de performance com [JMeter][] para o portal socialprotection.org
realizados em ambiente local.

## Definição dos testes

Testes configurados para requisitar 3 URLs anônimas:

1. http://socialprotection.local/
1. http://socialprotection.local/about
1. http://socialprotection.local/team

Essas 3 URLs são acessadas cada uma 5 vezes com um intervalo de 300ms entre
requisições, sendo realizados por 20 "usuários" (threads nos termos do JMeter)
simultâneos, este conjunto é repetido 5 vezes, resultando num total de 1500
requisições. O código-fonte dos testes estão no repositório [social-protection-performance][].

## Cache HTTP com Varnish

Foi adicionado um container [Varnish][] à frente do Nginx para prover cache de
páginas HTML mas o Drupal força por algum motivo (plugins próprios de cache
como Memcached, AuthCache e outros) um cabeçalho na resposta HTTP
`Cache-Control: no-cache, must-revalidate` fazendo o Varnish nunca responder
com conteúdo do cache causando `cache_miss` em todas as requisições (é
necessário revisar todos os módulos e configurações de cache utilizados no
portal), exemplo do cabeçalho HTTP retornado pelo Drupal realizado com o
comando `cURL` na página **/about**.

```console
$ curl -I http://socialprotection.local/about
HTTP/1.1 200 OK
Server: nginx/1.13.8
Date: Tue, 31 Mar 2020 22:33:48 GMT
Content-Type: text/html; charset=utf-8
Vary: Accept-Encoding
X-Powered-By: PHP/5.6.40
X-Drupal-Cache: MISS
Expires: Sun, 19 Nov 1978 05:00:00 GMT
Cache-Control: no-cache, must-revalidate
X-Content-Type-Options: nosniff
Content-Language: en
X-Frame-Options: SAMEORIGIN
X-UA-Compatible: IE=edge
X-Generator: Drupal 7 (http://drupal.org)
Link: <http://socialprotection.local/about>; rel="canonical",<http://socialprotection.local/node/11590>; rel="shortlink"
X-Varnish: 2
Age: 0
Via: 1.1 varnish (Varnish/6.4)
Accept-Ranges: bytes
Connection: keep-alive
```

Para contornar isto e forçar o cache do Varnish (**atenção** essa solução foi
feita apenas para fins de testes de validação do Varnish em ambiente local de
testes) foi adicionado às configurações do Varnish em
`/etc/varnish/default.vcl` uma diretiva forçando o cache de todas as páginas
por 1 hora e foi também removido do cabeçalho HTTP a chave `Cache-Control`:

```varnish
sub vcl_backend_response {
  set beresp.ttl = 1h;
  unset beresp.http.Cache-Control;
}
```

Com isso o Varnish passa a fazer cache de todas as requisições e retorna
apropriadamente o conteúdo em cache a partir da segunda requisição.

```console
curl -I http://socialprotection.local/about
HTTP/1.1 200 OK
Server: nginx/1.13.8
Date: Wed, 01 Apr 2020 02:52:30 GMT
Content-Type: text/html; charset=utf-8
Vary: Accept-Encoding
X-Powered-By: PHP/5.6.40
Expires: Sun, 19 Nov 1978 05:00:00 GMT
X-Content-Type-Options: nosniff
Content-Language: en
X-Frame-Options: SAMEORIGIN
X-UA-Compatible: IE=edge
X-Generator: Drupal 7 (http://drupal.org)
Link: <http://socialprotection.local/about>; rel="canonical",<http://socialprotection.local/node/11590>; rel="shortlink"
X-Varnish: 32770 3
Age: 0
Via: 1.1 varnish (Varnish/6.4)
Accept-Ranges: bytes
Content-Length: 115033
Connection: keep-alive
```

O container adicionado neste setup foi configurado para responder na porta 80
(porta anteriormente configurada no Nginx) e o Nginx foi reconfigurado para
usar a porta 8080, dessa forma é possível testar o site tanto com o Varnish,
quanto sem o Varnish acessando o Nginx e realizando acesso na configuração
padrão.

## Resultados

### Testes sem o Varnish, na porta 8080 do Nginx

Os testes foram executados num primeiro momento sem coleta de dados para
fazer o portal criar qualquer eventual cache e termos uma validação dos
testes com caches totalmente preenchidos. Na segunda execuçao foi feito
coleta de dados apresentados a seguir com uso de gráficos gerados pelo
próprio JMeter.

![nginx graph](assets/jmeter/01-spline-nginx.png)

![nginx graph](assets/jmeter/01-sumario-nginx.png)

Resumo:
* Tempo médio de resposta por requisição: 7184 ms (> 7 segundos)
* Vazão / Throughput = 2,6 requisições por segundo

### Testes com o Varnish, na porta 80

Neste cenário os testes foram também executados num primeiro momento
apenas para criar o cache no Varnish e então executados uma segunda
vez para coletar os dados e gráficos apresentados aqui.

![varnish graph](assets/jmeter/01-spline-varnish.png)

![varnish graph](assets/jmeter/01-sumario-varnish.png)

Resumo:
* Tempo médio de resposta por requisição: 5 ms (< 1 segundo)
* Vazão / Throughput = 35,7 requisições por segundo

A razão para a incrível diferença no tempo de resposta do Varnish é que
requisições são respondidas diretamente por ele sem chegar no PHP, Drupal,
Mysql ou Memcached. O Varnish recupera o resultado em cache e retorna
diretamente a resposta da requisição.

### Testes sem Varnish com Memcached desativado

O relatório produzido com os dados do New Relic apontaram para uma possível
lentidão do memcache, resultando em um maior tempo de resposta, algo que vai no
caminho oposto já que o memcached deveria contribuir para melhoria de
performance.

Os testes foram executados num cenário sem memcached, o módulo memcached foi
desabilitado com `drush dis memcache` no container `ssh`, em seguida todas as
configurações relacionadas ao memcache foram desabilitadas no arquivo
`settings.php`, o mesmo procedimento foi realizado, testes executados num
primeiro momento sem coleta de dados e num segundo momento com coleta,
apresentados à seguir.

![memcache graph](assets/jmeter/01-spline-memcache.png)

![memcache graph](assets/jmeter/01-sumario-memcache.png)

Resumo:
* Tempo médio de resposta por requisição: 5537 ms (> 5 segundos)
* Vazão / Throughput = 3,3 requisições por segundo

Os resultados confirmam os [dados do New Relic](/NEW_RELIC_REPORT.md) de que o
memcached está piorando o tempo de resposta do portal.

### Testes com Drupal oficial

Um Drupal limpo sem dados do socialprotection ou códigos personalizados foi
executado com a imagem docker oficial `docker run --name teste-drupal-jmeter -p
9090:80 -d drupal`, configurado com setup padrão e 2 artigos criados para
manter os testes JMeter funcionais, o objetivo é medir a performance do Drupal
sem interferência dos dados ou códigos do socialprotection.

![drupal graph](assets/jmeter/spline-drupal.png)

![drupal graph](assets/jmeter/sumario-drupal.png)

Resumo:
* Tempo médio de resposta por requisição: 19 ms (< 1 segundo)
* Vazão / Throughput = 35,1 requisições por segundo

### Cenários de teste adicionais

Alguns cenários além dos reportados acima foram realizados, nenhum apresentou
melhoria significativa, os dados agregados estão disponíveis na planilha
[performance.ods](performance.ods) e podem ser visualizados em [HTML aqui](performance.html.md).

#### Tempo de acesso autenticado e anônimo

Testes do JMETER com 2 grupos de usuários, um anônimo e outro autenticado, foi
realizado em homologação com acesso a página inicial do portal.

Teste Anônimo X Autenticado com Varnish:

![auth](assets/jmeter/homolog-auth-vs-anon-varnish.png)

Resumo com Varnish:
* Tempo médio de resposta por requisição Anônimo: 705 ms
* Tempo médio de resposta por requisição Autenticado: 9992 ms

Teste Anônimo X Autenticado sem Varnish:

![auth](assets/jmeter/homolog-auth-vs-anon.png)

Resumo sem Varnish:
* Tempo médio de resposta por requisição Anônimo: 8957 ms
* Tempo médio de resposta por requisição Autenticado: 11333 ms

## Próximos passos

Para a execução dos testes, não foi feita nenhuma configuração específica de
serviços e módulos do php, apenas foram mantidas aquelas já definidas no deploy
automático da plataforma. Existem configurações alternativas para se otimizar o
número de processos paralelos, limite de memória e latência máxima para o php e
banco de dados.

O Varnish é um poderoso cache HTTP, mas para usá-lo em produção é necessário
definir quais páginas devem ter cache, quanto tempo cada página permanece em
cache e quais regras são tomadas para invalidar o cache.

A princípio apenas páginas de acesso anônimo devem ser configuradas para cache
no Varnish, então requisições com informação de sessão em Cookie devem ficar de
fora, uma [configuração comum][increasing-your-hitrate] é descartar cookies
para URLs que não casam com `/admin`:

```varnish
if (!(req.url ~ "^/admin/")) {
  unset req.http.Cookie;
}
```

As regras podem ser tão mais elaboradas quanto necessário mas cairão sempre no
uso de expressões regulares. O Varnish se preocupa com o header `Cache-Control`
e com o parâmetro _max-age_ para calcular quanto tempo um objeto permanece em
cache, então é importante que o backend, neste caso o Drupal, envie
corretamente este cabeçalho tanto para garantir cache quanto para invalidar
páginas que tenham sido atualizadas, [testes adicionais com Varnish](VARNISH.md)
foram realizados em ambiente de homologação para validar a proposta da solução.

Além de configurar o Varnish e definir as melhores flags para o cenário é
importante também re-configurar o memcached para fazer ter impacto positivo ao
invés de negativo como está sendo atualmente, além de valiar também a
continuidade de diversos outros módulos de cache configurados hoje no portal
como o [Alternative PHP Cache][apc] (APC) e o [Authenticated User Page
Caching][authcache] (Authcache), ambos em uso juntamente com o [Memcache][].

Uma boa documentação de referência é este [overview sobre sistemas de
cache][overview-cache] do manual performance e escalabilidade do Drupal 7.

[jmeter]: https://jmeter.apache.org
[social-protection-performance]: https://gitlab.com/pencillabs/ipc-ig/social-protection-performance
[varnish]: https://varnish-cache.org
[increasing-your-hitrate]: https://varnish-cache.org/docs/trunk/users-guide/increasing-your-hitrate.html#cookies-from-the-client
[apc]: https://www.drupal.org/project/apc
[authcache]: https://www.drupal.org/project/authcache
[memcache]: https://www.drupal.org/project/memcache
[overview-cache]: https://www.drupal.org/docs/7/managing-site-performance-and-scalability/caching-to-improve-performance/caching-overview
